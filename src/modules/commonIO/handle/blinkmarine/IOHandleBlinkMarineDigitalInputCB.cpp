/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleBlinkMarineDigitalInput.h"
#include <cstdbool>


void IOHandleBlinkMarineDigitalInput::CbPGNReceiveBlinkmarineInputKeyContactState2(uint8_t u8SAMember, uint8_t u8KeyNumber, uint8_t u8ContactState) {

  // TODO: Timeout handling.

  IOHandleBlinkMarineDigitalInput* c = mCallbackDirectory[u8SAMember][(u8KeyNumber - 1)];
  if(c != nullptr)
  {
    c->CbPGNReceiveBlinkmarineInputKeyContactState3(u8SAMember, u8KeyNumber, u8ContactState);
  }

}


void IOHandleBlinkMarineDigitalInput::CbPGNReceiveBlinkmarineInputKeyContactState3(uint8_t u8SAMember, uint8_t u8KeyNumber, uint8_t u8ContactState) {
  bool newData;
  assert(u8SAMember == this->u8SAMember);
  newData = u8ContactState;

  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}

IOHandleBlinkMarineDigitalInput* IOHandleBlinkMarineDigitalInput::mCallbackDirectory[0xFF][4] = {}; //0-init.

