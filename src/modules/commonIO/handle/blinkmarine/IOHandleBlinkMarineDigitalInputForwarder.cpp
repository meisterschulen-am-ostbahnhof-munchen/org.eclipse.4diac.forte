/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include <cstdbool>
#include <cstdint>
#include "IOHandleBlinkMarineDigitalInput.h"
#include "BlinkmarineInput.h"

/**
 * @fn void CbPGNReceiveBlinkmarineInputKeyContactState(iso_u8, iso_u8, iso_u8)
 * @brief
 *
 * @pre
 * @post
 * @param u8SAMember     Current source address (SA) of the network member
 * @param u8KeyNumber    XX: Key number
 * @param u8ContactState 00h: Switch OFF (Key released) 01h: Switch ON (Key pressed)
 */
void CbPGNReceiveBlinkmarineInputKeyContactState(uint8_t u8SAMember, uint8_t u8KeyNumber, uint8_t u8ContactState) {
  IOHandleBlinkMarineDigitalInput::CbPGNReceiveBlinkmarineInputKeyContactState2(u8SAMember, u8KeyNumber, u8ContactState);
}
