/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleBlinkMarineDigitalInput.h"
#include <cstdbool>
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"

IOHandleBlinkMarineDigitalInput::IOHandleBlinkMarineDigitalInput(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , u8SAMember((TForteUInt8)paU8SAMember)
    , mPin(paPin)
    , newData(false)
  {
  uint8_t u8PinIndex = this->mPin - CommonIOEnums::PinNumber::Key1;
  IOHandleBlinkMarineDigitalInput::mCallbackDirectory[this->u8SAMember][u8PinIndex] = this;
  }


void IOHandleBlinkMarineDigitalInput::get(CIEC_ANY &paState) {
  static_cast<CIEC_BOOL &>(paState) = CIEC_BOOL(this->newData);
}

void IOHandleBlinkMarineDigitalInput::set(const CIEC_ANY &paState) {
}

IOHandleBlinkMarineDigitalInput::~IOHandleBlinkMarineDigitalInput() {
  uint8_t u8PinIndex = this->mPin - CommonIOEnums::PinNumber::Key1;
  IOHandleBlinkMarineDigitalInput::mCallbackDirectory[this->u8SAMember][u8PinIndex] = nullptr;
}
