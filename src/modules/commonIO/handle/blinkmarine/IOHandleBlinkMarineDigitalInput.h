/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef SRC_MODULES_COMMONIO_HANDLE_BLINKMARINE_IOHANDLEBLINKMARINEDIGITALINPUT_H_
#define SRC_MODULES_COMMONIO_HANDLE_BLINKMARINE_IOHANDLEBLINKMARINEDIGITALINPUT_H_

#include <core/io/mapper/io_handle.h>
#include <core/io/mapper/io_observer.h>
#include <CommonIODeviceController.h>
#include <stdint.h>


class CommonIODeviceController;

class IOHandleBlinkMarineDigitalInput : public forte::core::io::IOHandle {

public:
    IOHandleBlinkMarineDigitalInput(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin);
    ~IOHandleBlinkMarineDigitalInput();
  void get(CIEC_ANY &) override;
  void set(const CIEC_ANY &) override;

  //Static Callback
  static void CbPGNReceiveBlinkmarineInputKeyContactState2(uint8_t u8SAMember, uint8_t u8KeyNumber, uint8_t u8ContactState);

  //in-Class Callback
  void CbPGNReceiveBlinkmarineInputKeyContactState3(uint8_t u8SAMember, uint8_t u8KeyNumber, uint8_t u8ContactState);



private:
  uint8_t u8SAMember;
  CommonIOEnums::PinNumber mPin;
  bool newData;
  EXT_RAM_BSS_ATTR static IOHandleBlinkMarineDigitalInput* mCallbackDirectory[0xFF][4];

};

#endif /* SRC_MODULES_COMMONIO_HANDLE_BLINKMARINE_IOHANDLEBLINKMARINEDIGITALINPUT_H_ */
