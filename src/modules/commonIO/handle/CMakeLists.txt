#*******************************************************************************
# Copyright (c) 2021, 2022 Jonathan Lainer
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
# 
# Contributors:
#   Jonathan Lainer - Initial implementation.
# *******************************************************************************/

forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR})

if(FORTE_MODULE_DATAPANEL)
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/datapanel)
  forte_add_sourcefile_hcpp(datapanel/IOHandleDataPanelDigitalOutput)
  forte_add_sourcefile_hcpp(datapanel/IOHandleDataPanelDigitalInput)
  forte_add_sourcefile_cpp(datapanel/IOHandleDataPanelDigitalInputForwarder.cpp)
  forte_add_sourcefile_cpp(datapanel/IOHandleDataPanelDigitalInputCB.cpp)
endif(FORTE_MODULE_DATAPANEL)  

if(FORTE_MODULE_BLINKMARINE)
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/blinkmarine)
  forte_add_sourcefile_hcpp(blinkmarine/IOHandleBlinkMarineDigitalInput)
  forte_add_sourcefile_cpp(blinkmarine/IOHandleBlinkMarineDigitalInputForwarder.cpp)
  forte_add_sourcefile_cpp(blinkmarine/IOHandleBlinkMarineDigitalInputCB.cpp)
endif(FORTE_MODULE_BLINKMARINE) 

if(FORTE_MODULE_ESP32_DIGITAL_OUT)
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/esp32_digital_out)
  forte_add_sourcefile_hcpp(esp32_digital_out/IOHandleESP32DigitalO)
endif(FORTE_MODULE_ESP32_DIGITAL_OUT)  

if(FORTE_MODULE_ESP32_DIGITAL_IN)
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/esp32_digital_in)
  forte_add_sourcefile_hcpp(esp32_digital_in/button_handle)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_DOWN)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_UP)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_REPEAT)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_SINGLE_CLICK)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_DOUBLE_CLICK)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_LONG_PRESS_START)
  forte_add_sourcefile_hcpp(esp32_digital_in/IOHandleESP32Button_LONG_PRESS_HOLD)
endif(FORTE_MODULE_ESP32_DIGITAL_IN) 

if(FORTE_MODULE_ISOBUS)


# Auxiliary

  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/isobus/Auxiliary)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_00)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_01)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_02)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_03)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_04)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_05)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_06)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_07)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_08)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_09)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_10)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_11)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_12)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_13)
  forte_add_sourcefile_hcpp(isobus/Auxiliary/IOHandleAuxiliaryFunction_14)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_00.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_01.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_02.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_03.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_04.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_05.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_06.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_07.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_08.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_09.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_10.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_11.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_12.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_13.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryFunctionVT_14.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_00.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_01.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_02.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_03.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_04.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_05.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_06.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_07.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_08.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_09.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_10.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_11.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_12.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_13.cpp)
  forte_add_sourcefile_cpp(isobusVT/Auxiliary/IOHandleAuxiliaryInputVTForwarder_14.cpp)

# Button
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/isobus/Button)
  forte_add_sourcefile_hcpp(isobus/Button/IOHandleButton)
  forte_add_sourcefile_hcpp(isobus/Button/IOHandleButton_DOWN)
  forte_add_sourcefile_hcpp(isobus/Button/IOHandleButton_HOLD)
  forte_add_sourcefile_hcpp(isobus/Button/IOHandleButton_UP)
  forte_add_sourcefile_cpp(isobusVT/Button/IOHandleButtonVT.cpp)
  forte_add_sourcefile_cpp(isobusVT/Button/IOHandleButtonVT_DOWN.cpp)
  forte_add_sourcefile_cpp(isobusVT/Button/IOHandleButtonVT_HOLD.cpp)
  forte_add_sourcefile_cpp(isobusVT/Button/IOHandleButtonVT_UP.cpp)
  forte_add_sourcefile_cpp(isobusVT/Button/IOHandleButtonVTForwarder.cpp)

# NumericValue
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/isobus/NumericValue)
  forte_add_sourcefile_hcpp(isobus/NumericValue/IOHandleNumericValue)
  forte_add_sourcefile_cpp(isobusVT/NumericValue/IOHandleNumericValueVT.cpp)
  forte_add_sourcefile_cpp(isobusVT/NumericValue/IOHandleNumericValueVTForwarder.cpp)

# Softkey
  forte_add_include_directories(${CMAKE_CURRENT_SOURCE_DIR}/isobus/Softkey)
  forte_add_sourcefile_hcpp(isobus/Softkey/IOHandleSoftKey)
  forte_add_sourcefile_hcpp(isobus/Softkey/IOHandleSoftKey_DOWN)
  forte_add_sourcefile_hcpp(isobus/Softkey/IOHandleSoftKey_HOLD)
  forte_add_sourcefile_hcpp(isobus/Softkey/IOHandleSoftKey_UP)
  forte_add_sourcefile_cpp(isobusVT/Softkey/IOHandleSoftKeyVT.cpp)
  forte_add_sourcefile_cpp(isobusVT/Softkey/IOHandleSoftKeyVT_DOWN.cpp)
  forte_add_sourcefile_cpp(isobusVT/Softkey/IOHandleSoftKeyVT_HOLD.cpp)
  forte_add_sourcefile_cpp(isobusVT/Softkey/IOHandleSoftKeyVT_UP.cpp)
  forte_add_sourcefile_cpp(isobusVT/Softkey/IOHandleSoftKeyVTForwarder.cpp)

endif(FORTE_MODULE_ISOBUS)


