/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include <cstdbool>

#include "IOHandleNumericValue.h"
#include "VIEngine.h"

void IOHandleNumericValue::VTC_handleNumericValue1(const struct InputNumber_S *pInputNumberData) {
  DEVLOG_DEBUG("[IOHandleNumericValue] CIOHandler got NumericValue %i %i", pInputNumberData->objectIdOfInputNumber, pInputNumberData->newValue);
  VTC_handleNumericValue2(pInputNumberData);
}


void IOHandleNumericValue::VTC_handleNumericValue2(const struct InputNumber_S *pInputNumberData) {
  if(mVICallbackDirectory->count(pInputNumberData->objectIdOfInputNumber) > 0)
  {
    if(mVICallbackDirectory->at(pInputNumberData->objectIdOfInputNumber) != nullptr)
    {
      mVICallbackDirectory->at(pInputNumberData->objectIdOfInputNumber)->VTC_handleNumericValue3(pInputNumberData);
    }
  }
}


void IOHandleNumericValue::VTC_handleNumericValue3(const struct InputNumber_S *pInputNumberData) {
  if(this->newData == pInputNumberData->newValue) {
    //noop
  } else {
    this->newData = pInputNumberData->newValue;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleNumericValue*> * IOHandleNumericValue::mVICallbackDirectory = new std::map<uint16_t, IOHandleNumericValue*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
