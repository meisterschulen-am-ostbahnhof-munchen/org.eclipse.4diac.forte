/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleSoftKey.h"
#include <cstdbool>

#include "VIEngine.h"

void IOHandleSoftKey::VTC_handleSoftkeys1(const struct SoftKeyActivation_S *pKeyData) {
  DEVLOG_DEBUG("[IOHandleSoftKey] CIOHandler got Key %i %i", pKeyData->objectIdOfKeyObject, pKeyData->keyActivationCode);
  bool newData = false;
  switch(pKeyData->keyActivationCode)
  {
    case SK_PRESSED:
    case SK_STILL_HELD:
      newData = true;
      break;
    case SK_ABORTED: //VT Version 4<
    case SK_RELEASED:
      break;
  }
  VTC_handleSoftkeys2(newData, pKeyData);
}


void IOHandleSoftKey::VTC_handleSoftkeys2(bool newData, const struct SoftKeyActivation_S *pKeyData) {
  if(mVICallbackDirectory->count(pKeyData->objectIdOfKeyObject) > 0)
  {
    if(mVICallbackDirectory->at(pKeyData->objectIdOfKeyObject) != nullptr)
    {
      mVICallbackDirectory->at(pKeyData->objectIdOfKeyObject)->VTC_handleSoftkeys3(newData, pKeyData);
    }
  }
}


void IOHandleSoftKey::VTC_handleSoftkeys3(bool newData, const struct SoftKeyActivation_S *pKeyData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleSoftKey*> * IOHandleSoftKey::mVICallbackDirectory = new std::map<uint16_t, IOHandleSoftKey*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
