/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleSoftKey.h"
#include "IOHandleSoftKey_DOWN.h"
#include "IOHandleSoftKey_HOLD.h"
#include "IOHandleSoftKey_UP.h"
#include <cstdbool>

#include "VIEngine.h"

extern "C" void VTC_handleSoftkeys(const struct SoftKeyActivation_S *pKeyData);

void VTC_handleSoftkeys(const struct SoftKeyActivation_S *pKeyData) {
  IOHandleSoftKey     ::VTC_handleSoftkeys1(pKeyData);
  IOHandleSoftKey_DOWN::VTC_handleSoftkeys1(pKeyData);
  IOHandleSoftKey_HOLD::VTC_handleSoftkeys1(pKeyData);
  IOHandleSoftKey_UP  ::VTC_handleSoftkeys1(pKeyData);
}

