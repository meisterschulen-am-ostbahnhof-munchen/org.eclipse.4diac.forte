/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleSoftKey_DOWN.h"
#include <cstdbool>

#include "VIEngine.h"

void IOHandleSoftKey_DOWN::VTC_handleSoftkeys1(const struct SoftKeyActivation_S *pKeyData) {

  switch(pKeyData->keyActivationCode)
  {
    case SK_PRESSED:
      DEVLOG_DEBUG("[IOHandleSoftKey_DOWN] CIOHandler got Key %i %i", pKeyData->objectIdOfKeyObject, pKeyData->keyActivationCode);
      VTC_handleSoftkeys2(pKeyData);
      break;
    default:
      break;
  }
}


void IOHandleSoftKey_DOWN::VTC_handleSoftkeys2(const struct SoftKeyActivation_S *pKeyData) {
  if(mVICallbackDirectory->count(pKeyData->objectIdOfKeyObject) > 0)
  {
    if(mVICallbackDirectory->at(pKeyData->objectIdOfKeyObject) != nullptr)
    {
      mVICallbackDirectory->at(pKeyData->objectIdOfKeyObject)->VTC_handleSoftkeys3(pKeyData);
    }
  }
}


void IOHandleSoftKey_DOWN::VTC_handleSoftkeys3(const struct SoftKeyActivation_S *pKeyData) {
  this->onChange();
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleSoftKey_DOWN*> * IOHandleSoftKey_DOWN::mVICallbackDirectory = new std::map<uint16_t, IOHandleSoftKey_DOWN*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
