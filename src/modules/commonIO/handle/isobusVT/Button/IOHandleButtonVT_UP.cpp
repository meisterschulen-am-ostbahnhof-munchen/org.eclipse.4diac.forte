/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleButton_UP.h"
#include <cstdbool>

#include "VIEngine.h"

void IOHandleButton_UP::VTC_handleButtons1(const struct ButtonActivation_S *pButtonData) {
  switch(pButtonData->keyActivationCode)
  {
    case BT_RELEASED_UNLATCHED:
      DEVLOG_DEBUG("[IOHandleButton_UP] CIOHandler got Button %i %i", pButtonData->objectIdOfButtonObject, pButtonData->keyActivationCode);
      VTC_handleButtons2(pButtonData);
      break;
    default:
      break;
  }
}


void IOHandleButton_UP::VTC_handleButtons2(const struct ButtonActivation_S *pButtonData) {
  if(mVICallbackDirectory->count(pButtonData->objectIdOfButtonObject) > 0)
  {
    if(mVICallbackDirectory->at(pButtonData->objectIdOfButtonObject) != nullptr)
    {
      mVICallbackDirectory->at(pButtonData->objectIdOfButtonObject)->VTC_handleButtons3(pButtonData);
    }
  }
}


void IOHandleButton_UP::VTC_handleButtons3(const struct ButtonActivation_S *pButtonData) {
  this->onChange();
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleButton_UP*> * IOHandleButton_UP::mVICallbackDirectory = new std::map<uint16_t, IOHandleButton_UP*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
