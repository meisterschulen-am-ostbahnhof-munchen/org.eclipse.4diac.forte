/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleButton.h"
#include <cstdbool>

#include "VIEngine.h"

void IOHandleButton::VTC_handleButtons1(const struct ButtonActivation_S *pButtonData) {
  DEVLOG_DEBUG("[IOHandleButton] CIOHandler got Button %i %i", pButtonData->objectIdOfButtonObject, pButtonData->keyActivationCode);
  bool newData = false;
  switch(pButtonData->keyActivationCode)
  {
    case BT_PRESSED_LATCHED:
    case BT_STILL_HELD:
      newData = true;
      break;
    case BT_ABORTED: //VT Version 4<
    case BT_RELEASED_UNLATCHED:
      break;
  }
  VTC_handleButtons2(newData, pButtonData);
}


void IOHandleButton::VTC_handleButtons2(bool newData, const struct ButtonActivation_S *pButtonData) {
  if(mVICallbackDirectory->count(pButtonData->objectIdOfButtonObject) > 0)
  {
    if(mVICallbackDirectory->at(pButtonData->objectIdOfButtonObject) != nullptr)
    {
      mVICallbackDirectory->at(pButtonData->objectIdOfButtonObject)->VTC_handleButtons3(newData, pButtonData);
    }
  }
}


void IOHandleButton::VTC_handleButtons3(bool newData, const struct ButtonActivation_S *pButtonData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleButton*> * IOHandleButton::mVICallbackDirectory = new std::map<uint16_t, IOHandleButton*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
