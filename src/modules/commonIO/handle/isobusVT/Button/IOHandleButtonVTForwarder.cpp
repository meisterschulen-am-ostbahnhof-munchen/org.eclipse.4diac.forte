/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleButton.h"
#include "IOHandleButton_DOWN.h"
#include "IOHandleButton_HOLD.h"
#include "IOHandleButton_UP.h"
#include <cstdbool>

#include "VIEngine.h"

extern "C" void VTC_handleButtons(const struct ButtonActivation_S *pButtonData);

void VTC_handleButtons(const struct ButtonActivation_S *pButtonData) {
  IOHandleButton     ::VTC_handleButtons1(pButtonData);
  IOHandleButton_DOWN::VTC_handleButtons1(pButtonData);
  IOHandleButton_HOLD::VTC_handleButtons1(pButtonData);
  IOHandleButton_UP  ::VTC_handleButtons1(pButtonData);
}
