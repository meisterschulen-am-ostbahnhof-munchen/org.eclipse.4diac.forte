/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_05.h"
#include <cstdbool>

#include "VIEngine.h"

extern "C" void VTC_handleAuxInput_05  (const struct AuxiliaryNewInput_S *pAuxInputData);

void VTC_handleAuxInput_05  (const struct AuxiliaryNewInput_S *pAuxInputData) {
  IOHandleAuxiliaryFunction_05     ::VTC_handleAuxiliaryFunction1(pAuxInputData);
}

