/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_12.h"

#include "VIEngine.h"

/*
 *
 * Quadrature
 * Analog (maintains
 * position setting) 
 *
 * Value 1:
 * 0–100 %
 * Value 2:
 * 0–100 %
 * 
 * 
 * Two quadrature mounted analog maintains position setting
 * The centre position of each analog axis is at 50 % value.
 * Value 1:
 *  Axis #1:
 *  0 % = backward or down
 *  100 %(FAFF16) = forward or up
 * Value 2:
 *  Axis #2:
 *  0 % = left
 *  100 %(FAFF16) = right 
 * 
 * 
 * maps to IW IW - Input WORD (16 Bit)
 *
 */


void IOHandleAuxiliaryFunction_12::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_12] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  uint16_t newData_axis_1 = 0xFFFF;
  uint16_t newData_axis_2 = 0xFFFF;
  if (pAuxInputData->value1 <= ISO_VALID_SIGNAL_W)
  {
    newData_axis_1 = pAuxInputData->value1;
  }
  if (pAuxInputData->value2 <= ISO_VALID_SIGNAL_W)
  {
    newData_axis_2 = pAuxInputData->value2;
  }
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData_axis_1, newData_axis_2, pAuxInputData);
}


void IOHandleAuxiliaryFunction_12::VTC_handleAuxiliaryFunction2(uint16_t newData_axis_1, uint16_t newData_axis_2, const struct AuxiliaryNewInput_S *pAuxInputData) {
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_axis_1   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::AXIS_1);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_axis_2 = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::AXIS_2);
  if(mVICallbackDirectory->count(pair_axis_1) > 0)
  {
    if(mVICallbackDirectory->at(pair_axis_1) != nullptr)
    {
      mVICallbackDirectory->at(pair_axis_1)->VTC_handleAuxiliaryFunction3(newData_axis_1, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_axis_2) > 0)
  {
    if(mVICallbackDirectory->at(pair_axis_2) != nullptr)
    {
      mVICallbackDirectory->at(pair_axis_2)->VTC_handleAuxiliaryFunction3(newData_axis_2, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_12::VTC_handleAuxiliaryFunction3(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
    //TODO: do we need a Hysteresis here ?
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_12*> * IOHandleAuxiliaryFunction_12::mVICallbackDirectory = new std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_12*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
