/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_09.h"
#include <cstdbool>

#include "VIEngine.h"

/*
 *
 * Combined
 * Analog — return
 * to 50 % with
 * Dual Boolean —
 * Latching 
 *
 *
 * Value 1:
 * 0–100 %,
 * FB0016,
 * FB0116
 * Value 2:
 * 0–FFFF16
 * 
 * 
 * Analog maintains position setting with latching Boolean at 0 % and
 * 100 % positions
 * Value 1:
 * 0 % = backward, down, left, anticlockwise [counter-clockwise]
 * 100 %(FAFF16) = forward, up, right, clockwise
 * FB0016 = Latched forward
 * FB0116 = Latched backward
 * Value 2:
 * Number of physical transitions since power up. 
 *
 * maps to IW IW - Input WORD (16 Bit)
 * Maps to IX
 *
 *
 */



void IOHandleAuxiliaryFunction_09::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_09] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  uint16_t newData = 0xFFFF;
  bool newData_up   = false;
  bool newData_down = false;
  switch(pAuxInputData->value1)
  {
    case 0xFB00:
      newData_up = true;
      break;
    case 0xFB01:
      newData_down = true;
      break;
  }
  if (pAuxInputData->value1 <= ISO_VALID_SIGNAL_W)
  {
      newData = pAuxInputData->value1;
  }
  //Todo: value2 (Transition Counter)
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData, newData_up, newData_down, pAuxInputData);
}


void IOHandleAuxiliaryFunction_09::VTC_handleAuxiliaryFunction2(uint16_t newData, bool newData_up, bool newData_down, const struct AuxiliaryNewInput_S *pAuxInputData) {
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_axis_1   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::AXIS_1);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_up       = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::UP);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_down     = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::DOWN);
  if(mVICallbackDirectory->count(pair_axis_1) > 0)
  {
    if(mVICallbackDirectory->at(pair_axis_1) != nullptr)
    {
      mVICallbackDirectory->at(pair_axis_1)->VTC_handleAuxiliaryFunction3(newData, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_up) > 0)
  {
    if(mVICallbackDirectory->at(pair_up) != nullptr)
    {
      mVICallbackDirectory->at(pair_up)->VTC_handleAuxiliaryFunction3(newData_up, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_down) > 0)
  {
    if(mVICallbackDirectory->at(pair_down) != nullptr)
    {
      mVICallbackDirectory->at(pair_down)->VTC_handleAuxiliaryFunction3(newData_down, pAuxInputData);
    }
  }

}

void IOHandleAuxiliaryFunction_09::VTC_handleAuxiliaryFunction3(bool newData,       const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
    //TODO: do we need a Hysteresis here ?
  } else {
    this->newData = newData;
    this->onChange();
  }
}

void IOHandleAuxiliaryFunction_09::VTC_handleAuxiliaryFunction3(uint16_t newData_a, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData_a == newData_a) {
    //noop
    //TODO: do we need a Hysteresis here ?
  } else {
    this->newData_a = newData_a;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_09*> * IOHandleAuxiliaryFunction_09::mVICallbackDirectory = new std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_09*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
