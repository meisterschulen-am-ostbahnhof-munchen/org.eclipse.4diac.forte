/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_14.h"
#include <cstdbool>

#include "VIEngine.h"

/*
 *
 * Bidirectional
 * Encoder 
  * 
 * Value 1:
 * 0–FFFF16
 * Value 2:
 * 1–FFFF16
 * 
 * Count increases when turning in the encoders' “increase” direction
 * and count decreases when turning in the opposite direction
 * Value 1: Current Count
 *  0 to FFFF16 with rollover to 0.
 * Value 2: Calibration — Encoder Counts per revolution
 *  1 to FFFF16 (fixed value). 
 * 
 * 
 * maps to IW IW - Input WORD (16 Bit)
 *
 */

void IOHandleAuxiliaryFunction_14::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_14] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  uint16_t newData_counter = 0xFFFF;
  uint16_t newData_calibration = 0xFFFF;
  newData_counter = pAuxInputData->value1;
  newData_calibration = pAuxInputData->value2;
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData_counter, newData_calibration, pAuxInputData);
}


void IOHandleAuxiliaryFunction_14::VTC_handleAuxiliaryFunction2(uint16_t newData_counter, uint16_t newData_calibration, const struct AuxiliaryNewInput_S *pAuxInputData) {
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_counter   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::COUNTER);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_calibration = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::CALIBRATION);
  if(mVICallbackDirectory->count(pair_counter) > 0)
  {
    if(mVICallbackDirectory->at(pair_counter) != nullptr)
    {
      mVICallbackDirectory->at(pair_counter)->VTC_handleAuxiliaryFunction3(newData_counter, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_calibration) > 0)
  {
    if(mVICallbackDirectory->at(pair_calibration) != nullptr)
    {
      mVICallbackDirectory->at(pair_calibration)->VTC_handleAuxiliaryFunction3(newData_calibration, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_14::VTC_handleAuxiliaryFunction3(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_14*> * IOHandleAuxiliaryFunction_14::mVICallbackDirectory = new std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_14*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
