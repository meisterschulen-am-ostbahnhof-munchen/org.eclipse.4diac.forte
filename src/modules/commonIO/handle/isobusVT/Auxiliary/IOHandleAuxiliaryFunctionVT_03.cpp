/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_03.h"

#include "VIEngine.h"





/*
 *
 *
 * Analog
 * return to 50 %
 * Left/Right
 * 
 * 
 * Value 1:
 * 0–100 %
 * Value 2:
 * FFFF16
 * 
 * 
 * Two way analog (return to centre position)
 * Value 1:
 * 0 % = backward, down, left, anticlockwise [counter-clockwise]
 * 100 %(FAFF16) = forward, up, right, clockwise
 * Value 2:
 * Reserved, set to FFFF16
 *
 *
 * maps to IW IW - Input WORD (16 Bit)
 *
 */


void IOHandleAuxiliaryFunction_03::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_03] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  uint16_t newData = 0xFFFF;
  if (pAuxInputData->value1 <= ISO_VALID_SIGNAL_W)
  {
      newData = pAuxInputData->value1;
  }
  //Todo: value2 (Transition Counter)
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData, pAuxInputData);
}


void IOHandleAuxiliaryFunction_03::VTC_handleAuxiliaryFunction2(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(mVICallbackDirectory->count(pAuxInputData->objectIdOfAuxObject) > 0)
  {
    if(mVICallbackDirectory->at(pAuxInputData->objectIdOfAuxObject) != nullptr)
    {
      mVICallbackDirectory->at(pAuxInputData->objectIdOfAuxObject)->VTC_handleAuxiliaryFunction3(newData, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_03::VTC_handleAuxiliaryFunction3(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
    //TODO: do we need a Hysteresis here ?
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleAuxiliaryFunction_03*> * IOHandleAuxiliaryFunction_03::mVICallbackDirectory = new std::map<uint16_t, IOHandleAuxiliaryFunction_03*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
