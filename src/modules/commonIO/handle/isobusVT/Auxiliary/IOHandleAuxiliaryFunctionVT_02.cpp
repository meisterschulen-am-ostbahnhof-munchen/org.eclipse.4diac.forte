/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_02.h"
#include <cstdbool>

#include "VIEngine.h"




/*
 *
 * Boolean —
 * Non-Latching
 * (momentary)
 * Increase value
 *
 * Value 1:
 * 0, 1, 2
 * Value 2:
 * 0–FFFF16
 *
 *
 *
 * Two-position switch (return to off) (Momentary Single Pole, Double
 * Throw)
 * Value 1:
 * 0 = Off = backward, down, left, or not pressed
 * 1 = Momentary = forward, up, right, or pressed
 * 2 = held
 *
 *
 * Value 2:
 * Number of transitions of disabled to enabled since power up.
 * Overflows from FFFF16 to 0.
 *
 *
 *
 * Maps to IX
 *
 *
 */



void IOHandleAuxiliaryFunction_02::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_02] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  bool newData = false;
  switch(pAuxInputData->value1)
  {
    case AuxEnabled:
    case AuxHeld:
      newData = true;
      break;
    case AuxDisabled:
      break;
  }
  //Todo: value2 (Transition Counter)
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData, pAuxInputData);
}


void IOHandleAuxiliaryFunction_02::VTC_handleAuxiliaryFunction2(bool newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(mVICallbackDirectory->count(pAuxInputData->objectIdOfAuxObject) > 0)
  {
    if(mVICallbackDirectory->at(pAuxInputData->objectIdOfAuxObject) != nullptr)
    {
      mVICallbackDirectory->at(pAuxInputData->objectIdOfAuxObject)->VTC_handleAuxiliaryFunction3(newData, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_02::VTC_handleAuxiliaryFunction3(bool newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<uint16_t, IOHandleAuxiliaryFunction_02*> * IOHandleAuxiliaryFunction_02::mVICallbackDirectory = new std::map<uint16_t, IOHandleAuxiliaryFunction_02*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
