/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_11.h"
#include <cstdbool>

#include "VIEngine.h"

/*
 *
 * Quadrature
 * Boolean —
 * Non-Latching
 * 
 * 
 * Value 1:
 * Value 2:
 * 0–FFFF16
 * 

 * Two quadrature mounted Three-Position Switches (returning to centre
 * position) (Momentary Single Pole, Three Position, Centre Off)
 * Bit values can be combined to indicate transitions from one held
 * position to another held position.
 * See Figure J.1.
 * Value 1:
 * Bits 1 0 Forward or up
 * Bits 3 2 Backward or down
 * Bits 5 4 Right
 * Bits 7 6 Left
 * Possible values for each bit pair:
 * 
 * 
 *  00 = Off
 *  01 = On (first activation)
 *  10 = held
 *  11 = reserved
 * Value 2:
 * Number of transitions of disabled to enabled since power up.
 * Overflows from FFFF16 to 0.
 *
 *
 *
 * Maps to IX
 *
 *
 */



void IOHandleAuxiliaryFunction_11::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_11] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  bool newData_up    = false;
  bool newData_down  = false;
  bool newData_right = false;
  bool newData_left  = false;
  switch(pAuxInputData->value1 & 0x02u)
  {
    case AuxEnabled:
    case AuxHeld:
      newData_up = true;
      break;
    case AuxDisabled:
      break;
  }
  switch((pAuxInputData->value1 & 0x08u) >> 2)
  {
    case AuxEnabled:
    case AuxHeld:
      newData_down = true;
      break;
    case AuxDisabled:
      break;
  }
  switch((pAuxInputData->value1 & 0x20u) >> 4)
  {
    case AuxEnabled:
    case AuxHeld:
      newData_right = true;
      break;
    case AuxDisabled:
      break;
  }
  switch((pAuxInputData->value1 & 0x80u) >> 6)
  {
    case AuxEnabled:
    case AuxHeld:
      newData_left = true;
      break;
    case AuxDisabled:
      break;
  }
  //Todo: value2 (Transition Counter)
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData_up, newData_down, newData_right, newData_left, pAuxInputData);
}


void IOHandleAuxiliaryFunction_11::VTC_handleAuxiliaryFunction2(bool newData_up, bool newData_down, bool newData_right, bool newData_left, const struct AuxiliaryNewInput_S *pAuxInputData) {
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_up     = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::UP);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_down   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::DOWN);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_right  = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::RIGHT);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_left   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::LEFT);
  if(mVICallbackDirectory->count(pair_up) > 0)
  {
    if(mVICallbackDirectory->at(pair_up) != nullptr)
    {
      mVICallbackDirectory->at(pair_up)->VTC_handleAuxiliaryFunction3(newData_up, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_down) > 0)
  {
    if(mVICallbackDirectory->at(pair_down) != nullptr)
    {
      mVICallbackDirectory->at(pair_down)->VTC_handleAuxiliaryFunction3(newData_down, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_right) > 0)
  {
    if(mVICallbackDirectory->at(pair_right) != nullptr)
    {
      mVICallbackDirectory->at(pair_right)->VTC_handleAuxiliaryFunction3(newData_right, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_left) > 0)
  {
    if(mVICallbackDirectory->at(pair_left) != nullptr)
    {
      mVICallbackDirectory->at(pair_left)->VTC_handleAuxiliaryFunction3(newData_left, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_11::VTC_handleAuxiliaryFunction3(bool newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_11*> * IOHandleAuxiliaryFunction_11::mVICallbackDirectory = new std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_11*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
