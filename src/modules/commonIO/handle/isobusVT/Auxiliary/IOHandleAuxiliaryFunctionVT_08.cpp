/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_08.h"
#include <cstdbool>

#include "VIEngine.h"

/*
 *
 * Dual Boolean —
 * Latching (Down)
 * (Momentary up) 
 * 
 * Value 1:
 * 0, 1, 2, 4
 * Value 2:
 * 0–FFFF16
 * 
 * 
 * Three-Position Switch, latching in down position, momentary up
 * (Single Pole, Three Position, Centre Off)
 * Value 1:
 * 0 = Off (latching)
 * 1 = On = forward, up or right (non-latching)
 * 2 = held forward, up or right (non-latching)
 * 4 = On = backward, down or left (latching) 
 * 
 *
 *
 * AuxDisabled         = 0,  Off          - backward, down, left, or not pressed
 * AuxEnabled          = 1,  On           - forward, up, right, or pressed
 * AuxHeld             = 2,  Held         - (forward, up, or right)
 * AuxEnabledBackwards = 4,  On momentary - backward, down or left
 * AuxHeldBackwards    = 8,  Held         - (backwards, down, or left)
 *
 *
 *
 * Value 2:
 * Number of transitions of disabled to enabled since power up.
 * Overflows from FFFF16 to 0. 
 *
 *
 *
 * Maps to IX
 *
 *
 */



void IOHandleAuxiliaryFunction_08::VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData) {
  DEVLOG_DEBUG("[IOHandleAuxiliaryFunction_08] CIOHandler got AUX %i %i", pAuxInputData->objectIdOfAuxObject, pAuxInputData->value1);
  bool newData_up   = false;
  bool newData_down = false;
  switch(pAuxInputData->value1)
  {
    case AuxEnabled:
    case AuxHeld:
      newData_up = true;
      break;
    case AuxEnabledBackwards:
    case AuxHeldBackwards:
      newData_down = true;
      break;
    case AuxDisabled:
      break;
  }
  //Todo: value2 (Transition Counter)
  //Todo: Error Handling. pAuxInputData->iErrorCode
  VTC_handleAuxiliaryFunction2(newData_up, newData_down, pAuxInputData);
}


void IOHandleAuxiliaryFunction_08::VTC_handleAuxiliaryFunction2(bool newData_up, bool newData_down, const struct AuxiliaryNewInput_S *pAuxInputData) {
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_up     = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::UP);
  std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch> pair_down   = std::make_pair(pAuxInputData->objectIdOfAuxObject, CommonIOEnums::ThreePositionSwitch::DOWN);
  if(mVICallbackDirectory->count(pair_up) > 0)
  {
    if(mVICallbackDirectory->at(pair_up) != nullptr)
    {
      mVICallbackDirectory->at(pair_up)->VTC_handleAuxiliaryFunction3(newData_up, pAuxInputData);
    }
  }
  if(mVICallbackDirectory->count(pair_down) > 0)
  {
    if(mVICallbackDirectory->at(pair_down) != nullptr)
    {
      mVICallbackDirectory->at(pair_down)->VTC_handleAuxiliaryFunction3(newData_down, pAuxInputData);
    }
  }
}


void IOHandleAuxiliaryFunction_08::VTC_handleAuxiliaryFunction3(bool newData, const struct AuxiliaryNewInput_S *pAuxInputData) {
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}



#if defined(FORTE_MODULE_ISOBUS)
  std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_08*> * IOHandleAuxiliaryFunction_08::mVICallbackDirectory = new std::map<std::pair<uint16_t, CommonIOEnums::ThreePositionSwitch>, IOHandleAuxiliaryFunction_08*>(); //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
