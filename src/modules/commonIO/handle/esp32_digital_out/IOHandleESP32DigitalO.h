/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32DIGITALO_H_
#define SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32DIGITALO_H_

#include <core/io/mapper/io_handle.h>
#include <core/io/mapper/io_observer.h>
#include <CommonIODeviceController.h>
#include <stdint.h>

class CommonIODeviceController;

class IOHandleESP32DigitalO : public forte::core::io::IOHandle {

public:
    IOHandleESP32DigitalO(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin);
  void get(CIEC_ANY &) override;
  void set(const CIEC_ANY &) override;



private:
  int32_t gpio_num; //only used for Outputs.
};

#endif /* SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32DIGITALO_H_ */
