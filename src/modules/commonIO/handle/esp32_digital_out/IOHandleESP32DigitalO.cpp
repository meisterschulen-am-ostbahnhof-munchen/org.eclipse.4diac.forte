/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleESP32DigitalO.h"

#include <cstdbool>
#include "forte_usint.h"
#include "forte_uint.h"

// for Outputs only
#include "driver/gpio.h"


#if defined(FORTE_MODULE_HUTSCHIENENMOPED)
#include "hutschienenmoped_digital_out.h"
#endif //FORTE_MODULE_HUTSCHIENENMOPED


#if defined(FORTE_MODULE_DUALOUT)
#include "dual_out_digital_out.h"
#endif //FORTE_MODULE_HUTSCHIENENMOPED







IOHandleESP32DigitalO::IOHandleESP32DigitalO(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , gpio_num(GPIO_NUM_NC)
  {
    switch (paPin) {
      case CommonIOEnums::PinNumber::PinQ1:
        gpio_num = Q1_IO;
        break;
      case CommonIOEnums::PinNumber::PinQ2:
        gpio_num = Q2_IO;
        break;
      case CommonIOEnums::PinNumber::PinQ3:
        gpio_num = Q3_IO;
        break;
      case CommonIOEnums::PinNumber::PinQ4:
        gpio_num = Q4_IO;
        break;
    }
    if(gpio_num > GPIO_NUM_NC)
    {
      gpio_reset_pin((gpio_num_t)gpio_num);
      /* Set the GPIO as a push/pull output */
      gpio_set_direction((gpio_num_t)gpio_num, GPIO_MODE_OUTPUT);
    }
  }


void IOHandleESP32DigitalO::get(CIEC_ANY &paState) {
}

void IOHandleESP32DigitalO::set(const CIEC_ANY &paState) {
  int targetState = (true == static_cast<const CIEC_BOOL &>(paState)) ? 1 : 0;
  gpio_set_level((gpio_num_t)gpio_num, targetState);
}
