/*
 * hutschienenmoped_digital_out.h
 *
 *  Created on: 20.02.2023
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_OUT_HUTSCHIENENMOPED_DIGITAL_OUT_H_
#define SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_OUT_HUTSCHIENENMOPED_DIGITAL_OUT_H_


#if CONFIG_IDF_TARGET_ESP32
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_25) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32S2
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32S3
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32C3
  #define Q1_IO       (GPIO_NUM_NC) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32H4
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_25) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32C2
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_25) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32C6
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_25) /*!< GPIO_NUM_25, input and output */
#elif CONFIG_IDF_TARGET_ESP32H2
  #define Q1_IO       (GPIO_NUM_19) /*!< GPIO19, input and output */
  #define Q2_IO       (GPIO_NUM_5) /*!< GPIO_NUM_5, input and output */
  #define Q3_IO       (GPIO_NUM_33) /*!< GPIO_NUM_33, input and output */
  #define Q4_IO       (GPIO_NUM_25) /*!< GPIO_NUM_25, input and output */
#else
  #error no known Target
#endif


#endif /* SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_OUT_HUTSCHIENENMOPED_DIGITAL_OUT_H_ */
