/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleESP32Button.h"

#include <cstdbool>

#include "button_handle.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"

// for Buttons (input)
#include "iot_button.h"

#if defined(FORTE_MODULE_HUTSCHIENENMOPED)
#include "hutschienenmoped_digital_in.h"
#endif //FORTE_MODULE_HUTSCHIENENMOPED

IOHandleESP32Button::IOHandleESP32Button(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , newData(false)
	, g_btn(nullptr)
  {
    switch (paPin) {
      case CommonIOEnums::PinNumber::PinI1:
        register_GPIO_BUTTON(paPin, I1_IO);
        break;
      case CommonIOEnums::PinNumber::PinI2:
        register_GPIO_BUTTON(paPin, I2_IO);
        break;
      case CommonIOEnums::PinNumber::PinI3:
        register_GPIO_BUTTON(paPin, I3_IO);
        break;
      case CommonIOEnums::PinNumber::PinIA1:
        register_ADC_BUTTON(paPin, 0);
        break;
      case CommonIOEnums::PinNumber::PinIA2:
        register_ADC_BUTTON(paPin, 1);
        break;
      case CommonIOEnums::PinNumber::PinIA3:
        register_ADC_BUTTON(paPin, 2);
        break;
      case CommonIOEnums::PinNumber::PinIA4:
        register_ADC_BUTTON(paPin, 3);
        break;
      case CommonIOEnums::PinNumber::PinIA5:
        register_ADC_BUTTON(paPin, 4);
        break;
      case CommonIOEnums::PinNumber::PinIA6:
        register_ADC_BUTTON(paPin, 5);
        break;
      default:
        break;
    }
  }

static const button_event_t EVENT = BUTTON_PRESS_NORMAL;

void IOHandleESP32Button::register_GPIO_BUTTON(CommonIOEnums::PinNumber paPin, int32_t gpio_num) {
  //Check if button was already created
  if(g_btns[paPin] == nullptr) //button was not created before.
  {
    button_config_t cfg = {
      .type = BUTTON_TYPE_GPIO,
      .gpio_button_config = {
        .gpio_num = gpio_num,
        .active_level = 0,
      },
    };
    g_btns[paPin] = iot_button_create(&cfg);
  }
  g_btn = g_btns[paPin];
  iot_button_register_cb(g_btns[paPin], EVENT, button_cb, this);
}

void IOHandleESP32Button::register_ADC_BUTTON(CommonIOEnums::PinNumber paPin, uint8_t btnIndex) {
  //Check if button was already created
  if(g_btns[paPin] == nullptr) //button was not created before.
  {
    button_config_t cfg = {
      .type = BUTTON_TYPE_ADC,
      .adc_button_config = {
        .adc_channel = 4,     /*!< ADC1 channel 4 is GPIO32 */
        .button_index = btnIndex,
        .min = volmin[btnIndex],
        .max = volmax[btnIndex],
      },
    };
    g_btns[paPin] = iot_button_create(&cfg);
  }
  g_btn = g_btns[paPin];
  iot_button_register_cb(g_btns[paPin], EVENT, button_cb, this);
}

void IOHandleESP32Button::button_cb(void *hardware_data, void *usr_data) {
  if(usr_data) {
    IOHandleESP32Button *p = static_cast<IOHandleESP32Button *>(usr_data);
    p->button_cb(hardware_data);
  }
}

void IOHandleESP32Button::button_cb(void *hardware_data) {
  bool newData = false;
  switch(iot_button_get_event((button_handle_t)hardware_data))
  {
    case BUTTON_PRESS_DOWN:
      newData = true;
      break;
    default:
      break;
  }
  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}


void IOHandleESP32Button::get(CIEC_ANY &paState) {
  static_cast<CIEC_BOOL &>(paState) = CIEC_BOOL(this->newData);
}

IOHandleESP32Button::~IOHandleESP32Button() {
  iot_button_unregister_cb(g_btn, EVENT);
  DEVLOG_DEBUG("IOHandleESP32Button unregistered");
  size_t count = iot_button_count_cb(g_btn);
  DEVLOG_DEBUG("count  %i", count);
  if (0 == count) {
    DEVLOG_DEBUG("iot_button_delete unregistered");
    size_t i = get_btn_index(g_btn);
    iot_button_delete(g_btn);
    g_btns[i] = nullptr;
    g_btn = nullptr;
  }
}

void IOHandleESP32Button::set(const CIEC_ANY &paState) {
}
