/*
 * button_handle.cpp
 *
 *  Created on: 16.08.2022
 *      Author: franz
 */

#include "button_handle.h"


button_handle_t g_btns[BUTTON_NUM] = {0};
const uint16_t volmin[VOLUME_NUM] = { 190,  600, 1000, 1375, 1775, 2195 };
const uint16_t volmax[VOLUME_NUM] = { 600, 1000, 1375, 1775, 2195, 2705 };

size_t get_btn_index(button_handle_t btn)
{
    for (size_t i = 0; i < BUTTON_NUM; i++) {
        if (btn == g_btns[i]) {
            return i;
        }
    }
    return -1;
}
