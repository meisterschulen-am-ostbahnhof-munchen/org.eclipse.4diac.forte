/*
 * hutschienenmoped_digital_in.h
 *
 *  Created on: 20.02.2023
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_IN_HUTSCHIENENMOPED_DIGITAL_IN_H_
#define SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_IN_HUTSCHIENENMOPED_DIGITAL_IN_H_


#if CONFIG_IDF_TARGET_ESP32
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32S2
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32S3
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32C3
  #define I1_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_NC) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32H4
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32C2
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32C6
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#elif CONFIG_IDF_TARGET_ESP32H2
  #define I1_IO       (GPIO_NUM_26) /*!< GPIO_NUM_26, input and output */
  #define I2_IO       (GPIO_NUM_32) /*!< GPIO_NUM_32, input and output */
  #define I3_IO       (GPIO_NUM_39) /*!< GPIO_NUM_39, input and output */
#else
  #error no known Target
#endif


#endif /* SRC_MODULES_COMMONIO_HANDLE_ESP32_DIGITAL_IN_HUTSCHIENENMOPED_DIGITAL_IN_H_ */
