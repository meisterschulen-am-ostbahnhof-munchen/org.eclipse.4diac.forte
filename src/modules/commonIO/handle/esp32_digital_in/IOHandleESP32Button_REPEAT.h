/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32BUTTON_REPEAT_H_
#define SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32BUTTON_REPEAT_H_

#include <core/io/mapper/io_handle.h>
#include <core/io/mapper/io_observer.h>
#include <CommonIODeviceController.h>
#include <stdint.h>

class CommonIODeviceController;

class IOHandleESP32Button_REPEAT : public forte::core::io::IOHandle {

public:
    IOHandleESP32Button_REPEAT(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin);
    ~IOHandleESP32Button_REPEAT();
    void register_GPIO_BUTTON(CommonIOEnums::PinNumber paPin, int32_t gpio_num);
    void register_ADC_BUTTON (CommonIOEnums::PinNumber paPin, uint8_t btnIndex);
    static void button_cb(void *hardware_data, void *usr_data);
    void button_cb(void *hardware_data);

  void get(CIEC_ANY &) override;
  void set(const CIEC_ANY &) override;

private:
  uint8_t repeat;
  void * g_btn;
};

#endif /* SRC_MODULES_COMMONIO_HANDLE_IOHANDLEESP32BUTTON_REPEAT_H_ */
