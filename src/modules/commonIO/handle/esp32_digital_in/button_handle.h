/*
 * button_handle.h
 *
 *  Created on: 16.08.2022
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMONIO_HANDLE_BUTTON_HANDLE_H_
#define SRC_MODULES_COMMONIO_HANDLE_BUTTON_HANDLE_H_



#include "iot_button.h"

#define BUTTON_NUM 16
#define VOLUME_NUM 6


extern button_handle_t g_btns[BUTTON_NUM];

extern const uint16_t volmin[VOLUME_NUM];
extern const uint16_t volmax[VOLUME_NUM];

extern size_t get_btn_index(button_handle_t btn);



#endif /* SRC_MODULES_COMMONIO_HANDLE_BUTTON_HANDLE_H_ */
