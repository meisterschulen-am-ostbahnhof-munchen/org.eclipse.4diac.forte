/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleSoftKey_UP.h"
#include <cstdbool>
#include "forte_usint.h"
#include "forte_uint.h"

IOHandleSoftKey_UP::IOHandleSoftKey_UP(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mU16ObjId((TForteUInt16)paU16ObjId)
  {
    mVICallbackDirectory->insert({mU16ObjId, this});
  }


void IOHandleSoftKey_UP::get(CIEC_ANY &paState) {
}

void IOHandleSoftKey_UP::set(const CIEC_ANY &paState) {
}

IOHandleSoftKey_UP::~IOHandleSoftKey_UP() {
  mVICallbackDirectory->erase(mU16ObjId);
}
