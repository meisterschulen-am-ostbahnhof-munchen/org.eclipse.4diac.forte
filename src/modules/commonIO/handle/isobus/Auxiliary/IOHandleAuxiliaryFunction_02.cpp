/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_02.h"
#include <cstdbool>
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"

IOHandleAuxiliaryFunction_02::IOHandleAuxiliaryFunction_02(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mU16ObjId((TForteUInt16)paU16ObjId)
    , newData(false)
  {
    mVICallbackDirectory->insert({mU16ObjId, this});
  }


void IOHandleAuxiliaryFunction_02::get(CIEC_ANY &paState) {
  static_cast<CIEC_BOOL &>(paState) = CIEC_BOOL(this->newData);
}

void IOHandleAuxiliaryFunction_02::set(const CIEC_ANY &paState) {
}

IOHandleAuxiliaryFunction_02::~IOHandleAuxiliaryFunction_02() {
  mVICallbackDirectory->erase(mU16ObjId);
}
