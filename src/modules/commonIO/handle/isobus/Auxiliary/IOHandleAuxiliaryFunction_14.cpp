/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_14.h"
#include <cstdbool>
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_word.h"

IOHandleAuxiliaryFunction_14::IOHandleAuxiliaryFunction_14(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin, CommonIOEnums::ThreePositionSwitch paSwitch)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mPairId(std::make_pair((TForteUInt16)paU16ObjId, paSwitch))
    , newData(0x0)
  {
    mVICallbackDirectory->insert({mPairId, this});
  }


void IOHandleAuxiliaryFunction_14::get(CIEC_ANY &paState) {
  static_cast<CIEC_WORD &>(paState) = CIEC_WORD(this->newData);
}

void IOHandleAuxiliaryFunction_14::set(const CIEC_ANY &paState) {
}

IOHandleAuxiliaryFunction_14::~IOHandleAuxiliaryFunction_14() {
  mVICallbackDirectory->erase(mPairId);
}
