/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleAuxiliaryFunction_10.h"
#include <cstdbool>
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_word.h"

IOHandleAuxiliaryFunction_10::IOHandleAuxiliaryFunction_10(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin, CommonIOEnums::ThreePositionSwitch paSwitch)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mPairId(std::make_pair((TForteUInt16)paU16ObjId, paSwitch))
  {
    mVICallbackDirectory->insert({mPairId, this});
    switch(mPairId.second){
      case CommonIOEnums::ThreePositionSwitch::AXIS_1:
        newData_a = 0xFFFF;
        break;
      default:
        newData =false;
        break;
    }
  }


void IOHandleAuxiliaryFunction_10::get(CIEC_ANY &paState) {
  switch(mPairId.second){
    case CommonIOEnums::ThreePositionSwitch::AXIS_1:
      static_cast<CIEC_WORD &>(paState) = CIEC_WORD(this->newData_a);
      break;
    default:
      static_cast<CIEC_BOOL &>(paState) = CIEC_BOOL(this->newData);
      break;
  }
}

void IOHandleAuxiliaryFunction_10::set(const CIEC_ANY &paState) {
}

IOHandleAuxiliaryFunction_10::~IOHandleAuxiliaryFunction_10() {
  mVICallbackDirectory->erase(mPairId);
}
