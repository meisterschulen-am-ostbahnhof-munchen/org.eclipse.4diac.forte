/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef SRC_MODULES_COMMONIO_HANDLE_ISOBUS_IOHANDLEAUXILIARYFUNCTION_04_H_
#define SRC_MODULES_COMMONIO_HANDLE_ISOBUS_IOHANDLEAUXILIARYFUNCTION_04_H_

#include <core/io/mapper/io_handle.h>
#include <core/io/mapper/io_observer.h>
#include <CommonIODeviceController.h>
#include <stdint.h>


#if defined(FORTE_MODULE_ISOBUS)
  #include "IsoDef.h"
#endif //FORTE_MODULE_ISOBUS

class CommonIODeviceController;

class IOHandleAuxiliaryFunction_04 : public forte::core::io::IOHandle {

public:
    IOHandleAuxiliaryFunction_04(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin);
    ~IOHandleAuxiliaryFunction_04();
  void get(CIEC_ANY &) override;
  void set(const CIEC_ANY &) override;

#if defined(FORTE_MODULE_ISOBUS)

        //Static Callback
        static void VTC_handleAuxiliaryFunction1(const struct AuxiliaryNewInput_S *pAuxInputData);
        static void VTC_handleAuxiliaryFunction2(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData);

        //in-Class Callback
        void VTC_handleAuxiliaryFunction3(uint16_t newData, const struct AuxiliaryNewInput_S *pAuxInputData);

#endif //FORTE_MODULE_ISOBUS



private:
  uint16_t mU16ObjId;
  uint16_t newData;
#if defined(FORTE_MODULE_ISOBUS)
  static std::map<uint16_t, IOHandleAuxiliaryFunction_04*> * mVICallbackDirectory; //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
};

#endif /* SRC_MODULES_COMMONIO_HANDLE_ISOBUS_IOHANDLEAUXILIARYFUNCTION_04_H_ */
