/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef SRC_MODULES_COMMON_IO_HANDLE_IOHANDLEBUTTON_UP_H_
#define SRC_MODULES_COMMON_IO_HANDLE_IOHANDLEBUTTON_UP_H_

#include <core/io/mapper/io_handle.h>
#include <core/io/mapper/io_observer.h>
#include <CommonIODeviceController.h>
#include <stdint.h>


#if defined(FORTE_MODULE_ISOBUS)
  #include "IsoDef.h"
#endif //FORTE_MODULE_ISOBUS

class CommonIODeviceController;

class IOHandleButton_UP : public forte::core::io::IOHandle {

public:
    IOHandleButton_UP(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin);
    ~IOHandleButton_UP();
  void get(CIEC_ANY &) override;
  void set(const CIEC_ANY &) override;

#if defined(FORTE_MODULE_ISOBUS)

        //Static Callback
        static void VTC_handleButtons1(const struct ButtonActivation_S *pButtonData);
        static void VTC_handleButtons2(const struct ButtonActivation_S *pButtonData);

        //in-Class Callback
        void VTC_handleButtons3(const struct ButtonActivation_S *pButtonData);

#endif //FORTE_MODULE_ISOBUS



private:
  uint16_t mU16ObjId;
#if defined(FORTE_MODULE_ISOBUS)
  static std::map<uint16_t, IOHandleButton_UP*> * mVICallbackDirectory; //only for ISOBUS
#endif //FORTE_MODULE_ISOBUS
};

#endif /* SRC_MODULES_COMMON_IO_HANDLE_IOHANDLEBUTTON_UP_H_ */
