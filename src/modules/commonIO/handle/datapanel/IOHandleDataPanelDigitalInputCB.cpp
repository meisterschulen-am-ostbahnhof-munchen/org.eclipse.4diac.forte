/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleDataPanelDigitalInput.h"
#include <cstdbool>
#include "DatapanelAddresses.h"
#include "DatapanelInput.h"


void IOHandleDataPanelDigitalInput::CbPGNReceiveDataPanelIn2(const PGNDAT_T* psData) {

  if(psData->qTimedOut)
  {
    DEVLOG_ERROR("IOHandleDataPanelDigitalInput  qTimedOut    %i \n", psData->userParam);
    memset(&au8RXDataPanelIn[psData->userParam.u8SAIndex], 0, sizeof(datapanelInputs_s));       //Zero out the transaction
  }


  for (size_t i = 0; i < 16; ++i) {
    IOHandleDataPanelDigitalInput* c = mCallbackDirectory[psData->userParam.u8SAIndex][i];
    if(c != nullptr)
    {
      c->CbPGNReceiveDataPanelIn3(psData);
    }
  }
}


void IOHandleDataPanelDigitalInput::CbPGNReceiveDataPanelIn3(const PGNDAT_T* psData) {
  bool newData;
  assert(psData->userParam.u8SAIndex == this->userParam.u8SAIndex);
  switch(this->mPin){
    case CommonIOEnums::PinNumber::Pin1A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input1A; break;
    case CommonIOEnums::PinNumber::Pin1B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input1B; break;
    case CommonIOEnums::PinNumber::Pin2A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input2A; break;
    case CommonIOEnums::PinNumber::Pin2B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input2B; break;
    case CommonIOEnums::PinNumber::Pin3A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input3A; break;
    case CommonIOEnums::PinNumber::Pin3B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input3B; break;
    case CommonIOEnums::PinNumber::Pin4A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input4A; break;
    case CommonIOEnums::PinNumber::Pin4B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input4B; break;
    case CommonIOEnums::PinNumber::Pin5A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input5A; break;
    case CommonIOEnums::PinNumber::Pin5B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input5B; break;
    case CommonIOEnums::PinNumber::Pin6A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input6A; break;
    case CommonIOEnums::PinNumber::Pin6B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input6B; break;
    case CommonIOEnums::PinNumber::Pin7A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input7A; break;
    case CommonIOEnums::PinNumber::Pin7B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input7B; break;
    case CommonIOEnums::PinNumber::Pin8A:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input8A; break;
    case CommonIOEnums::PinNumber::Pin8B:  newData = au8RXDataPanelIn[this->userParam.u8SAIndex].Input8B; break;
    default:
      break;
  }




  if(this->newData == newData) {
    //noop
  } else {
    this->newData = newData;
    this->onChange();
  }
}

IOHandleDataPanelDigitalInput* IOHandleDataPanelDigitalInput::mCallbackDirectory[16][16] = {}; //0-init.

