/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleDataPanelDigitalOutput.h"
#include <cstdbool>
#include "forte_usint.h"
#include "forte_uint.h"
#include "DatapanelAddresses.h"
#include "DatapanelOutput.h"

IOHandleDataPanelDigitalOutput::IOHandleDataPanelDigitalOutput(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mPin(paPin)
  {
    this->userParam.u8SAIndex = (TForteUInt8)paU8SAMember - u8SAMember_BASE_e::u8SAMember_BASE;
  }

void IOHandleDataPanelDigitalOutput::get(CIEC_ANY &paState) {
  //TODO !! Inputs !
}

IOHandleDataPanelDigitalOutput::~IOHandleDataPanelDigitalOutput() {
  au8TXDataPanel51[this->userParam.u8SAIndex].Output1A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output1B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output2A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output2B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output3A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output3B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output4A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output4B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output5A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output5B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output6A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output6B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output7A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output7B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output8A           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Output8B           = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_1 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_2 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_3 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_4 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_5 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_6 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_7 = 0;
  au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_8 = 0;
}

void IOHandleDataPanelDigitalOutput::set(const CIEC_ANY &paState) {
  int targetState = (true == static_cast<const CIEC_BOOL &>(paState)) ? 1 : 0;
  switch(this->mPin){
    case CommonIOEnums::PinNumber::Pin1A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output1A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin1B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output1B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin2A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output2A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin2B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output2B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin3A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output3A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin3B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output3B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin4A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output4A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin4B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output4B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin5A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output5A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin5B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output5B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin6A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output6A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin6B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output6B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin7A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output7A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin7B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output7B           = targetState; break;
    case CommonIOEnums::PinNumber::Pin8A:  au8TXDataPanel51[this->userParam.u8SAIndex].Output8A           = targetState; break;
    case CommonIOEnums::PinNumber::Pin8B:  au8TXDataPanel51[this->userParam.u8SAIndex].Output8B           = targetState; break;
    case CommonIOEnums::PinNumber::PinIP1: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_1 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP2: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_2 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP3: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_3 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP4: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_4 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP5: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_5 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP6: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_6 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP7: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_7 = targetState; break;
    case CommonIOEnums::PinNumber::PinIP8: au8TXDataPanel51[this->userParam.u8SAIndex].Input_Power_Port_8 = targetState; break;
    default:
      break;
  }
}
