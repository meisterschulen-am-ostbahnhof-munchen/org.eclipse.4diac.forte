/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "IOHandleDataPanelDigitalInput.h"
#include <cstdbool>
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "DatapanelAddresses.h"

IOHandleDataPanelDigitalInput::IOHandleDataPanelDigitalInput(CommonIODeviceController *paDeviceCtrl, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIOEnums::PinNumber paPin)
    : forte::core::io::IOHandle(static_cast<forte::core::io::IODeviceController*>(paDeviceCtrl), paDirection, paDataType)
    , mPin(paPin)
    , newData(false)
  {
  this->userParam.u8SAIndex = (TForteUInt8)paU8SAMember - u8SAMember_BASE_e::u8SAMember_BASE;
  uint8_t u8PinIndex = this->mPin - CommonIOEnums::PinNumber::Pin1A;
  IOHandleDataPanelDigitalInput::mCallbackDirectory[this->userParam.u8SAIndex][u8PinIndex] = this;
  }


void IOHandleDataPanelDigitalInput::get(CIEC_ANY &paState) {
  static_cast<CIEC_BOOL &>(paState) = CIEC_BOOL(this->newData);
}

void IOHandleDataPanelDigitalInput::set(const CIEC_ANY &paState) {
}

IOHandleDataPanelDigitalInput::~IOHandleDataPanelDigitalInput() {
  uint8_t u8PinIndex = this->mPin - CommonIOEnums::PinNumber::Pin1A;
  IOHandleDataPanelDigitalInput::mCallbackDirectory[this->userParam.u8SAIndex][u8PinIndex] = nullptr;
}
