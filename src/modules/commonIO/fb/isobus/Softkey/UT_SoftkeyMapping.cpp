/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_SoftkeyMapping
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "UT_SoftkeyMapping.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "UT_SoftkeyMapping_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_UT_SoftkeyMapping, g_nStringIdUT_SoftkeyMapping)

const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId_1, g_nStringIdDigitalInput_1, g_nStringIdu16ObjId_2, g_nStringIdDigitalInput_2, g_nStringIdu16ObjId_3, g_nStringIdDigitalInput_3, g_nStringIdu16ObjId_4, g_nStringIdDigitalInput_4, g_nStringIdu16ObjId_5, g_nStringIdDigitalInput_5, g_nStringIdu16ObjId_6, g_nStringIdDigitalInput_6, g_nStringIdu16ObjId_7, g_nStringIdDigitalInput_7, g_nStringIdu16ObjId_8, g_nStringIdDigitalInput_8};
const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_UT_SoftkeyMapping::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, scmWithListDelimiter};
const TForteInt16 FORTE_UT_SoftkeyMapping::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_UT_SoftkeyMapping::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_UT_SoftkeyMapping::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_UT_SoftkeyMapping::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_UT_SoftkeyMapping::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  18, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_UT_SoftkeyMapping::FORTE_UT_SoftkeyMapping(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId_1(nullptr),
    conn_DigitalInput_1(nullptr),
    conn_u16ObjId_2(nullptr),
    conn_DigitalInput_2(nullptr),
    conn_u16ObjId_3(nullptr),
    conn_DigitalInput_3(nullptr),
    conn_u16ObjId_4(nullptr),
    conn_DigitalInput_4(nullptr),
    conn_u16ObjId_5(nullptr),
    conn_DigitalInput_5(nullptr),
    conn_u16ObjId_6(nullptr),
    conn_DigitalInput_6(nullptr),
    conn_u16ObjId_7(nullptr),
    conn_DigitalInput_7(nullptr),
    conn_u16ObjId_8(nullptr),
    conn_DigitalInput_8(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_UT_SoftkeyMapping::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId_1 = 0_UINT;
  var_DigitalInput_1 = u""_WSTRING;
  var_u16ObjId_2 = 0_UINT;
  var_DigitalInput_2 = u""_WSTRING;
  var_u16ObjId_3 = 0_UINT;
  var_DigitalInput_3 = u""_WSTRING;
  var_u16ObjId_4 = 0_UINT;
  var_DigitalInput_4 = u""_WSTRING;
  var_u16ObjId_5 = 0_UINT;
  var_DigitalInput_5 = u""_WSTRING;
  var_u16ObjId_6 = 0_UINT;
  var_DigitalInput_6 = u""_WSTRING;
  var_u16ObjId_7 = 0_UINT;
  var_DigitalInput_7 = u""_WSTRING;
  var_u16ObjId_8 = 0_UINT;
  var_DigitalInput_8 = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_UT_SoftkeyMapping::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_1A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1.getValue(), var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin1A));
      handle_2A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2.getValue(), var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin2A));
      handle_3A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3.getValue(), var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin3A));
      handle_4A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4.getValue(), var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin4A));
      handle_5A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5.getValue(), var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin5A));
      handle_6A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6.getValue(), var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin6A));
      handle_7A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7.getValue(), var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin7A));
      handle_8A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8.getValue(), var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::SoftKey, CommonIOEnums::PinNumber::Pin8A));


      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_UT_SoftkeyMapping::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId_1, conn_u16ObjId_1);
      readData(3, var_DigitalInput_1, conn_DigitalInput_1);
      readData(4, var_u16ObjId_2, conn_u16ObjId_2);
      readData(5, var_DigitalInput_2, conn_DigitalInput_2);
      readData(6, var_u16ObjId_3, conn_u16ObjId_3);
      readData(7, var_DigitalInput_3, conn_DigitalInput_3);
      readData(8, var_u16ObjId_4, conn_u16ObjId_4);
      readData(9, var_DigitalInput_4, conn_DigitalInput_4);
      readData(10, var_u16ObjId_5, conn_u16ObjId_5);
      readData(11, var_DigitalInput_5, conn_DigitalInput_5);
      readData(12, var_u16ObjId_6, conn_u16ObjId_6);
      readData(13, var_DigitalInput_6, conn_DigitalInput_6);
      readData(14, var_u16ObjId_7, conn_u16ObjId_7);
      readData(15, var_DigitalInput_7, conn_DigitalInput_7);
      readData(16, var_u16ObjId_8, conn_u16ObjId_8);
      readData(17, var_DigitalInput_8, conn_DigitalInput_8);
      break;
    }
    default:
      break;
  }
}

void FORTE_UT_SoftkeyMapping::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_UT_SoftkeyMapping::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId_1;
    case 3: return &var_DigitalInput_1;
    case 4: return &var_u16ObjId_2;
    case 5: return &var_DigitalInput_2;
    case 6: return &var_u16ObjId_3;
    case 7: return &var_DigitalInput_3;
    case 8: return &var_u16ObjId_4;
    case 9: return &var_DigitalInput_4;
    case 10: return &var_u16ObjId_5;
    case 11: return &var_DigitalInput_5;
    case 12: return &var_u16ObjId_6;
    case 13: return &var_DigitalInput_6;
    case 14: return &var_u16ObjId_7;
    case 15: return &var_DigitalInput_7;
    case 16: return &var_u16ObjId_8;
    case 17: return &var_DigitalInput_8;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_SoftkeyMapping::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_SoftkeyMapping::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_UT_SoftkeyMapping::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_UT_SoftkeyMapping::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId_1;
    case 3: return &conn_DigitalInput_1;
    case 4: return &conn_u16ObjId_2;
    case 5: return &conn_DigitalInput_2;
    case 6: return &conn_u16ObjId_3;
    case 7: return &conn_DigitalInput_3;
    case 8: return &conn_u16ObjId_4;
    case 9: return &conn_DigitalInput_4;
    case 10: return &conn_u16ObjId_5;
    case 11: return &conn_DigitalInput_5;
    case 12: return &conn_u16ObjId_6;
    case 13: return &conn_DigitalInput_6;
    case 14: return &conn_u16ObjId_7;
    case 15: return &conn_DigitalInput_7;
    case 16: return &conn_u16ObjId_8;
    case 17: return &conn_DigitalInput_8;
  }
  return nullptr;
}

CDataConnection *FORTE_UT_SoftkeyMapping::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_UT_SoftkeyMapping::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_UT_SoftkeyMapping::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

