/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_ButtonMapping
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_UT_ButtonMapping final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_UT_ButtonMapping)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;
  
  std::unique_ptr<forte::core::io::IOHandle> handle_1A;
  std::unique_ptr<forte::core::io::IOHandle> handle_2A;
  std::unique_ptr<forte::core::io::IOHandle> handle_3A;
  std::unique_ptr<forte::core::io::IOHandle> handle_4A;
  std::unique_ptr<forte::core::io::IOHandle> handle_5A;
  std::unique_ptr<forte::core::io::IOHandle> handle_6A;
  std::unique_ptr<forte::core::io::IOHandle> handle_7A;
  std::unique_ptr<forte::core::io::IOHandle> handle_8A;

public:
  FORTE_UT_ButtonMapping(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId_1;
  CIEC_WSTRING var_DigitalInput_1;
  CIEC_UINT var_u16ObjId_2;
  CIEC_WSTRING var_DigitalInput_2;
  CIEC_UINT var_u16ObjId_3;
  CIEC_WSTRING var_DigitalInput_3;
  CIEC_UINT var_u16ObjId_4;
  CIEC_WSTRING var_DigitalInput_4;
  CIEC_UINT var_u16ObjId_5;
  CIEC_WSTRING var_DigitalInput_5;
  CIEC_UINT var_u16ObjId_6;
  CIEC_WSTRING var_DigitalInput_6;
  CIEC_UINT var_u16ObjId_7;
  CIEC_WSTRING var_DigitalInput_7;
  CIEC_UINT var_u16ObjId_8;
  CIEC_WSTRING var_DigitalInput_8;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId_1;
  CDataConnection *conn_DigitalInput_1;
  CDataConnection *conn_u16ObjId_2;
  CDataConnection *conn_DigitalInput_2;
  CDataConnection *conn_u16ObjId_3;
  CDataConnection *conn_DigitalInput_3;
  CDataConnection *conn_u16ObjId_4;
  CDataConnection *conn_DigitalInput_4;
  CDataConnection *conn_u16ObjId_5;
  CDataConnection *conn_DigitalInput_5;
  CDataConnection *conn_u16ObjId_6;
  CDataConnection *conn_DigitalInput_6;
  CDataConnection *conn_u16ObjId_7;
  CDataConnection *conn_DigitalInput_7;
  CDataConnection *conn_u16ObjId_8;
  CDataConnection *conn_DigitalInput_8;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paDigitalInput_1, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paDigitalInput_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paDigitalInput_3, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paDigitalInput_4, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paDigitalInput_5, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paDigitalInput_6, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paDigitalInput_7, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paDigitalInput_8, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId_1 = pau16ObjId_1;
    var_DigitalInput_1 = paDigitalInput_1;
    var_u16ObjId_2 = pau16ObjId_2;
    var_DigitalInput_2 = paDigitalInput_2;
    var_u16ObjId_3 = pau16ObjId_3;
    var_DigitalInput_3 = paDigitalInput_3;
    var_u16ObjId_4 = pau16ObjId_4;
    var_DigitalInput_4 = paDigitalInput_4;
    var_u16ObjId_5 = pau16ObjId_5;
    var_DigitalInput_5 = paDigitalInput_5;
    var_u16ObjId_6 = pau16ObjId_6;
    var_DigitalInput_6 = paDigitalInput_6;
    var_u16ObjId_7 = pau16ObjId_7;
    var_DigitalInput_7 = paDigitalInput_7;
    var_u16ObjId_8 = pau16ObjId_8;
    var_DigitalInput_8 = paDigitalInput_8;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paDigitalInput_1, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paDigitalInput_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paDigitalInput_3, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paDigitalInput_4, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paDigitalInput_5, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paDigitalInput_6, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paDigitalInput_7, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paDigitalInput_8, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId_1, paDigitalInput_1, pau16ObjId_2, paDigitalInput_2, pau16ObjId_3, paDigitalInput_3, pau16ObjId_4, paDigitalInput_4, pau16ObjId_5, paDigitalInput_5, pau16ObjId_6, paDigitalInput_6, pau16ObjId_7, paDigitalInput_7, pau16ObjId_8, paDigitalInput_8, paQO, paSTATUS);
  }
};


