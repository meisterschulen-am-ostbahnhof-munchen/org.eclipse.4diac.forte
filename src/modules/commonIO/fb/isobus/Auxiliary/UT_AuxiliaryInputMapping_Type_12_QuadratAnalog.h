/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_12_QuadratAnalog
 *** Description: Type 12 QuadratAnalog Two quadrature mounted analog maintain position setting - centre is 50%
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_UT_AuxiliaryInputMapping_Type_12_QuadratAnalog final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_12_QuadratAnalog)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  std::unique_ptr<forte::core::io::IOHandle> handle_1A;
  std::unique_ptr<forte::core::io::IOHandle> handle_1B;
  std::unique_ptr<forte::core::io::IOHandle> handle_2A;
  std::unique_ptr<forte::core::io::IOHandle> handle_2B;
  std::unique_ptr<forte::core::io::IOHandle> handle_3A;
  std::unique_ptr<forte::core::io::IOHandle> handle_3B;
  std::unique_ptr<forte::core::io::IOHandle> handle_4A;
  std::unique_ptr<forte::core::io::IOHandle> handle_4B;
  std::unique_ptr<forte::core::io::IOHandle> handle_5A;
  std::unique_ptr<forte::core::io::IOHandle> handle_5B;
  std::unique_ptr<forte::core::io::IOHandle> handle_6A;
  std::unique_ptr<forte::core::io::IOHandle> handle_6B;
  std::unique_ptr<forte::core::io::IOHandle> handle_7A;
  std::unique_ptr<forte::core::io::IOHandle> handle_7B;
  std::unique_ptr<forte::core::io::IOHandle> handle_8A;
  std::unique_ptr<forte::core::io::IOHandle> handle_8B;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_UT_AuxiliaryInputMapping_Type_12_QuadratAnalog(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId_1;
  CIEC_WSTRING var_AnalogInput_1_Axis_1;
  CIEC_WSTRING var_AnalogInput_1_Axis_2;
  CIEC_UINT var_u16ObjId_2;
  CIEC_WSTRING var_AnalogInput_2_Axis_1;
  CIEC_WSTRING var_AnalogInput_2_Axis_2;
  CIEC_UINT var_u16ObjId_3;
  CIEC_WSTRING var_AnalogInput_3_Axis_1;
  CIEC_WSTRING var_AnalogInput_3_Axis_2;
  CIEC_UINT var_u16ObjId_4;
  CIEC_WSTRING var_AnalogInput_4_Axis_1;
  CIEC_WSTRING var_AnalogInput_4_Axis_2;
  CIEC_UINT var_u16ObjId_5;
  CIEC_WSTRING var_AnalogInput_5_Axis_1;
  CIEC_WSTRING var_AnalogInput_5_Axis_2;
  CIEC_UINT var_u16ObjId_6;
  CIEC_WSTRING var_AnalogInput_6_Axis_1;
  CIEC_WSTRING var_AnalogInput_6_Axis_2;
  CIEC_UINT var_u16ObjId_7;
  CIEC_WSTRING var_AnalogInput_7_Axis_1;
  CIEC_WSTRING var_AnalogInput_7_Axis_2;
  CIEC_UINT var_u16ObjId_8;
  CIEC_WSTRING var_AnalogInput_8_Axis_1;
  CIEC_WSTRING var_AnalogInput_8_Axis_2;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId_1;
  CDataConnection *conn_AnalogInput_1_Axis_1;
  CDataConnection *conn_AnalogInput_1_Axis_2;
  CDataConnection *conn_u16ObjId_2;
  CDataConnection *conn_AnalogInput_2_Axis_1;
  CDataConnection *conn_AnalogInput_2_Axis_2;
  CDataConnection *conn_u16ObjId_3;
  CDataConnection *conn_AnalogInput_3_Axis_1;
  CDataConnection *conn_AnalogInput_3_Axis_2;
  CDataConnection *conn_u16ObjId_4;
  CDataConnection *conn_AnalogInput_4_Axis_1;
  CDataConnection *conn_AnalogInput_4_Axis_2;
  CDataConnection *conn_u16ObjId_5;
  CDataConnection *conn_AnalogInput_5_Axis_1;
  CDataConnection *conn_AnalogInput_5_Axis_2;
  CDataConnection *conn_u16ObjId_6;
  CDataConnection *conn_AnalogInput_6_Axis_1;
  CDataConnection *conn_AnalogInput_6_Axis_2;
  CDataConnection *conn_u16ObjId_7;
  CDataConnection *conn_AnalogInput_7_Axis_1;
  CDataConnection *conn_AnalogInput_7_Axis_2;
  CDataConnection *conn_u16ObjId_8;
  CDataConnection *conn_AnalogInput_8_Axis_1;
  CDataConnection *conn_AnalogInput_8_Axis_2;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paAnalogInput_1_Axis_1, const CIEC_WSTRING &paAnalogInput_1_Axis_2, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paAnalogInput_2_Axis_1, const CIEC_WSTRING &paAnalogInput_2_Axis_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paAnalogInput_3_Axis_1, const CIEC_WSTRING &paAnalogInput_3_Axis_2, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paAnalogInput_4_Axis_1, const CIEC_WSTRING &paAnalogInput_4_Axis_2, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paAnalogInput_5_Axis_1, const CIEC_WSTRING &paAnalogInput_5_Axis_2, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paAnalogInput_6_Axis_1, const CIEC_WSTRING &paAnalogInput_6_Axis_2, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paAnalogInput_7_Axis_1, const CIEC_WSTRING &paAnalogInput_7_Axis_2, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paAnalogInput_8_Axis_1, const CIEC_WSTRING &paAnalogInput_8_Axis_2, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId_1 = pau16ObjId_1;
    var_AnalogInput_1_Axis_1 = paAnalogInput_1_Axis_1;
    var_AnalogInput_1_Axis_2 = paAnalogInput_1_Axis_2;
    var_u16ObjId_2 = pau16ObjId_2;
    var_AnalogInput_2_Axis_1 = paAnalogInput_2_Axis_1;
    var_AnalogInput_2_Axis_2 = paAnalogInput_2_Axis_2;
    var_u16ObjId_3 = pau16ObjId_3;
    var_AnalogInput_3_Axis_1 = paAnalogInput_3_Axis_1;
    var_AnalogInput_3_Axis_2 = paAnalogInput_3_Axis_2;
    var_u16ObjId_4 = pau16ObjId_4;
    var_AnalogInput_4_Axis_1 = paAnalogInput_4_Axis_1;
    var_AnalogInput_4_Axis_2 = paAnalogInput_4_Axis_2;
    var_u16ObjId_5 = pau16ObjId_5;
    var_AnalogInput_5_Axis_1 = paAnalogInput_5_Axis_1;
    var_AnalogInput_5_Axis_2 = paAnalogInput_5_Axis_2;
    var_u16ObjId_6 = pau16ObjId_6;
    var_AnalogInput_6_Axis_1 = paAnalogInput_6_Axis_1;
    var_AnalogInput_6_Axis_2 = paAnalogInput_6_Axis_2;
    var_u16ObjId_7 = pau16ObjId_7;
    var_AnalogInput_7_Axis_1 = paAnalogInput_7_Axis_1;
    var_AnalogInput_7_Axis_2 = paAnalogInput_7_Axis_2;
    var_u16ObjId_8 = pau16ObjId_8;
    var_AnalogInput_8_Axis_1 = paAnalogInput_8_Axis_1;
    var_AnalogInput_8_Axis_2 = paAnalogInput_8_Axis_2;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paAnalogInput_1_Axis_1, const CIEC_WSTRING &paAnalogInput_1_Axis_2, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paAnalogInput_2_Axis_1, const CIEC_WSTRING &paAnalogInput_2_Axis_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paAnalogInput_3_Axis_1, const CIEC_WSTRING &paAnalogInput_3_Axis_2, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paAnalogInput_4_Axis_1, const CIEC_WSTRING &paAnalogInput_4_Axis_2, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paAnalogInput_5_Axis_1, const CIEC_WSTRING &paAnalogInput_5_Axis_2, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paAnalogInput_6_Axis_1, const CIEC_WSTRING &paAnalogInput_6_Axis_2, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paAnalogInput_7_Axis_1, const CIEC_WSTRING &paAnalogInput_7_Axis_2, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paAnalogInput_8_Axis_1, const CIEC_WSTRING &paAnalogInput_8_Axis_2, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId_1, paAnalogInput_1_Axis_1, paAnalogInput_1_Axis_2, pau16ObjId_2, paAnalogInput_2_Axis_1, paAnalogInput_2_Axis_2, pau16ObjId_3, paAnalogInput_3_Axis_1, paAnalogInput_3_Axis_2, pau16ObjId_4, paAnalogInput_4_Axis_1, paAnalogInput_4_Axis_2, pau16ObjId_5, paAnalogInput_5_Axis_1, paAnalogInput_5_Axis_2, pau16ObjId_6, paAnalogInput_6_Axis_1, paAnalogInput_6_Axis_2, pau16ObjId_7, paAnalogInput_7_Axis_1, paAnalogInput_7_Axis_2, pau16ObjId_8, paAnalogInput_8_Axis_1, paAnalogInput_8_Axis_2, paQO, paSTATUS);
  }
};


