/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter
 *** Description: Type 14 BidirectionalCounter Count increases when turning in the encoders "increase" direction (analog opposite direction)
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  std::unique_ptr<forte::core::io::IOHandle> handle_1A;
  std::unique_ptr<forte::core::io::IOHandle> handle_1B;
  std::unique_ptr<forte::core::io::IOHandle> handle_2A;
  std::unique_ptr<forte::core::io::IOHandle> handle_2B;
  std::unique_ptr<forte::core::io::IOHandle> handle_3A;
  std::unique_ptr<forte::core::io::IOHandle> handle_3B;
  std::unique_ptr<forte::core::io::IOHandle> handle_4A;
  std::unique_ptr<forte::core::io::IOHandle> handle_4B;
  std::unique_ptr<forte::core::io::IOHandle> handle_5A;
  std::unique_ptr<forte::core::io::IOHandle> handle_5B;
  std::unique_ptr<forte::core::io::IOHandle> handle_6A;
  std::unique_ptr<forte::core::io::IOHandle> handle_6B;
  std::unique_ptr<forte::core::io::IOHandle> handle_7A;
  std::unique_ptr<forte::core::io::IOHandle> handle_7B;
  std::unique_ptr<forte::core::io::IOHandle> handle_8A;
  std::unique_ptr<forte::core::io::IOHandle> handle_8B;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId_1;
  CIEC_WSTRING var_CounterInput_1;
  CIEC_WSTRING var_CalibrationInput_1;
  CIEC_UINT var_u16ObjId_2;
  CIEC_WSTRING var_CounterInput_2;
  CIEC_WSTRING var_CalibrationInput_2;
  CIEC_UINT var_u16ObjId_3;
  CIEC_WSTRING var_CounterInput_3;
  CIEC_WSTRING var_CalibrationInput_3;
  CIEC_UINT var_u16ObjId_4;
  CIEC_WSTRING var_CounterInput_4;
  CIEC_WSTRING var_CalibrationInput_4;
  CIEC_UINT var_u16ObjId_5;
  CIEC_WSTRING var_CounterInput_5;
  CIEC_WSTRING var_CalibrationInput_5;
  CIEC_UINT var_u16ObjId_6;
  CIEC_WSTRING var_CounterInput_6;
  CIEC_WSTRING var_CalibrationInput_6;
  CIEC_UINT var_u16ObjId_7;
  CIEC_WSTRING var_CounterInput_7;
  CIEC_WSTRING var_CalibrationInput_7;
  CIEC_UINT var_u16ObjId_8;
  CIEC_WSTRING var_CounterInput_8;
  CIEC_WSTRING var_CalibrationInput_8;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId_1;
  CDataConnection *conn_CounterInput_1;
  CDataConnection *conn_CalibrationInput_1;
  CDataConnection *conn_u16ObjId_2;
  CDataConnection *conn_CounterInput_2;
  CDataConnection *conn_CalibrationInput_2;
  CDataConnection *conn_u16ObjId_3;
  CDataConnection *conn_CounterInput_3;
  CDataConnection *conn_CalibrationInput_3;
  CDataConnection *conn_u16ObjId_4;
  CDataConnection *conn_CounterInput_4;
  CDataConnection *conn_CalibrationInput_4;
  CDataConnection *conn_u16ObjId_5;
  CDataConnection *conn_CounterInput_5;
  CDataConnection *conn_CalibrationInput_5;
  CDataConnection *conn_u16ObjId_6;
  CDataConnection *conn_CounterInput_6;
  CDataConnection *conn_CalibrationInput_6;
  CDataConnection *conn_u16ObjId_7;
  CDataConnection *conn_CounterInput_7;
  CDataConnection *conn_CalibrationInput_7;
  CDataConnection *conn_u16ObjId_8;
  CDataConnection *conn_CounterInput_8;
  CDataConnection *conn_CalibrationInput_8;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paCounterInput_1, const CIEC_WSTRING &paCalibrationInput_1, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paCounterInput_2, const CIEC_WSTRING &paCalibrationInput_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paCounterInput_3, const CIEC_WSTRING &paCalibrationInput_3, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paCounterInput_4, const CIEC_WSTRING &paCalibrationInput_4, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paCounterInput_5, const CIEC_WSTRING &paCalibrationInput_5, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paCounterInput_6, const CIEC_WSTRING &paCalibrationInput_6, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paCounterInput_7, const CIEC_WSTRING &paCalibrationInput_7, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paCounterInput_8, const CIEC_WSTRING &paCalibrationInput_8, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId_1 = pau16ObjId_1;
    var_CounterInput_1 = paCounterInput_1;
    var_CalibrationInput_1 = paCalibrationInput_1;
    var_u16ObjId_2 = pau16ObjId_2;
    var_CounterInput_2 = paCounterInput_2;
    var_CalibrationInput_2 = paCalibrationInput_2;
    var_u16ObjId_3 = pau16ObjId_3;
    var_CounterInput_3 = paCounterInput_3;
    var_CalibrationInput_3 = paCalibrationInput_3;
    var_u16ObjId_4 = pau16ObjId_4;
    var_CounterInput_4 = paCounterInput_4;
    var_CalibrationInput_4 = paCalibrationInput_4;
    var_u16ObjId_5 = pau16ObjId_5;
    var_CounterInput_5 = paCounterInput_5;
    var_CalibrationInput_5 = paCalibrationInput_5;
    var_u16ObjId_6 = pau16ObjId_6;
    var_CounterInput_6 = paCounterInput_6;
    var_CalibrationInput_6 = paCalibrationInput_6;
    var_u16ObjId_7 = pau16ObjId_7;
    var_CounterInput_7 = paCounterInput_7;
    var_CalibrationInput_7 = paCalibrationInput_7;
    var_u16ObjId_8 = pau16ObjId_8;
    var_CounterInput_8 = paCounterInput_8;
    var_CalibrationInput_8 = paCalibrationInput_8;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paCounterInput_1, const CIEC_WSTRING &paCalibrationInput_1, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paCounterInput_2, const CIEC_WSTRING &paCalibrationInput_2, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paCounterInput_3, const CIEC_WSTRING &paCalibrationInput_3, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paCounterInput_4, const CIEC_WSTRING &paCalibrationInput_4, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paCounterInput_5, const CIEC_WSTRING &paCalibrationInput_5, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paCounterInput_6, const CIEC_WSTRING &paCalibrationInput_6, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paCounterInput_7, const CIEC_WSTRING &paCalibrationInput_7, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paCounterInput_8, const CIEC_WSTRING &paCalibrationInput_8, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId_1, paCounterInput_1, paCalibrationInput_1, pau16ObjId_2, paCounterInput_2, paCalibrationInput_2, pau16ObjId_3, paCounterInput_3, paCalibrationInput_3, pau16ObjId_4, paCounterInput_4, paCalibrationInput_4, pau16ObjId_5, paCounterInput_5, paCalibrationInput_5, pau16ObjId_6, paCounterInput_6, paCalibrationInput_6, pau16ObjId_7, paCounterInput_7, paCalibrationInput_7, pau16ObjId_8, paCounterInput_8, paCalibrationInput_8, paQO, paSTATUS);
  }
};


