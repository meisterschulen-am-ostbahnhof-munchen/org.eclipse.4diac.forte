/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched
 *** Description: Type 11 QuadratBool_NonLatched Two quadrature mounted Three-position switches - return to centre position
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched, g_nStringIdUT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched)

const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId_1, g_nStringIdDigitalInput_1_UP, g_nStringIdDigitalInput_1_DOWN, g_nStringIdDigitalInput_1_RIGHT, g_nStringIdDigitalInput_1_LEFT, g_nStringIdu16ObjId_2, g_nStringIdDigitalInput_2_UP, g_nStringIdDigitalInput_2_DOWN, g_nStringIdDigitalInput_2_RIGHT, g_nStringIdDigitalInput_2_LEFT, g_nStringIdu16ObjId_3, g_nStringIdDigitalInput_3_UP, g_nStringIdDigitalInput_3_DOWN, g_nStringIdDigitalInput_3_RIGHT, g_nStringIdDigitalInput_3_LEFT, g_nStringIdu16ObjId_4, g_nStringIdDigitalInput_4_UP, g_nStringIdDigitalInput_4_DOWN, g_nStringIdDigitalInput_4_RIGHT, g_nStringIdDigitalInput_4_LEFT, g_nStringIdu16ObjId_5, g_nStringIdDigitalInput_5_UP, g_nStringIdDigitalInput_5_DOWN, g_nStringIdDigitalInput_5_RIGHT, g_nStringIdDigitalInput_5_LEFT, g_nStringIdu16ObjId_6, g_nStringIdDigitalInput_6_UP, g_nStringIdDigitalInput_6_DOWN, g_nStringIdDigitalInput_6_RIGHT, g_nStringIdDigitalInput_6_LEFT, g_nStringIdu16ObjId_7, g_nStringIdDigitalInput_7_UP, g_nStringIdDigitalInput_7_DOWN, g_nStringIdDigitalInput_7_RIGHT, g_nStringIdDigitalInput_7_LEFT, g_nStringIdu16ObjId_8, g_nStringIdDigitalInput_8_UP, g_nStringIdDigitalInput_8_DOWN, g_nStringIdDigitalInput_8_RIGHT, g_nStringIdDigitalInput_8_LEFT};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  42, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId_1(nullptr),
    conn_DigitalInput_1_UP(nullptr),
    conn_DigitalInput_1_DOWN(nullptr),
    conn_DigitalInput_1_RIGHT(nullptr),
    conn_DigitalInput_1_LEFT(nullptr),
    conn_u16ObjId_2(nullptr),
    conn_DigitalInput_2_UP(nullptr),
    conn_DigitalInput_2_DOWN(nullptr),
    conn_DigitalInput_2_RIGHT(nullptr),
    conn_DigitalInput_2_LEFT(nullptr),
    conn_u16ObjId_3(nullptr),
    conn_DigitalInput_3_UP(nullptr),
    conn_DigitalInput_3_DOWN(nullptr),
    conn_DigitalInput_3_RIGHT(nullptr),
    conn_DigitalInput_3_LEFT(nullptr),
    conn_u16ObjId_4(nullptr),
    conn_DigitalInput_4_UP(nullptr),
    conn_DigitalInput_4_DOWN(nullptr),
    conn_DigitalInput_4_RIGHT(nullptr),
    conn_DigitalInput_4_LEFT(nullptr),
    conn_u16ObjId_5(nullptr),
    conn_DigitalInput_5_UP(nullptr),
    conn_DigitalInput_5_DOWN(nullptr),
    conn_DigitalInput_5_RIGHT(nullptr),
    conn_DigitalInput_5_LEFT(nullptr),
    conn_u16ObjId_6(nullptr),
    conn_DigitalInput_6_UP(nullptr),
    conn_DigitalInput_6_DOWN(nullptr),
    conn_DigitalInput_6_RIGHT(nullptr),
    conn_DigitalInput_6_LEFT(nullptr),
    conn_u16ObjId_7(nullptr),
    conn_DigitalInput_7_UP(nullptr),
    conn_DigitalInput_7_DOWN(nullptr),
    conn_DigitalInput_7_RIGHT(nullptr),
    conn_DigitalInput_7_LEFT(nullptr),
    conn_u16ObjId_8(nullptr),
    conn_DigitalInput_8_UP(nullptr),
    conn_DigitalInput_8_DOWN(nullptr),
    conn_DigitalInput_8_RIGHT(nullptr),
    conn_DigitalInput_8_LEFT(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId_1 = 0_UINT;
  var_DigitalInput_1_UP = u""_WSTRING;
  var_DigitalInput_1_DOWN = u""_WSTRING;
  var_DigitalInput_1_RIGHT = u""_WSTRING;
  var_DigitalInput_1_LEFT = u""_WSTRING;
  var_u16ObjId_2 = 0_UINT;
  var_DigitalInput_2_UP = u""_WSTRING;
  var_DigitalInput_2_DOWN = u""_WSTRING;
  var_DigitalInput_2_RIGHT = u""_WSTRING;
  var_DigitalInput_2_LEFT = u""_WSTRING;
  var_u16ObjId_3 = 0_UINT;
  var_DigitalInput_3_UP = u""_WSTRING;
  var_DigitalInput_3_DOWN = u""_WSTRING;
  var_DigitalInput_3_RIGHT = u""_WSTRING;
  var_DigitalInput_3_LEFT = u""_WSTRING;
  var_u16ObjId_4 = 0_UINT;
  var_DigitalInput_4_UP = u""_WSTRING;
  var_DigitalInput_4_DOWN = u""_WSTRING;
  var_DigitalInput_4_RIGHT = u""_WSTRING;
  var_DigitalInput_4_LEFT = u""_WSTRING;
  var_u16ObjId_5 = 0_UINT;
  var_DigitalInput_5_UP = u""_WSTRING;
  var_DigitalInput_5_DOWN = u""_WSTRING;
  var_DigitalInput_5_RIGHT = u""_WSTRING;
  var_DigitalInput_5_LEFT = u""_WSTRING;
  var_u16ObjId_6 = 0_UINT;
  var_DigitalInput_6_UP = u""_WSTRING;
  var_DigitalInput_6_DOWN = u""_WSTRING;
  var_DigitalInput_6_RIGHT = u""_WSTRING;
  var_DigitalInput_6_LEFT = u""_WSTRING;
  var_u16ObjId_7 = 0_UINT;
  var_DigitalInput_7_UP = u""_WSTRING;
  var_DigitalInput_7_DOWN = u""_WSTRING;
  var_DigitalInput_7_RIGHT = u""_WSTRING;
  var_DigitalInput_7_LEFT = u""_WSTRING;
  var_u16ObjId_8 = 0_UINT;
  var_DigitalInput_8_UP = u""_WSTRING;
  var_DigitalInput_8_DOWN = u""_WSTRING;
  var_DigitalInput_8_RIGHT = u""_WSTRING;
  var_DigitalInput_8_LEFT = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_1A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_UP.getValue(),    var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_1B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_1C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_1D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_2A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_UP.getValue(),    var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_2B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_2C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_2D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_3A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_UP.getValue(),    var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_3B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_3C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_3D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_4A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_UP.getValue(),    var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_4B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_4C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_4D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_5A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_UP.getValue(),    var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_5B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_5C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_5D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_6A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_UP.getValue(),    var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_6B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_6C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_6D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_7A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_UP.getValue(),    var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_7B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_7C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_7D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::LEFT));
      handle_8A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_UP.getValue(),    var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_8B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_DOWN.getValue(),  var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_8C = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_RIGHT.getValue(), var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::RIGHT));
      handle_8D = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_LEFT.getValue(),  var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_11, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::LEFT));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId_1, conn_u16ObjId_1);
      readData(3, var_DigitalInput_1_UP, conn_DigitalInput_1_UP);
      readData(4, var_DigitalInput_1_DOWN, conn_DigitalInput_1_DOWN);
      readData(5, var_DigitalInput_1_RIGHT, conn_DigitalInput_1_RIGHT);
      readData(6, var_DigitalInput_1_LEFT, conn_DigitalInput_1_LEFT);
      readData(7, var_u16ObjId_2, conn_u16ObjId_2);
      readData(8, var_DigitalInput_2_UP, conn_DigitalInput_2_UP);
      readData(9, var_DigitalInput_2_DOWN, conn_DigitalInput_2_DOWN);
      readData(10, var_DigitalInput_2_RIGHT, conn_DigitalInput_2_RIGHT);
      readData(11, var_DigitalInput_2_LEFT, conn_DigitalInput_2_LEFT);
      readData(12, var_u16ObjId_3, conn_u16ObjId_3);
      readData(13, var_DigitalInput_3_UP, conn_DigitalInput_3_UP);
      readData(14, var_DigitalInput_3_DOWN, conn_DigitalInput_3_DOWN);
      readData(15, var_DigitalInput_3_RIGHT, conn_DigitalInput_3_RIGHT);
      readData(16, var_DigitalInput_3_LEFT, conn_DigitalInput_3_LEFT);
      readData(17, var_u16ObjId_4, conn_u16ObjId_4);
      readData(18, var_DigitalInput_4_UP, conn_DigitalInput_4_UP);
      readData(19, var_DigitalInput_4_DOWN, conn_DigitalInput_4_DOWN);
      readData(20, var_DigitalInput_4_RIGHT, conn_DigitalInput_4_RIGHT);
      readData(21, var_DigitalInput_4_LEFT, conn_DigitalInput_4_LEFT);
      readData(22, var_u16ObjId_5, conn_u16ObjId_5);
      readData(23, var_DigitalInput_5_UP, conn_DigitalInput_5_UP);
      readData(24, var_DigitalInput_5_DOWN, conn_DigitalInput_5_DOWN);
      readData(25, var_DigitalInput_5_RIGHT, conn_DigitalInput_5_RIGHT);
      readData(26, var_DigitalInput_5_LEFT, conn_DigitalInput_5_LEFT);
      readData(27, var_u16ObjId_6, conn_u16ObjId_6);
      readData(28, var_DigitalInput_6_UP, conn_DigitalInput_6_UP);
      readData(29, var_DigitalInput_6_DOWN, conn_DigitalInput_6_DOWN);
      readData(30, var_DigitalInput_6_RIGHT, conn_DigitalInput_6_RIGHT);
      readData(31, var_DigitalInput_6_LEFT, conn_DigitalInput_6_LEFT);
      readData(32, var_u16ObjId_7, conn_u16ObjId_7);
      readData(33, var_DigitalInput_7_UP, conn_DigitalInput_7_UP);
      readData(34, var_DigitalInput_7_DOWN, conn_DigitalInput_7_DOWN);
      readData(35, var_DigitalInput_7_RIGHT, conn_DigitalInput_7_RIGHT);
      readData(36, var_DigitalInput_7_LEFT, conn_DigitalInput_7_LEFT);
      readData(37, var_u16ObjId_8, conn_u16ObjId_8);
      readData(38, var_DigitalInput_8_UP, conn_DigitalInput_8_UP);
      readData(39, var_DigitalInput_8_DOWN, conn_DigitalInput_8_DOWN);
      readData(40, var_DigitalInput_8_RIGHT, conn_DigitalInput_8_RIGHT);
      readData(41, var_DigitalInput_8_LEFT, conn_DigitalInput_8_LEFT);
      break;
    }
    default:
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId_1;
    case 3: return &var_DigitalInput_1_UP;
    case 4: return &var_DigitalInput_1_DOWN;
    case 5: return &var_DigitalInput_1_RIGHT;
    case 6: return &var_DigitalInput_1_LEFT;
    case 7: return &var_u16ObjId_2;
    case 8: return &var_DigitalInput_2_UP;
    case 9: return &var_DigitalInput_2_DOWN;
    case 10: return &var_DigitalInput_2_RIGHT;
    case 11: return &var_DigitalInput_2_LEFT;
    case 12: return &var_u16ObjId_3;
    case 13: return &var_DigitalInput_3_UP;
    case 14: return &var_DigitalInput_3_DOWN;
    case 15: return &var_DigitalInput_3_RIGHT;
    case 16: return &var_DigitalInput_3_LEFT;
    case 17: return &var_u16ObjId_4;
    case 18: return &var_DigitalInput_4_UP;
    case 19: return &var_DigitalInput_4_DOWN;
    case 20: return &var_DigitalInput_4_RIGHT;
    case 21: return &var_DigitalInput_4_LEFT;
    case 22: return &var_u16ObjId_5;
    case 23: return &var_DigitalInput_5_UP;
    case 24: return &var_DigitalInput_5_DOWN;
    case 25: return &var_DigitalInput_5_RIGHT;
    case 26: return &var_DigitalInput_5_LEFT;
    case 27: return &var_u16ObjId_6;
    case 28: return &var_DigitalInput_6_UP;
    case 29: return &var_DigitalInput_6_DOWN;
    case 30: return &var_DigitalInput_6_RIGHT;
    case 31: return &var_DigitalInput_6_LEFT;
    case 32: return &var_u16ObjId_7;
    case 33: return &var_DigitalInput_7_UP;
    case 34: return &var_DigitalInput_7_DOWN;
    case 35: return &var_DigitalInput_7_RIGHT;
    case 36: return &var_DigitalInput_7_LEFT;
    case 37: return &var_u16ObjId_8;
    case 38: return &var_DigitalInput_8_UP;
    case 39: return &var_DigitalInput_8_DOWN;
    case 40: return &var_DigitalInput_8_RIGHT;
    case 41: return &var_DigitalInput_8_LEFT;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId_1;
    case 3: return &conn_DigitalInput_1_UP;
    case 4: return &conn_DigitalInput_1_DOWN;
    case 5: return &conn_DigitalInput_1_RIGHT;
    case 6: return &conn_DigitalInput_1_LEFT;
    case 7: return &conn_u16ObjId_2;
    case 8: return &conn_DigitalInput_2_UP;
    case 9: return &conn_DigitalInput_2_DOWN;
    case 10: return &conn_DigitalInput_2_RIGHT;
    case 11: return &conn_DigitalInput_2_LEFT;
    case 12: return &conn_u16ObjId_3;
    case 13: return &conn_DigitalInput_3_UP;
    case 14: return &conn_DigitalInput_3_DOWN;
    case 15: return &conn_DigitalInput_3_RIGHT;
    case 16: return &conn_DigitalInput_3_LEFT;
    case 17: return &conn_u16ObjId_4;
    case 18: return &conn_DigitalInput_4_UP;
    case 19: return &conn_DigitalInput_4_DOWN;
    case 20: return &conn_DigitalInput_4_RIGHT;
    case 21: return &conn_DigitalInput_4_LEFT;
    case 22: return &conn_u16ObjId_5;
    case 23: return &conn_DigitalInput_5_UP;
    case 24: return &conn_DigitalInput_5_DOWN;
    case 25: return &conn_DigitalInput_5_RIGHT;
    case 26: return &conn_DigitalInput_5_LEFT;
    case 27: return &conn_u16ObjId_6;
    case 28: return &conn_DigitalInput_6_UP;
    case 29: return &conn_DigitalInput_6_DOWN;
    case 30: return &conn_DigitalInput_6_RIGHT;
    case 31: return &conn_DigitalInput_6_LEFT;
    case 32: return &conn_u16ObjId_7;
    case 33: return &conn_DigitalInput_7_UP;
    case 34: return &conn_DigitalInput_7_DOWN;
    case 35: return &conn_DigitalInput_7_RIGHT;
    case 36: return &conn_DigitalInput_7_LEFT;
    case 37: return &conn_u16ObjId_8;
    case 38: return &conn_DigitalInput_8_UP;
    case 39: return &conn_DigitalInput_8_DOWN;
    case 40: return &conn_DigitalInput_8_RIGHT;
    case 41: return &conn_DigitalInput_8_LEFT;
  }
  return nullptr;
}

CDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_11_QuadratBool_NonLatched::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

