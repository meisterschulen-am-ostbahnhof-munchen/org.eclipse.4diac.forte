/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown
 *** Description: Type 8 DualBool_LatchedDown Three-position switch - latching in down position
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown, g_nStringIdUT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown)

const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId_1, g_nStringIdDigitalInput_1_UP, g_nStringIdDigitalInput_1_DOWN, g_nStringIdu16ObjId_2, g_nStringIdDigitalInput_2_UP, g_nStringIdDigitalInput_2_DOWN, g_nStringIdu16ObjId_3, g_nStringIdDigitalInput_3_UP, g_nStringIdDigitalInput_3_DOWN, g_nStringIdu16ObjId_4, g_nStringIdDigitalInput_4_UP, g_nStringIdDigitalInput_4_DOWN, g_nStringIdu16ObjId_5, g_nStringIdDigitalInput_5_UP, g_nStringIdDigitalInput_5_DOWN, g_nStringIdu16ObjId_6, g_nStringIdDigitalInput_6_UP, g_nStringIdDigitalInput_6_DOWN, g_nStringIdu16ObjId_7, g_nStringIdDigitalInput_7_UP, g_nStringIdDigitalInput_7_DOWN, g_nStringIdu16ObjId_8, g_nStringIdDigitalInput_8_UP, g_nStringIdDigitalInput_8_DOWN};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  26, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId_1(nullptr),
    conn_DigitalInput_1_UP(nullptr),
    conn_DigitalInput_1_DOWN(nullptr),
    conn_u16ObjId_2(nullptr),
    conn_DigitalInput_2_UP(nullptr),
    conn_DigitalInput_2_DOWN(nullptr),
    conn_u16ObjId_3(nullptr),
    conn_DigitalInput_3_UP(nullptr),
    conn_DigitalInput_3_DOWN(nullptr),
    conn_u16ObjId_4(nullptr),
    conn_DigitalInput_4_UP(nullptr),
    conn_DigitalInput_4_DOWN(nullptr),
    conn_u16ObjId_5(nullptr),
    conn_DigitalInput_5_UP(nullptr),
    conn_DigitalInput_5_DOWN(nullptr),
    conn_u16ObjId_6(nullptr),
    conn_DigitalInput_6_UP(nullptr),
    conn_DigitalInput_6_DOWN(nullptr),
    conn_u16ObjId_7(nullptr),
    conn_DigitalInput_7_UP(nullptr),
    conn_DigitalInput_7_DOWN(nullptr),
    conn_u16ObjId_8(nullptr),
    conn_DigitalInput_8_UP(nullptr),
    conn_DigitalInput_8_DOWN(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId_1 = 0_UINT;
  var_DigitalInput_1_UP = u""_WSTRING;
  var_DigitalInput_1_DOWN = u""_WSTRING;
  var_u16ObjId_2 = 0_UINT;
  var_DigitalInput_2_UP = u""_WSTRING;
  var_DigitalInput_2_DOWN = u""_WSTRING;
  var_u16ObjId_3 = 0_UINT;
  var_DigitalInput_3_UP = u""_WSTRING;
  var_DigitalInput_3_DOWN = u""_WSTRING;
  var_u16ObjId_4 = 0_UINT;
  var_DigitalInput_4_UP = u""_WSTRING;
  var_DigitalInput_4_DOWN = u""_WSTRING;
  var_u16ObjId_5 = 0_UINT;
  var_DigitalInput_5_UP = u""_WSTRING;
  var_DigitalInput_5_DOWN = u""_WSTRING;
  var_u16ObjId_6 = 0_UINT;
  var_DigitalInput_6_UP = u""_WSTRING;
  var_DigitalInput_6_DOWN = u""_WSTRING;
  var_u16ObjId_7 = 0_UINT;
  var_DigitalInput_7_UP = u""_WSTRING;
  var_DigitalInput_7_DOWN = u""_WSTRING;
  var_u16ObjId_8 = 0_UINT;
  var_DigitalInput_8_UP = u""_WSTRING;
  var_DigitalInput_8_DOWN = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_1A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_UP.getValue(),   var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_1B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_1_DOWN.getValue(), var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_2A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_UP.getValue(),   var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_2B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_2_DOWN.getValue(), var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_3A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_UP.getValue(),   var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_3B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_3_DOWN.getValue(), var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_4A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_UP.getValue(),   var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_4B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_4_DOWN.getValue(), var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_5A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_UP.getValue(),   var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_5B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_5_DOWN.getValue(), var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_6A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_UP.getValue(),   var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_6B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_6_DOWN.getValue(), var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_7A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_UP.getValue(),   var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_7B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_7_DOWN.getValue(), var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::DOWN));
      handle_8A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_UP.getValue(),   var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::UP));
      handle_8B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_8_DOWN.getValue(), var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::AuxiliaryFunction_08, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::DOWN));


      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId_1, conn_u16ObjId_1);
      readData(3, var_DigitalInput_1_UP, conn_DigitalInput_1_UP);
      readData(4, var_DigitalInput_1_DOWN, conn_DigitalInput_1_DOWN);
      readData(5, var_u16ObjId_2, conn_u16ObjId_2);
      readData(6, var_DigitalInput_2_UP, conn_DigitalInput_2_UP);
      readData(7, var_DigitalInput_2_DOWN, conn_DigitalInput_2_DOWN);
      readData(8, var_u16ObjId_3, conn_u16ObjId_3);
      readData(9, var_DigitalInput_3_UP, conn_DigitalInput_3_UP);
      readData(10, var_DigitalInput_3_DOWN, conn_DigitalInput_3_DOWN);
      readData(11, var_u16ObjId_4, conn_u16ObjId_4);
      readData(12, var_DigitalInput_4_UP, conn_DigitalInput_4_UP);
      readData(13, var_DigitalInput_4_DOWN, conn_DigitalInput_4_DOWN);
      readData(14, var_u16ObjId_5, conn_u16ObjId_5);
      readData(15, var_DigitalInput_5_UP, conn_DigitalInput_5_UP);
      readData(16, var_DigitalInput_5_DOWN, conn_DigitalInput_5_DOWN);
      readData(17, var_u16ObjId_6, conn_u16ObjId_6);
      readData(18, var_DigitalInput_6_UP, conn_DigitalInput_6_UP);
      readData(19, var_DigitalInput_6_DOWN, conn_DigitalInput_6_DOWN);
      readData(20, var_u16ObjId_7, conn_u16ObjId_7);
      readData(21, var_DigitalInput_7_UP, conn_DigitalInput_7_UP);
      readData(22, var_DigitalInput_7_DOWN, conn_DigitalInput_7_DOWN);
      readData(23, var_u16ObjId_8, conn_u16ObjId_8);
      readData(24, var_DigitalInput_8_UP, conn_DigitalInput_8_UP);
      readData(25, var_DigitalInput_8_DOWN, conn_DigitalInput_8_DOWN);
      break;
    }
    default:
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId_1;
    case 3: return &var_DigitalInput_1_UP;
    case 4: return &var_DigitalInput_1_DOWN;
    case 5: return &var_u16ObjId_2;
    case 6: return &var_DigitalInput_2_UP;
    case 7: return &var_DigitalInput_2_DOWN;
    case 8: return &var_u16ObjId_3;
    case 9: return &var_DigitalInput_3_UP;
    case 10: return &var_DigitalInput_3_DOWN;
    case 11: return &var_u16ObjId_4;
    case 12: return &var_DigitalInput_4_UP;
    case 13: return &var_DigitalInput_4_DOWN;
    case 14: return &var_u16ObjId_5;
    case 15: return &var_DigitalInput_5_UP;
    case 16: return &var_DigitalInput_5_DOWN;
    case 17: return &var_u16ObjId_6;
    case 18: return &var_DigitalInput_6_UP;
    case 19: return &var_DigitalInput_6_DOWN;
    case 20: return &var_u16ObjId_7;
    case 21: return &var_DigitalInput_7_UP;
    case 22: return &var_DigitalInput_7_DOWN;
    case 23: return &var_u16ObjId_8;
    case 24: return &var_DigitalInput_8_UP;
    case 25: return &var_DigitalInput_8_DOWN;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId_1;
    case 3: return &conn_DigitalInput_1_UP;
    case 4: return &conn_DigitalInput_1_DOWN;
    case 5: return &conn_u16ObjId_2;
    case 6: return &conn_DigitalInput_2_UP;
    case 7: return &conn_DigitalInput_2_DOWN;
    case 8: return &conn_u16ObjId_3;
    case 9: return &conn_DigitalInput_3_UP;
    case 10: return &conn_DigitalInput_3_DOWN;
    case 11: return &conn_u16ObjId_4;
    case 12: return &conn_DigitalInput_4_UP;
    case 13: return &conn_DigitalInput_4_DOWN;
    case 14: return &conn_u16ObjId_5;
    case 15: return &conn_DigitalInput_5_UP;
    case 16: return &conn_DigitalInput_5_DOWN;
    case 17: return &conn_u16ObjId_6;
    case 18: return &conn_DigitalInput_6_UP;
    case 19: return &conn_DigitalInput_6_DOWN;
    case 20: return &conn_u16ObjId_7;
    case 21: return &conn_DigitalInput_7_UP;
    case 22: return &conn_DigitalInput_7_DOWN;
    case 23: return &conn_u16ObjId_8;
    case 24: return &conn_DigitalInput_8_UP;
    case 25: return &conn_DigitalInput_8_DOWN;
  }
  return nullptr;
}

CDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_08_DualBool_LatchedDown::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

