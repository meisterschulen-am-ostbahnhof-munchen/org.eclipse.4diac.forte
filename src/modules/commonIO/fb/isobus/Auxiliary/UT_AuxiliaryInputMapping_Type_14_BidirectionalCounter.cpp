/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter
 *** Description: Type 14 BidirectionalCounter Count increases when turning in the encoders "increase" direction (analog opposite direction)
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter, g_nStringIdUT_AuxiliaryInputMapping_Type_14_BidirectionalCounter)

const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId_1, g_nStringIdCounterInput_1, g_nStringIdCalibrationInput_1, g_nStringIdu16ObjId_2, g_nStringIdCounterInput_2, g_nStringIdCalibrationInput_2, g_nStringIdu16ObjId_3, g_nStringIdCounterInput_3, g_nStringIdCalibrationInput_3, g_nStringIdu16ObjId_4, g_nStringIdCounterInput_4, g_nStringIdCalibrationInput_4, g_nStringIdu16ObjId_5, g_nStringIdCounterInput_5, g_nStringIdCalibrationInput_5, g_nStringIdu16ObjId_6, g_nStringIdCounterInput_6, g_nStringIdCalibrationInput_6, g_nStringIdu16ObjId_7, g_nStringIdCounterInput_7, g_nStringIdCalibrationInput_7, g_nStringIdu16ObjId_8, g_nStringIdCounterInput_8, g_nStringIdCalibrationInput_8};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  26, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId_1(nullptr),
    conn_CounterInput_1(nullptr),
    conn_CalibrationInput_1(nullptr),
    conn_u16ObjId_2(nullptr),
    conn_CounterInput_2(nullptr),
    conn_CalibrationInput_2(nullptr),
    conn_u16ObjId_3(nullptr),
    conn_CounterInput_3(nullptr),
    conn_CalibrationInput_3(nullptr),
    conn_u16ObjId_4(nullptr),
    conn_CounterInput_4(nullptr),
    conn_CalibrationInput_4(nullptr),
    conn_u16ObjId_5(nullptr),
    conn_CounterInput_5(nullptr),
    conn_CalibrationInput_5(nullptr),
    conn_u16ObjId_6(nullptr),
    conn_CounterInput_6(nullptr),
    conn_CalibrationInput_6(nullptr),
    conn_u16ObjId_7(nullptr),
    conn_CounterInput_7(nullptr),
    conn_CalibrationInput_7(nullptr),
    conn_u16ObjId_8(nullptr),
    conn_CounterInput_8(nullptr),
    conn_CalibrationInput_8(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId_1 = 0_UINT;
  var_CounterInput_1 = u""_WSTRING;
  var_CalibrationInput_1 = u""_WSTRING;
  var_u16ObjId_2 = 0_UINT;
  var_CounterInput_2 = u""_WSTRING;
  var_CalibrationInput_2 = u""_WSTRING;
  var_u16ObjId_3 = 0_UINT;
  var_CounterInput_3 = u""_WSTRING;
  var_CalibrationInput_3 = u""_WSTRING;
  var_u16ObjId_4 = 0_UINT;
  var_CounterInput_4 = u""_WSTRING;
  var_CalibrationInput_4 = u""_WSTRING;
  var_u16ObjId_5 = 0_UINT;
  var_CounterInput_5 = u""_WSTRING;
  var_CalibrationInput_5 = u""_WSTRING;
  var_u16ObjId_6 = 0_UINT;
  var_CounterInput_6 = u""_WSTRING;
  var_CalibrationInput_6 = u""_WSTRING;
  var_u16ObjId_7 = 0_UINT;
  var_CounterInput_7 = u""_WSTRING;
  var_CalibrationInput_7 = u""_WSTRING;
  var_u16ObjId_8 = 0_UINT;
  var_CounterInput_8 = u""_WSTRING;
  var_CalibrationInput_8 = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_1A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_1.getValue(),     var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_1B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_1.getValue(), var_u8SAMember, var_u16ObjId_1, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin1A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_2A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_2.getValue(),     var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_2B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_2.getValue(), var_u8SAMember, var_u16ObjId_2, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin2A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_3A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_3.getValue(),     var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_3B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_3.getValue(), var_u8SAMember, var_u16ObjId_3, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin3A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_4A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_4.getValue(),     var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_4B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_4.getValue(), var_u8SAMember, var_u16ObjId_4, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin4A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_5A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_5.getValue(),     var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_5B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_5.getValue(), var_u8SAMember, var_u16ObjId_5, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin5A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_6A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_6.getValue(),     var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_6B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_6.getValue(), var_u8SAMember, var_u16ObjId_6, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin6A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_7A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_7.getValue(),     var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_7B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_7.getValue(), var_u8SAMember, var_u16ObjId_7, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin7A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));
      handle_8A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CounterInput_8.getValue(),     var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::COUNTER));
      handle_8B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_CalibrationInput_8.getValue(), var_u8SAMember, var_u16ObjId_8, forte::core::io::IOMapper::In, CIEC_ANY::e_WORD, CommonIODeviceController::HandleType::AuxiliaryFunction_14, CommonIOEnums::PinNumber::Pin8A, CommonIOEnums::ThreePositionSwitch::CALIBRATION));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId_1, conn_u16ObjId_1);
      readData(3, var_CounterInput_1, conn_CounterInput_1);
      readData(4, var_CalibrationInput_1, conn_CalibrationInput_1);
      readData(5, var_u16ObjId_2, conn_u16ObjId_2);
      readData(6, var_CounterInput_2, conn_CounterInput_2);
      readData(7, var_CalibrationInput_2, conn_CalibrationInput_2);
      readData(8, var_u16ObjId_3, conn_u16ObjId_3);
      readData(9, var_CounterInput_3, conn_CounterInput_3);
      readData(10, var_CalibrationInput_3, conn_CalibrationInput_3);
      readData(11, var_u16ObjId_4, conn_u16ObjId_4);
      readData(12, var_CounterInput_4, conn_CounterInput_4);
      readData(13, var_CalibrationInput_4, conn_CalibrationInput_4);
      readData(14, var_u16ObjId_5, conn_u16ObjId_5);
      readData(15, var_CounterInput_5, conn_CounterInput_5);
      readData(16, var_CalibrationInput_5, conn_CalibrationInput_5);
      readData(17, var_u16ObjId_6, conn_u16ObjId_6);
      readData(18, var_CounterInput_6, conn_CounterInput_6);
      readData(19, var_CalibrationInput_6, conn_CalibrationInput_6);
      readData(20, var_u16ObjId_7, conn_u16ObjId_7);
      readData(21, var_CounterInput_7, conn_CounterInput_7);
      readData(22, var_CalibrationInput_7, conn_CalibrationInput_7);
      readData(23, var_u16ObjId_8, conn_u16ObjId_8);
      readData(24, var_CounterInput_8, conn_CounterInput_8);
      readData(25, var_CalibrationInput_8, conn_CalibrationInput_8);
      break;
    }
    default:
      break;
  }
}

void FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId_1;
    case 3: return &var_CounterInput_1;
    case 4: return &var_CalibrationInput_1;
    case 5: return &var_u16ObjId_2;
    case 6: return &var_CounterInput_2;
    case 7: return &var_CalibrationInput_2;
    case 8: return &var_u16ObjId_3;
    case 9: return &var_CounterInput_3;
    case 10: return &var_CalibrationInput_3;
    case 11: return &var_u16ObjId_4;
    case 12: return &var_CounterInput_4;
    case 13: return &var_CalibrationInput_4;
    case 14: return &var_u16ObjId_5;
    case 15: return &var_CounterInput_5;
    case 16: return &var_CalibrationInput_5;
    case 17: return &var_u16ObjId_6;
    case 18: return &var_CounterInput_6;
    case 19: return &var_CalibrationInput_6;
    case 20: return &var_u16ObjId_7;
    case 21: return &var_CounterInput_7;
    case 22: return &var_CalibrationInput_7;
    case 23: return &var_u16ObjId_8;
    case 24: return &var_CounterInput_8;
    case 25: return &var_CalibrationInput_8;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId_1;
    case 3: return &conn_CounterInput_1;
    case 4: return &conn_CalibrationInput_1;
    case 5: return &conn_u16ObjId_2;
    case 6: return &conn_CounterInput_2;
    case 7: return &conn_CalibrationInput_2;
    case 8: return &conn_u16ObjId_3;
    case 9: return &conn_CounterInput_3;
    case 10: return &conn_CalibrationInput_3;
    case 11: return &conn_u16ObjId_4;
    case 12: return &conn_CounterInput_4;
    case 13: return &conn_CalibrationInput_4;
    case 14: return &conn_u16ObjId_5;
    case 15: return &conn_CounterInput_5;
    case 16: return &conn_CalibrationInput_5;
    case 17: return &conn_u16ObjId_6;
    case 18: return &conn_CounterInput_6;
    case 19: return &conn_CalibrationInput_6;
    case 20: return &conn_u16ObjId_7;
    case 21: return &conn_CounterInput_7;
    case 22: return &conn_CalibrationInput_7;
    case 23: return &conn_u16ObjId_8;
    case 24: return &conn_CounterInput_8;
    case 25: return &conn_CalibrationInput_8;
  }
  return nullptr;
}

CDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_UT_AuxiliaryInputMapping_Type_14_BidirectionalCounter::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

