/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: UT_AuxiliaryInputMapping_Type_05_DualBool_BothLatched
 *** Description: Type 5 DualBool_BothLatched Three-position switch - latching in all positions
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_UT_AuxiliaryInputMapping_Type_05_DualBool_BothLatched final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_UT_AuxiliaryInputMapping_Type_05_DualBool_BothLatched)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  
  std::unique_ptr<forte::core::io::IOHandle> handle_1A;
  std::unique_ptr<forte::core::io::IOHandle> handle_1B;
  std::unique_ptr<forte::core::io::IOHandle> handle_2A;
  std::unique_ptr<forte::core::io::IOHandle> handle_2B;
  std::unique_ptr<forte::core::io::IOHandle> handle_3A;
  std::unique_ptr<forte::core::io::IOHandle> handle_3B;
  std::unique_ptr<forte::core::io::IOHandle> handle_4A;
  std::unique_ptr<forte::core::io::IOHandle> handle_4B;
  std::unique_ptr<forte::core::io::IOHandle> handle_5A;
  std::unique_ptr<forte::core::io::IOHandle> handle_5B;
  std::unique_ptr<forte::core::io::IOHandle> handle_6A;
  std::unique_ptr<forte::core::io::IOHandle> handle_6B;
  std::unique_ptr<forte::core::io::IOHandle> handle_7A;
  std::unique_ptr<forte::core::io::IOHandle> handle_7B;
  std::unique_ptr<forte::core::io::IOHandle> handle_8A;
  std::unique_ptr<forte::core::io::IOHandle> handle_8B;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_UT_AuxiliaryInputMapping_Type_05_DualBool_BothLatched(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId_1;
  CIEC_WSTRING var_DigitalInput_1_UP;
  CIEC_WSTRING var_DigitalInput_1_DOWN;
  CIEC_UINT var_u16ObjId_2;
  CIEC_WSTRING var_DigitalInput_2_UP;
  CIEC_WSTRING var_DigitalInput_2_DOWN;
  CIEC_UINT var_u16ObjId_3;
  CIEC_WSTRING var_DigitalInput_3_UP;
  CIEC_WSTRING var_DigitalInput_3_DOWN;
  CIEC_UINT var_u16ObjId_4;
  CIEC_WSTRING var_DigitalInput_4_UP;
  CIEC_WSTRING var_DigitalInput_4_DOWN;
  CIEC_UINT var_u16ObjId_5;
  CIEC_WSTRING var_DigitalInput_5_UP;
  CIEC_WSTRING var_DigitalInput_5_DOWN;
  CIEC_UINT var_u16ObjId_6;
  CIEC_WSTRING var_DigitalInput_6_UP;
  CIEC_WSTRING var_DigitalInput_6_DOWN;
  CIEC_UINT var_u16ObjId_7;
  CIEC_WSTRING var_DigitalInput_7_UP;
  CIEC_WSTRING var_DigitalInput_7_DOWN;
  CIEC_UINT var_u16ObjId_8;
  CIEC_WSTRING var_DigitalInput_8_UP;
  CIEC_WSTRING var_DigitalInput_8_DOWN;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId_1;
  CDataConnection *conn_DigitalInput_1_UP;
  CDataConnection *conn_DigitalInput_1_DOWN;
  CDataConnection *conn_u16ObjId_2;
  CDataConnection *conn_DigitalInput_2_UP;
  CDataConnection *conn_DigitalInput_2_DOWN;
  CDataConnection *conn_u16ObjId_3;
  CDataConnection *conn_DigitalInput_3_UP;
  CDataConnection *conn_DigitalInput_3_DOWN;
  CDataConnection *conn_u16ObjId_4;
  CDataConnection *conn_DigitalInput_4_UP;
  CDataConnection *conn_DigitalInput_4_DOWN;
  CDataConnection *conn_u16ObjId_5;
  CDataConnection *conn_DigitalInput_5_UP;
  CDataConnection *conn_DigitalInput_5_DOWN;
  CDataConnection *conn_u16ObjId_6;
  CDataConnection *conn_DigitalInput_6_UP;
  CDataConnection *conn_DigitalInput_6_DOWN;
  CDataConnection *conn_u16ObjId_7;
  CDataConnection *conn_DigitalInput_7_UP;
  CDataConnection *conn_DigitalInput_7_DOWN;
  CDataConnection *conn_u16ObjId_8;
  CDataConnection *conn_DigitalInput_8_UP;
  CDataConnection *conn_DigitalInput_8_DOWN;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paDigitalInput_1_UP, const CIEC_WSTRING &paDigitalInput_1_DOWN, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paDigitalInput_2_UP, const CIEC_WSTRING &paDigitalInput_2_DOWN, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paDigitalInput_3_UP, const CIEC_WSTRING &paDigitalInput_3_DOWN, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paDigitalInput_4_UP, const CIEC_WSTRING &paDigitalInput_4_DOWN, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paDigitalInput_5_UP, const CIEC_WSTRING &paDigitalInput_5_DOWN, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paDigitalInput_6_UP, const CIEC_WSTRING &paDigitalInput_6_DOWN, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paDigitalInput_7_UP, const CIEC_WSTRING &paDigitalInput_7_DOWN, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paDigitalInput_8_UP, const CIEC_WSTRING &paDigitalInput_8_DOWN, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId_1 = pau16ObjId_1;
    var_DigitalInput_1_UP = paDigitalInput_1_UP;
    var_DigitalInput_1_DOWN = paDigitalInput_1_DOWN;
    var_u16ObjId_2 = pau16ObjId_2;
    var_DigitalInput_2_UP = paDigitalInput_2_UP;
    var_DigitalInput_2_DOWN = paDigitalInput_2_DOWN;
    var_u16ObjId_3 = pau16ObjId_3;
    var_DigitalInput_3_UP = paDigitalInput_3_UP;
    var_DigitalInput_3_DOWN = paDigitalInput_3_DOWN;
    var_u16ObjId_4 = pau16ObjId_4;
    var_DigitalInput_4_UP = paDigitalInput_4_UP;
    var_DigitalInput_4_DOWN = paDigitalInput_4_DOWN;
    var_u16ObjId_5 = pau16ObjId_5;
    var_DigitalInput_5_UP = paDigitalInput_5_UP;
    var_DigitalInput_5_DOWN = paDigitalInput_5_DOWN;
    var_u16ObjId_6 = pau16ObjId_6;
    var_DigitalInput_6_UP = paDigitalInput_6_UP;
    var_DigitalInput_6_DOWN = paDigitalInput_6_DOWN;
    var_u16ObjId_7 = pau16ObjId_7;
    var_DigitalInput_7_UP = paDigitalInput_7_UP;
    var_DigitalInput_7_DOWN = paDigitalInput_7_DOWN;
    var_u16ObjId_8 = pau16ObjId_8;
    var_DigitalInput_8_UP = paDigitalInput_8_UP;
    var_DigitalInput_8_DOWN = paDigitalInput_8_DOWN;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId_1, const CIEC_WSTRING &paDigitalInput_1_UP, const CIEC_WSTRING &paDigitalInput_1_DOWN, const CIEC_UINT &pau16ObjId_2, const CIEC_WSTRING &paDigitalInput_2_UP, const CIEC_WSTRING &paDigitalInput_2_DOWN, const CIEC_UINT &pau16ObjId_3, const CIEC_WSTRING &paDigitalInput_3_UP, const CIEC_WSTRING &paDigitalInput_3_DOWN, const CIEC_UINT &pau16ObjId_4, const CIEC_WSTRING &paDigitalInput_4_UP, const CIEC_WSTRING &paDigitalInput_4_DOWN, const CIEC_UINT &pau16ObjId_5, const CIEC_WSTRING &paDigitalInput_5_UP, const CIEC_WSTRING &paDigitalInput_5_DOWN, const CIEC_UINT &pau16ObjId_6, const CIEC_WSTRING &paDigitalInput_6_UP, const CIEC_WSTRING &paDigitalInput_6_DOWN, const CIEC_UINT &pau16ObjId_7, const CIEC_WSTRING &paDigitalInput_7_UP, const CIEC_WSTRING &paDigitalInput_7_DOWN, const CIEC_UINT &pau16ObjId_8, const CIEC_WSTRING &paDigitalInput_8_UP, const CIEC_WSTRING &paDigitalInput_8_DOWN, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId_1, paDigitalInput_1_UP, paDigitalInput_1_DOWN, pau16ObjId_2, paDigitalInput_2_UP, paDigitalInput_2_DOWN, pau16ObjId_3, paDigitalInput_3_UP, paDigitalInput_3_DOWN, pau16ObjId_4, paDigitalInput_4_UP, paDigitalInput_4_DOWN, pau16ObjId_5, paDigitalInput_5_UP, paDigitalInput_5_DOWN, pau16ObjId_6, paDigitalInput_6_UP, paDigitalInput_6_DOWN, pau16ObjId_7, paDigitalInput_7_UP, paDigitalInput_7_DOWN, pau16ObjId_8, paDigitalInput_8_UP, paDigitalInput_8_DOWN, paQO, paSTATUS);
  }
};


