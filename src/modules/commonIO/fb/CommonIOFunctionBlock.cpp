/*
 * CommonIOFunctionBlock.cpp
 *
 *  Created on: 13.08.2022
 *      Author: franz
 */

#include "CommonIOFunctionBlock.h"
#include <extevhandlerhelper.h>


CommonIOFunctionBlock::CommonIOFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
        CStringDictionary::TStringId pa_nInstanceNameId):
    CEventSourceFB( pa_poSrcRes, pa_pstInterfaceSpec, pa_nInstanceNameId) {
}

CommonIOFunctionBlock::~CommonIOFunctionBlock() = default;





const CIEC_STRING CommonIOFunctionBlock::scmOK                 = "OK"_STRING;
const CIEC_STRING CommonIOFunctionBlock::scmqTimedOut          = "qTimedOut"_STRING;
const CIEC_STRING CommonIOFunctionBlock::scmNotInitialised     = "Not initialized"_STRING;




forte::core::io::IOHandle * CommonIOFunctionBlock::mapPin(
	std::string const &paId,
    CIEC_USINT &paU8SAMember,
    CIEC_UINT &paU16ObjId,
    forte::core::io::IOMapper::Direction paDirection,
    CIEC_ANY::EDataTypeID paDataType,
    CommonIODeviceController::HandleType paHandleType,
    CommonIOEnums::PinNumber paPin,
    CommonIOEnums::ThreePositionSwitch paSwitch
    )
{
  // Only initialize pins that have an id.
  if (false) {
    //noop
  } else if (paId.empty()) {
    DEVLOG_INFO("CommonIOFunctionBlock mapped pin -------- (ID empty)\n");
  } else if ((TForteUInt8)paU8SAMember == 0) {
    DEVLOG_INFO("CommonIOFunctionBlock mapped pin -------- (paU8SAMember = 0)\n");
  } else if ((TForteUInt16)paU16ObjId == 0) {
    DEVLOG_INFO("CommonIOFunctionBlock mapped pin -------- (paU16ObjId = 0)\n");
  } else {
    DEVLOG_INFO("CommonIOFunctionBlock mapped pin to ##%s##  paU8SAMember=%i   paU16ObjId=%i\n", paId, (TForteUInt8)paU8SAMember, (TForteUInt16)paU16ObjId);

    // Create a GPIO pin handle using the port struct to identify the MMIO port and
    // a bit mask to identify the pin.
    CommonIODeviceController::CommonIOHandleDescriptor descr(paId, paU8SAMember, paU16ObjId, paDirection, paDataType, paHandleType, paPin, paSwitch);

    CommonIODeviceController &ctrl     = getExtEvHandler<CommonIODeviceController>(*this);
    forte::core::io::IOHandle * handle = ctrl.initHandle(&descr);
    forte::core::io::IOMapper& mapper  = forte::core::io::IOMapper::getInstance();

    mapper.registerHandle(paId, handle);
    return handle;
  }
  return nullptr; //all other cases.
}




