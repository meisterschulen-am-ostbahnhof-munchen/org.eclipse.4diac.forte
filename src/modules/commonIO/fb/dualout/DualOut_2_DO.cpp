/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DualOut_2_DO
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "DualOut_2_DO.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "DualOut_2_DO_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_DualOut_2_DO, g_nStringIdDualOut_2_DO)

const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalOutput_Q1, g_nStringIdDigitalOutput_Q2};
const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_DualOut_2_DO::scmEIWith[] = {0, 1, 2, 3, 4, scmWithListDelimiter};
const TForteInt16 FORTE_DualOut_2_DO::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_DualOut_2_DO::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_DualOut_2_DO::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_DualOut_2_DO::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_DualOut_2_DO::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  5, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_DualOut_2_DO::FORTE_DualOut_2_DO(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_u16ObjId(65535_UINT),
    var_DigitalOutput_Q1(u"DigitalOutput_Q1"_WSTRING),
    var_DigitalOutput_Q2(u"DigitalOutput_Q2"_WSTRING),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalOutput_Q1(nullptr),
    conn_DigitalOutput_Q2(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_DualOut_2_DO::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalOutput_Q1 = u"DigitalOutput_Q1"_WSTRING;
  var_DigitalOutput_Q2 = u"DigitalOutput_Q2"_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_DualOut_2_DO::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;

      handle_Q1 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q1.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ1));
      handle_Q2 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q2.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ2));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_DualOut_2_DO::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalOutput_Q1, conn_DigitalOutput_Q1);
      readData(4, var_DigitalOutput_Q2, conn_DigitalOutput_Q2);
      break;
    }
    default:
      break;
  }
}

void FORTE_DualOut_2_DO::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_DualOut_2_DO::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalOutput_Q1;
    case 4: return &var_DigitalOutput_Q2;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DualOut_2_DO::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DualOut_2_DO::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_DualOut_2_DO::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_DualOut_2_DO::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalOutput_Q1;
    case 4: return &conn_DigitalOutput_Q2;
  }
  return nullptr;
}

CDataConnection *FORTE_DualOut_2_DO::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_DualOut_2_DO::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_DualOut_2_DO::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

