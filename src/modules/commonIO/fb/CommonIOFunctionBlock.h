/*
 * CommonIOFunctionBlock.h
 *
 *  Created on: 13.08.2022
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMON_IO_COMMON_IOFUNCTIONBLOCK_H_
#define SRC_MODULES_COMMON_IO_COMMON_IOFUNCTIONBLOCK_H_

#include <memory>
#include "funcbloc.h"
#include "core/io/mapper/io_mapper.h"
#include "handler/CommonIODeviceController.h"


#include <extevhan.h>
#include <esfb.h>
#include <resource.h>
class CommonIOFunctionBlock : public CEventSourceFB {

public:



    CommonIOFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
            CStringDictionary::TStringId pa_nInstanceNameId);

    ~CommonIOFunctionBlock() override;


    forte::core::io::IOHandle * mapPin(
    	std::string const &paId,
        CIEC_USINT &paU8SAMember,
        CIEC_UINT &paU16ObjId,
        forte::core::io::IOMapper::Direction paDirection,
        CIEC_ANY::EDataTypeID paDataType,
        CommonIODeviceController::HandleType paHandleType,
        CommonIOEnums::PinNumber paPin,
        CommonIOEnums::ThreePositionSwitch paSwitch = CommonIOEnums::ThreePositionSwitch::ThreePositionSwitch_Invalid
        );



protected:
    static const CIEC_STRING scmOK;
    static const CIEC_STRING scmqTimedOut;
    static const CIEC_STRING scmNotInitialised;
    //TODO avoid  16 Magic Number. !!!

};



#endif /* SRC_MODULES_COMMON_IO_COMMON_IOFUNCTIONBLOCK_H_ */
