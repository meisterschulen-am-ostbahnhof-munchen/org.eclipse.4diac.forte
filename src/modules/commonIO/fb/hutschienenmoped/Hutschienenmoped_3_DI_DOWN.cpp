/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Hutschienenmoped_3_DI_DOWN
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "Hutschienenmoped_3_DI_DOWN.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Hutschienenmoped_3_DI_DOWN_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_Hutschienenmoped_3_DI_DOWN, g_nStringIdHutschienenmoped_3_DI_DOWN)

const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalInput_I1, g_nStringIdDigitalInput_I2, g_nStringIdDigitalInput_I3};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_Hutschienenmoped_3_DI_DOWN::scmEIWith[] = {0, 1, 2, 3, 4, 5, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_3_DI_DOWN::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_Hutschienenmoped_3_DI_DOWN::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_3_DI_DOWN::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_3_DI_DOWN::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_Hutschienenmoped_3_DI_DOWN::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  6, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Hutschienenmoped_3_DI_DOWN::FORTE_Hutschienenmoped_3_DI_DOWN(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_u16ObjId(65535_UINT),
    var_DigitalInput_I1(u"DigitalInput_I1_DOWN"_WSTRING),
    var_DigitalInput_I2(u"DigitalInput_I2_DOWN"_WSTRING),
    var_DigitalInput_I3(u"DigitalInput_I3_DOWN"_WSTRING),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalInput_I1(nullptr),
    conn_DigitalInput_I2(nullptr),
    conn_DigitalInput_I3(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_Hutschienenmoped_3_DI_DOWN::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalInput_I1 = u"DigitalInput_I1_DOWN"_WSTRING;
  var_DigitalInput_I2 = u"DigitalInput_I2_DOWN"_WSTRING;
  var_DigitalInput_I3 = u"DigitalInput_I3_DOWN"_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_Hutschienenmoped_3_DI_DOWN::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_I1 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_I1.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_DOWN, CommonIOEnums::PinNumber::PinI1));
      handle_I2 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_I2.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_DOWN, CommonIOEnums::PinNumber::PinI2));
      handle_I3 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_I3.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_DOWN, CommonIOEnums::PinNumber::PinI3));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_Hutschienenmoped_3_DI_DOWN::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalInput_I1, conn_DigitalInput_I1);
      readData(4, var_DigitalInput_I2, conn_DigitalInput_I2);
      readData(5, var_DigitalInput_I3, conn_DigitalInput_I3);
      break;
    }
    default:
      break;
  }
}

void FORTE_Hutschienenmoped_3_DI_DOWN::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Hutschienenmoped_3_DI_DOWN::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalInput_I1;
    case 4: return &var_DigitalInput_I2;
    case 5: return &var_DigitalInput_I3;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_3_DI_DOWN::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_3_DI_DOWN::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Hutschienenmoped_3_DI_DOWN::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_Hutschienenmoped_3_DI_DOWN::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalInput_I1;
    case 4: return &conn_DigitalInput_I2;
    case 5: return &conn_DigitalInput_I3;
  }
  return nullptr;
}

CDataConnection *FORTE_Hutschienenmoped_3_DI_DOWN::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Hutschienenmoped_3_DI_DOWN::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Hutschienenmoped_3_DI_DOWN::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

