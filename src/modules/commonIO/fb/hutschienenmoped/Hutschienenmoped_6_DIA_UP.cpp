/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Hutschienenmoped_6_DIA_UP
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "Hutschienenmoped_6_DIA_UP.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Hutschienenmoped_6_DIA_UP_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_Hutschienenmoped_6_DIA_UP, g_nStringIdHutschienenmoped_6_DIA_UP)

const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalInput_IA1, g_nStringIdDigitalInput_IA2, g_nStringIdDigitalInput_IA3, g_nStringIdDigitalInput_IA4, g_nStringIdDigitalInput_IA5, g_nStringIdDigitalInput_IA6};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_Hutschienenmoped_6_DIA_UP::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_6_DIA_UP::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_Hutschienenmoped_6_DIA_UP::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_6_DIA_UP::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_6_DIA_UP::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_Hutschienenmoped_6_DIA_UP::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  9, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Hutschienenmoped_6_DIA_UP::FORTE_Hutschienenmoped_6_DIA_UP(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_u16ObjId(65535_UINT),
    var_DigitalInput_IA1(u"DigitalInput_IA1_UP"_WSTRING),
    var_DigitalInput_IA2(u"DigitalInput_IA2_UP"_WSTRING),
    var_DigitalInput_IA3(u"DigitalInput_IA3_UP"_WSTRING),
    var_DigitalInput_IA4(u"DigitalInput_IA4_UP"_WSTRING),
    var_DigitalInput_IA5(u"DigitalInput_IA5_UP"_WSTRING),
    var_DigitalInput_IA6(u"DigitalInput_IA6_UP"_WSTRING),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalInput_IA1(nullptr),
    conn_DigitalInput_IA2(nullptr),
    conn_DigitalInput_IA3(nullptr),
    conn_DigitalInput_IA4(nullptr),
    conn_DigitalInput_IA5(nullptr),
    conn_DigitalInput_IA6(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_Hutschienenmoped_6_DIA_UP::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalInput_IA1 = u"DigitalInput_IA1_UP"_WSTRING;
  var_DigitalInput_IA2 = u"DigitalInput_IA2_UP"_WSTRING;
  var_DigitalInput_IA3 = u"DigitalInput_IA3_UP"_WSTRING;
  var_DigitalInput_IA4 = u"DigitalInput_IA4_UP"_WSTRING;
  var_DigitalInput_IA5 = u"DigitalInput_IA5_UP"_WSTRING;
  var_DigitalInput_IA6 = u"DigitalInput_IA6_UP"_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_Hutschienenmoped_6_DIA_UP::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_IA1 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA1.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA1));
      handle_IA2 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA2.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA2));
      handle_IA3 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA3.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA3));
      handle_IA4 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA4.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA4));
      handle_IA5 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA5.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA5));
      handle_IA6 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_IA6.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_Max, CommonIODeviceController::HandleType::ESP32Button_UP, CommonIOEnums::PinNumber::PinIA6));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_Hutschienenmoped_6_DIA_UP::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalInput_IA1, conn_DigitalInput_IA1);
      readData(4, var_DigitalInput_IA2, conn_DigitalInput_IA2);
      readData(5, var_DigitalInput_IA3, conn_DigitalInput_IA3);
      readData(6, var_DigitalInput_IA4, conn_DigitalInput_IA4);
      readData(7, var_DigitalInput_IA5, conn_DigitalInput_IA5);
      readData(8, var_DigitalInput_IA6, conn_DigitalInput_IA6);
      break;
    }
    default:
      break;
  }
}

void FORTE_Hutschienenmoped_6_DIA_UP::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Hutschienenmoped_6_DIA_UP::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalInput_IA1;
    case 4: return &var_DigitalInput_IA2;
    case 5: return &var_DigitalInput_IA3;
    case 6: return &var_DigitalInput_IA4;
    case 7: return &var_DigitalInput_IA5;
    case 8: return &var_DigitalInput_IA6;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_6_DIA_UP::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_6_DIA_UP::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Hutschienenmoped_6_DIA_UP::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_Hutschienenmoped_6_DIA_UP::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalInput_IA1;
    case 4: return &conn_DigitalInput_IA2;
    case 5: return &conn_DigitalInput_IA3;
    case 6: return &conn_DigitalInput_IA4;
    case 7: return &conn_DigitalInput_IA5;
    case 8: return &conn_DigitalInput_IA6;
  }
  return nullptr;
}

CDataConnection *FORTE_Hutschienenmoped_6_DIA_UP::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Hutschienenmoped_6_DIA_UP::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Hutschienenmoped_6_DIA_UP::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

