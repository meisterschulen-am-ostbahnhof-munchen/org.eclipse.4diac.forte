/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Hutschienenmoped_6_DIA_LONG_PRESS_HOLD
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_Hutschienenmoped_6_DIA_LONG_PRESS_HOLD final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Hutschienenmoped_6_DIA_LONG_PRESS_HOLD)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  
  std::unique_ptr<forte::core::io::IOHandle> handle_IA1;
  std::unique_ptr<forte::core::io::IOHandle> handle_IA2;
  std::unique_ptr<forte::core::io::IOHandle> handle_IA3;
  std::unique_ptr<forte::core::io::IOHandle> handle_IA4;
  std::unique_ptr<forte::core::io::IOHandle> handle_IA5;
  std::unique_ptr<forte::core::io::IOHandle> handle_IA6;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Hutschienenmoped_6_DIA_LONG_PRESS_HOLD(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId;
  CIEC_WSTRING var_DigitalInput_IA1;
  CIEC_WSTRING var_DigitalInput_IA2;
  CIEC_WSTRING var_DigitalInput_IA3;
  CIEC_WSTRING var_DigitalInput_IA4;
  CIEC_WSTRING var_DigitalInput_IA5;
  CIEC_WSTRING var_DigitalInput_IA6;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_DigitalInput_IA1;
  CDataConnection *conn_DigitalInput_IA2;
  CDataConnection *conn_DigitalInput_IA3;
  CDataConnection *conn_DigitalInput_IA4;
  CDataConnection *conn_DigitalInput_IA5;
  CDataConnection *conn_DigitalInput_IA6;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalInput_IA1, const CIEC_WSTRING &paDigitalInput_IA2, const CIEC_WSTRING &paDigitalInput_IA3, const CIEC_WSTRING &paDigitalInput_IA4, const CIEC_WSTRING &paDigitalInput_IA5, const CIEC_WSTRING &paDigitalInput_IA6, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId = pau16ObjId;
    var_DigitalInput_IA1 = paDigitalInput_IA1;
    var_DigitalInput_IA2 = paDigitalInput_IA2;
    var_DigitalInput_IA3 = paDigitalInput_IA3;
    var_DigitalInput_IA4 = paDigitalInput_IA4;
    var_DigitalInput_IA5 = paDigitalInput_IA5;
    var_DigitalInput_IA6 = paDigitalInput_IA6;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalInput_IA1, const CIEC_WSTRING &paDigitalInput_IA2, const CIEC_WSTRING &paDigitalInput_IA3, const CIEC_WSTRING &paDigitalInput_IA4, const CIEC_WSTRING &paDigitalInput_IA5, const CIEC_WSTRING &paDigitalInput_IA6, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId, paDigitalInput_IA1, paDigitalInput_IA2, paDigitalInput_IA3, paDigitalInput_IA4, paDigitalInput_IA5, paDigitalInput_IA6, paQO, paSTATUS);
  }
};


