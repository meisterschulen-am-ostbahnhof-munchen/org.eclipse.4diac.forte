/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Hutschienenmoped_4_DO
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "Hutschienenmoped_4_DO.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Hutschienenmoped_4_DO_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_Hutschienenmoped_4_DO, g_nStringIdHutschienenmoped_4_DO)

const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalOutput_Q1, g_nStringIdDigitalOutput_Q2, g_nStringIdDigitalOutput_Q3, g_nStringIdDigitalOutput_Q4};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_Hutschienenmoped_4_DO::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_4_DO::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_Hutschienenmoped_4_DO::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Hutschienenmoped_4_DO::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Hutschienenmoped_4_DO::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_Hutschienenmoped_4_DO::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  7, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Hutschienenmoped_4_DO::FORTE_Hutschienenmoped_4_DO(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(255_USINT),
    var_u16ObjId(65535_UINT),
    var_DigitalOutput_Q1(u"DigitalOutput_Q1"_WSTRING),
    var_DigitalOutput_Q2(u"DigitalOutput_Q2"_WSTRING),
    var_DigitalOutput_Q3(u"DigitalOutput_Q3"_WSTRING),
    var_DigitalOutput_Q4(u"DigitalOutput_Q4"_WSTRING),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalOutput_Q1(nullptr),
    conn_DigitalOutput_Q2(nullptr),
    conn_DigitalOutput_Q3(nullptr),
    conn_DigitalOutput_Q4(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_Hutschienenmoped_4_DO::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 255_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalOutput_Q1 = u"DigitalOutput_Q1"_WSTRING;
  var_DigitalOutput_Q2 = u"DigitalOutput_Q2"_WSTRING;
  var_DigitalOutput_Q3 = u"DigitalOutput_Q3"_WSTRING;
  var_DigitalOutput_Q4 = u"DigitalOutput_Q4"_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_Hutschienenmoped_4_DO::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;

      handle_Q1 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q1.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ1));
      handle_Q2 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q2.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ2));
      handle_Q3 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q3.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ3));
      handle_Q4 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_Q4.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::ESP32DigitalO, CommonIOEnums::PinNumber::PinQ4));

      sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_Hutschienenmoped_4_DO::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalOutput_Q1, conn_DigitalOutput_Q1);
      readData(4, var_DigitalOutput_Q2, conn_DigitalOutput_Q2);
      readData(5, var_DigitalOutput_Q3, conn_DigitalOutput_Q3);
      readData(6, var_DigitalOutput_Q4, conn_DigitalOutput_Q4);
      break;
    }
    default:
      break;
  }
}

void FORTE_Hutschienenmoped_4_DO::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Hutschienenmoped_4_DO::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalOutput_Q1;
    case 4: return &var_DigitalOutput_Q2;
    case 5: return &var_DigitalOutput_Q3;
    case 6: return &var_DigitalOutput_Q4;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_4_DO::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Hutschienenmoped_4_DO::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Hutschienenmoped_4_DO::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_Hutschienenmoped_4_DO::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalOutput_Q1;
    case 4: return &conn_DigitalOutput_Q2;
    case 5: return &conn_DigitalOutput_Q3;
    case 6: return &conn_DigitalOutput_Q4;
  }
  return nullptr;
}

CDataConnection *FORTE_Hutschienenmoped_4_DO::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Hutschienenmoped_4_DO::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Hutschienenmoped_4_DO::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

