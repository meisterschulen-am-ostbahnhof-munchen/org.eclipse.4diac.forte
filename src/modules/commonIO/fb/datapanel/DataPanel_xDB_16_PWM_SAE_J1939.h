/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DataPanel_xDB_16_PWM_SAE_J1939
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "DataPanelFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_DataPanel_xDB_16_PWM_SAE_J1939 final : public DataPanelFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_DataPanel_xDB_16_PWM_SAE_J1939)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_DataPanel_xDB_16_PWM_SAE_J1939(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId;
  CIEC_WSTRING var_DigitalOutput_1A;
  CIEC_WSTRING var_DigitalOutput_1B;
  CIEC_WSTRING var_DigitalOutput_2A;
  CIEC_WSTRING var_DigitalOutput_2B;
  CIEC_WSTRING var_DigitalOutput_3A;
  CIEC_WSTRING var_DigitalOutput_3B;
  CIEC_WSTRING var_DigitalOutput_4A;
  CIEC_WSTRING var_DigitalOutput_4B;
  CIEC_WSTRING var_DigitalOutput_5A;
  CIEC_WSTRING var_DigitalOutput_5B;
  CIEC_WSTRING var_DigitalOutput_6A;
  CIEC_WSTRING var_DigitalOutput_6B;
  CIEC_WSTRING var_DigitalOutput_7A;
  CIEC_WSTRING var_DigitalOutput_7B;
  CIEC_WSTRING var_DigitalOutput_8A;
  CIEC_WSTRING var_DigitalOutput_8B;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_DigitalOutput_1A;
  CDataConnection *conn_DigitalOutput_1B;
  CDataConnection *conn_DigitalOutput_2A;
  CDataConnection *conn_DigitalOutput_2B;
  CDataConnection *conn_DigitalOutput_3A;
  CDataConnection *conn_DigitalOutput_3B;
  CDataConnection *conn_DigitalOutput_4A;
  CDataConnection *conn_DigitalOutput_4B;
  CDataConnection *conn_DigitalOutput_5A;
  CDataConnection *conn_DigitalOutput_5B;
  CDataConnection *conn_DigitalOutput_6A;
  CDataConnection *conn_DigitalOutput_6B;
  CDataConnection *conn_DigitalOutput_7A;
  CDataConnection *conn_DigitalOutput_7B;
  CDataConnection *conn_DigitalOutput_8A;
  CDataConnection *conn_DigitalOutput_8B;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalOutput_1A, const CIEC_WSTRING &paDigitalOutput_1B, const CIEC_WSTRING &paDigitalOutput_2A, const CIEC_WSTRING &paDigitalOutput_2B, const CIEC_WSTRING &paDigitalOutput_3A, const CIEC_WSTRING &paDigitalOutput_3B, const CIEC_WSTRING &paDigitalOutput_4A, const CIEC_WSTRING &paDigitalOutput_4B, const CIEC_WSTRING &paDigitalOutput_5A, const CIEC_WSTRING &paDigitalOutput_5B, const CIEC_WSTRING &paDigitalOutput_6A, const CIEC_WSTRING &paDigitalOutput_6B, const CIEC_WSTRING &paDigitalOutput_7A, const CIEC_WSTRING &paDigitalOutput_7B, const CIEC_WSTRING &paDigitalOutput_8A, const CIEC_WSTRING &paDigitalOutput_8B, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId = pau16ObjId;
    var_DigitalOutput_1A = paDigitalOutput_1A;
    var_DigitalOutput_1B = paDigitalOutput_1B;
    var_DigitalOutput_2A = paDigitalOutput_2A;
    var_DigitalOutput_2B = paDigitalOutput_2B;
    var_DigitalOutput_3A = paDigitalOutput_3A;
    var_DigitalOutput_3B = paDigitalOutput_3B;
    var_DigitalOutput_4A = paDigitalOutput_4A;
    var_DigitalOutput_4B = paDigitalOutput_4B;
    var_DigitalOutput_5A = paDigitalOutput_5A;
    var_DigitalOutput_5B = paDigitalOutput_5B;
    var_DigitalOutput_6A = paDigitalOutput_6A;
    var_DigitalOutput_6B = paDigitalOutput_6B;
    var_DigitalOutput_7A = paDigitalOutput_7A;
    var_DigitalOutput_7B = paDigitalOutput_7B;
    var_DigitalOutput_8A = paDigitalOutput_8A;
    var_DigitalOutput_8B = paDigitalOutput_8B;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalOutput_1A, const CIEC_WSTRING &paDigitalOutput_1B, const CIEC_WSTRING &paDigitalOutput_2A, const CIEC_WSTRING &paDigitalOutput_2B, const CIEC_WSTRING &paDigitalOutput_3A, const CIEC_WSTRING &paDigitalOutput_3B, const CIEC_WSTRING &paDigitalOutput_4A, const CIEC_WSTRING &paDigitalOutput_4B, const CIEC_WSTRING &paDigitalOutput_5A, const CIEC_WSTRING &paDigitalOutput_5B, const CIEC_WSTRING &paDigitalOutput_6A, const CIEC_WSTRING &paDigitalOutput_6B, const CIEC_WSTRING &paDigitalOutput_7A, const CIEC_WSTRING &paDigitalOutput_7B, const CIEC_WSTRING &paDigitalOutput_8A, const CIEC_WSTRING &paDigitalOutput_8B, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId, paDigitalOutput_1A, paDigitalOutput_1B, paDigitalOutput_2A, paDigitalOutput_2B, paDigitalOutput_3A, paDigitalOutput_3B, paDigitalOutput_4A, paDigitalOutput_4B, paDigitalOutput_5A, paDigitalOutput_5B, paDigitalOutput_6A, paDigitalOutput_6B, paDigitalOutput_7A, paDigitalOutput_7B, paDigitalOutput_8A, paDigitalOutput_8B, paQO, paSTATUS);
  }
};


