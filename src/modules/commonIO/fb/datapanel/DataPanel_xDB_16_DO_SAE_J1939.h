/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DataPanel_xDB_16_DO_SAE_J1939
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "DataPanelFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_DataPanel_xDB_16_DO_SAE_J1939 final : public DataPanelFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_DataPanel_xDB_16_DO_SAE_J1939)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  
  std::unique_ptr<forte::core::io::IOHandle> handle_1A;
  std::unique_ptr<forte::core::io::IOHandle> handle_1B;
  std::unique_ptr<forte::core::io::IOHandle> handle_2A;
  std::unique_ptr<forte::core::io::IOHandle> handle_2B;
  std::unique_ptr<forte::core::io::IOHandle> handle_3A;
  std::unique_ptr<forte::core::io::IOHandle> handle_3B;
  std::unique_ptr<forte::core::io::IOHandle> handle_4A;
  std::unique_ptr<forte::core::io::IOHandle> handle_4B;
  std::unique_ptr<forte::core::io::IOHandle> handle_5A;
  std::unique_ptr<forte::core::io::IOHandle> handle_5B;
  std::unique_ptr<forte::core::io::IOHandle> handle_6A;
  std::unique_ptr<forte::core::io::IOHandle> handle_6B;
  std::unique_ptr<forte::core::io::IOHandle> handle_7A;
  std::unique_ptr<forte::core::io::IOHandle> handle_7B;
  std::unique_ptr<forte::core::io::IOHandle> handle_8A;
  std::unique_ptr<forte::core::io::IOHandle> handle_8B;

  ISO_USER_PARAM_T userParam;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_DataPanel_xDB_16_DO_SAE_J1939(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId;
  CIEC_WSTRING var_DigitalOutput_1A;
  CIEC_WSTRING var_DigitalOutput_1B;
  CIEC_WSTRING var_DigitalOutput_2A;
  CIEC_WSTRING var_DigitalOutput_2B;
  CIEC_WSTRING var_DigitalOutput_3A;
  CIEC_WSTRING var_DigitalOutput_3B;
  CIEC_WSTRING var_DigitalOutput_4A;
  CIEC_WSTRING var_DigitalOutput_4B;
  CIEC_WSTRING var_DigitalOutput_5A;
  CIEC_WSTRING var_DigitalOutput_5B;
  CIEC_WSTRING var_DigitalOutput_6A;
  CIEC_WSTRING var_DigitalOutput_6B;
  CIEC_WSTRING var_DigitalOutput_7A;
  CIEC_WSTRING var_DigitalOutput_7B;
  CIEC_WSTRING var_DigitalOutput_8A;
  CIEC_WSTRING var_DigitalOutput_8B;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;
  CIEC_BOOL var_qTimedOut;
  CIEC_USINT var_Version;
  CIEC_USINT var_Revision;
  CIEC_USINT var_Fault_Code;
  CIEC_USINT var_User_ID;
  CIEC_USINT var_Hardware_Version;
  CIEC_BOOL var_Status_01;
  CIEC_BOOL var_Status_02;
  CIEC_BOOL var_Status_03;
  CIEC_BOOL var_Status_04;
  CIEC_BOOL var_Status_05;
  CIEC_BOOL var_Status_06;
  CIEC_BOOL var_Status_07;
  CIEC_BOOL var_Status_08;
  CIEC_BOOL var_Status_09;
  CIEC_BOOL var_Status_10;
  CIEC_BOOL var_Status_11;
  CIEC_BOOL var_Status_12;
  CIEC_BOOL var_Status_13;
  CIEC_BOOL var_Status_14;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;
  CIEC_BOOL var_conn_qTimedOut;
  CIEC_USINT var_conn_Version;
  CIEC_USINT var_conn_Revision;
  CIEC_USINT var_conn_Fault_Code;
  CIEC_USINT var_conn_User_ID;
  CIEC_USINT var_conn_Hardware_Version;
  CIEC_BOOL var_conn_Status_01;
  CIEC_BOOL var_conn_Status_02;
  CIEC_BOOL var_conn_Status_03;
  CIEC_BOOL var_conn_Status_04;
  CIEC_BOOL var_conn_Status_05;
  CIEC_BOOL var_conn_Status_06;
  CIEC_BOOL var_conn_Status_07;
  CIEC_BOOL var_conn_Status_08;
  CIEC_BOOL var_conn_Status_09;
  CIEC_BOOL var_conn_Status_10;
  CIEC_BOOL var_conn_Status_11;
  CIEC_BOOL var_conn_Status_12;
  CIEC_BOOL var_conn_Status_13;
  CIEC_BOOL var_conn_Status_14;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_DigitalOutput_1A;
  CDataConnection *conn_DigitalOutput_1B;
  CDataConnection *conn_DigitalOutput_2A;
  CDataConnection *conn_DigitalOutput_2B;
  CDataConnection *conn_DigitalOutput_3A;
  CDataConnection *conn_DigitalOutput_3B;
  CDataConnection *conn_DigitalOutput_4A;
  CDataConnection *conn_DigitalOutput_4B;
  CDataConnection *conn_DigitalOutput_5A;
  CDataConnection *conn_DigitalOutput_5B;
  CDataConnection *conn_DigitalOutput_6A;
  CDataConnection *conn_DigitalOutput_6B;
  CDataConnection *conn_DigitalOutput_7A;
  CDataConnection *conn_DigitalOutput_7B;
  CDataConnection *conn_DigitalOutput_8A;
  CDataConnection *conn_DigitalOutput_8B;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;
  CDataConnection conn_qTimedOut;
  CDataConnection conn_Version;
  CDataConnection conn_Revision;
  CDataConnection conn_Fault_Code;
  CDataConnection conn_User_ID;
  CDataConnection conn_Hardware_Version;
  CDataConnection conn_Status_01;
  CDataConnection conn_Status_02;
  CDataConnection conn_Status_03;
  CDataConnection conn_Status_04;
  CDataConnection conn_Status_05;
  CDataConnection conn_Status_06;
  CDataConnection conn_Status_07;
  CDataConnection conn_Status_08;
  CDataConnection conn_Status_09;
  CDataConnection conn_Status_10;
  CDataConnection conn_Status_11;
  CDataConnection conn_Status_12;
  CDataConnection conn_Status_13;
  CDataConnection conn_Status_14;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalOutput_1A, const CIEC_WSTRING &paDigitalOutput_1B, const CIEC_WSTRING &paDigitalOutput_2A, const CIEC_WSTRING &paDigitalOutput_2B, const CIEC_WSTRING &paDigitalOutput_3A, const CIEC_WSTRING &paDigitalOutput_3B, const CIEC_WSTRING &paDigitalOutput_4A, const CIEC_WSTRING &paDigitalOutput_4B, const CIEC_WSTRING &paDigitalOutput_5A, const CIEC_WSTRING &paDigitalOutput_5B, const CIEC_WSTRING &paDigitalOutput_6A, const CIEC_WSTRING &paDigitalOutput_6B, const CIEC_WSTRING &paDigitalOutput_7A, const CIEC_WSTRING &paDigitalOutput_7B, const CIEC_WSTRING &paDigitalOutput_8A, const CIEC_WSTRING &paDigitalOutput_8B, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS, CIEC_BOOL &paqTimedOut, CIEC_USINT &paVersion, CIEC_USINT &paRevision, CIEC_USINT &paFault_Code, CIEC_USINT &paUser_ID, CIEC_USINT &paHardware_Version, CIEC_BOOL &paStatus_01, CIEC_BOOL &paStatus_02, CIEC_BOOL &paStatus_03, CIEC_BOOL &paStatus_04, CIEC_BOOL &paStatus_05, CIEC_BOOL &paStatus_06, CIEC_BOOL &paStatus_07, CIEC_BOOL &paStatus_08, CIEC_BOOL &paStatus_09, CIEC_BOOL &paStatus_10, CIEC_BOOL &paStatus_11, CIEC_BOOL &paStatus_12, CIEC_BOOL &paStatus_13, CIEC_BOOL &paStatus_14) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId = pau16ObjId;
    var_DigitalOutput_1A = paDigitalOutput_1A;
    var_DigitalOutput_1B = paDigitalOutput_1B;
    var_DigitalOutput_2A = paDigitalOutput_2A;
    var_DigitalOutput_2B = paDigitalOutput_2B;
    var_DigitalOutput_3A = paDigitalOutput_3A;
    var_DigitalOutput_3B = paDigitalOutput_3B;
    var_DigitalOutput_4A = paDigitalOutput_4A;
    var_DigitalOutput_4B = paDigitalOutput_4B;
    var_DigitalOutput_5A = paDigitalOutput_5A;
    var_DigitalOutput_5B = paDigitalOutput_5B;
    var_DigitalOutput_6A = paDigitalOutput_6A;
    var_DigitalOutput_6B = paDigitalOutput_6B;
    var_DigitalOutput_7A = paDigitalOutput_7A;
    var_DigitalOutput_7B = paDigitalOutput_7B;
    var_DigitalOutput_8A = paDigitalOutput_8A;
    var_DigitalOutput_8B = paDigitalOutput_8B;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
    paqTimedOut = var_qTimedOut;
    paVersion = var_Version;
    paRevision = var_Revision;
    paFault_Code = var_Fault_Code;
    paUser_ID = var_User_ID;
    paHardware_Version = var_Hardware_Version;
    paStatus_01 = var_Status_01;
    paStatus_02 = var_Status_02;
    paStatus_03 = var_Status_03;
    paStatus_04 = var_Status_04;
    paStatus_05 = var_Status_05;
    paStatus_06 = var_Status_06;
    paStatus_07 = var_Status_07;
    paStatus_08 = var_Status_08;
    paStatus_09 = var_Status_09;
    paStatus_10 = var_Status_10;
    paStatus_11 = var_Status_11;
    paStatus_12 = var_Status_12;
    paStatus_13 = var_Status_13;
    paStatus_14 = var_Status_14;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalOutput_1A, const CIEC_WSTRING &paDigitalOutput_1B, const CIEC_WSTRING &paDigitalOutput_2A, const CIEC_WSTRING &paDigitalOutput_2B, const CIEC_WSTRING &paDigitalOutput_3A, const CIEC_WSTRING &paDigitalOutput_3B, const CIEC_WSTRING &paDigitalOutput_4A, const CIEC_WSTRING &paDigitalOutput_4B, const CIEC_WSTRING &paDigitalOutput_5A, const CIEC_WSTRING &paDigitalOutput_5B, const CIEC_WSTRING &paDigitalOutput_6A, const CIEC_WSTRING &paDigitalOutput_6B, const CIEC_WSTRING &paDigitalOutput_7A, const CIEC_WSTRING &paDigitalOutput_7B, const CIEC_WSTRING &paDigitalOutput_8A, const CIEC_WSTRING &paDigitalOutput_8B, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS, CIEC_BOOL &paqTimedOut, CIEC_USINT &paVersion, CIEC_USINT &paRevision, CIEC_USINT &paFault_Code, CIEC_USINT &paUser_ID, CIEC_USINT &paHardware_Version, CIEC_BOOL &paStatus_01, CIEC_BOOL &paStatus_02, CIEC_BOOL &paStatus_03, CIEC_BOOL &paStatus_04, CIEC_BOOL &paStatus_05, CIEC_BOOL &paStatus_06, CIEC_BOOL &paStatus_07, CIEC_BOOL &paStatus_08, CIEC_BOOL &paStatus_09, CIEC_BOOL &paStatus_10, CIEC_BOOL &paStatus_11, CIEC_BOOL &paStatus_12, CIEC_BOOL &paStatus_13, CIEC_BOOL &paStatus_14) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId, paDigitalOutput_1A, paDigitalOutput_1B, paDigitalOutput_2A, paDigitalOutput_2B, paDigitalOutput_3A, paDigitalOutput_3B, paDigitalOutput_4A, paDigitalOutput_4B, paDigitalOutput_5A, paDigitalOutput_5B, paDigitalOutput_6A, paDigitalOutput_6B, paDigitalOutput_7A, paDigitalOutput_7B, paDigitalOutput_8A, paDigitalOutput_8B, paQO, paSTATUS, paqTimedOut, paVersion, paRevision, paFault_Code, paUser_ID, paHardware_Version, paStatus_01, paStatus_02, paStatus_03, paStatus_04, paStatus_05, paStatus_06, paStatus_07, paStatus_08, paStatus_09, paStatus_10, paStatus_11, paStatus_12, paStatus_13, paStatus_14);
  }
};


