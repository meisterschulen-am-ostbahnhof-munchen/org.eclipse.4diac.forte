/*
 * DataPanelFunctionBlock.h
 *
 *  Created on: 20.08.2022
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMONIO_FB_DATAPANEL_DATAPANELFUNCTIONBLOCK_H_
#define SRC_MODULES_COMMONIO_FB_DATAPANEL_DATAPANELFUNCTIONBLOCK_H_

#include <memory>
#include "funcbloc.h"
#include "core/io/mapper/io_mapper.h"
#include "handler/CommonIODeviceController.h"
#include "CommonIOFunctionBlock.h"


#include <extevhan.h>
#include <esfb.h>
#include <resource.h>

#include "IsoDef.h"

class DataPanelFunctionBlock : public CommonIOFunctionBlock  {

public:



    DataPanelFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
            CStringDictionary::TStringId pa_nInstanceNameId);

    ~DataPanelFunctionBlock() override;


    class CbDataPanelFunctionBlockHandler : public CExternalEventHandler{
    DECLARE_HANDLER(CbDataPanelFunctionBlockHandler)
      ;
      public:
        //Static Callback
        static void CbPGNReceiveDataPanelSt2(const PGNDAT_T* psData);
        //in-Class Callback
        void CbPGNReceiveDataPanelSt3(const PGNDAT_T* psData);

        /* functions needed for the external event handler interface */
        void enableHandler() override;
        void disableHandler() override;
        void setPriority(int paPriority) override;
        int getPriority() const override;
      private:
        static  DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler* g_vth;  //Pointer to instance
    };


protected:
    virtual void CbPGNReceiveDataPanelSt4(const PGNDAT_T *psData) = 0;



        static DataPanelFunctionBlock* g_vts[16];  //Pointer to instance
};



#endif /* SRC_MODULES_COMMONIO_FB_DATAPANEL_DATAPANELFUNCTIONBLOCK_H_ */
