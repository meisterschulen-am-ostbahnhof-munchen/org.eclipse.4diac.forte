/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DataPanel_xDB_16_DO_SAE_J1939
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "DataPanel_xDB_16_DO_SAE_J1939.h"
#include "DatapanelAddresses.h"
#include "DatapanelStatus.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "DataPanel_xDB_16_DO_SAE_J1939_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_DataPanel_xDB_16_DO_SAE_J1939, g_nStringIdDataPanel_xDB_16_DO_SAE_J1939)

const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalOutput_1A, g_nStringIdDigitalOutput_1B, g_nStringIdDigitalOutput_2A, g_nStringIdDigitalOutput_2B, g_nStringIdDigitalOutput_3A, g_nStringIdDigitalOutput_3B, g_nStringIdDigitalOutput_4A, g_nStringIdDigitalOutput_4B, g_nStringIdDigitalOutput_5A, g_nStringIdDigitalOutput_5B, g_nStringIdDigitalOutput_6A, g_nStringIdDigitalOutput_6B, g_nStringIdDigitalOutput_7A, g_nStringIdDigitalOutput_7B, g_nStringIdDigitalOutput_8A, g_nStringIdDigitalOutput_8B};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS, g_nStringIdqTimedOut, g_nStringIdVersion, g_nStringIdRevision, g_nStringIdFault_Code, g_nStringIdUser_ID, g_nStringIdHardware_Version, g_nStringIdStatus_01, g_nStringIdStatus_02, g_nStringIdStatus_03, g_nStringIdStatus_04, g_nStringIdStatus_05, g_nStringIdStatus_06, g_nStringIdStatus_07, g_nStringIdStatus_08, g_nStringIdStatus_09, g_nStringIdStatus_10, g_nStringIdStatus_11, g_nStringIdStatus_12, g_nStringIdStatus_13, g_nStringIdStatus_14};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING, g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL, g_nStringIdBOOL};
const TDataIOID FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_DataPanel_xDB_16_DO_SAE_J1939::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  19, scmDataInputNames, scmDataInputTypeIds,
  22, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_DataPanel_xDB_16_DO_SAE_J1939::FORTE_DataPanel_xDB_16_DO_SAE_J1939(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    DataPanelFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(224_USINT),
    var_u16ObjId(65535_UINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    var_conn_qTimedOut(var_qTimedOut),
    var_conn_Version(var_Version),
    var_conn_Revision(var_Revision),
    var_conn_Fault_Code(var_Fault_Code),
    var_conn_User_ID(var_User_ID),
    var_conn_Hardware_Version(var_Hardware_Version),
    var_conn_Status_01(var_Status_01),
    var_conn_Status_02(var_Status_02),
    var_conn_Status_03(var_Status_03),
    var_conn_Status_04(var_Status_04),
    var_conn_Status_05(var_Status_05),
    var_conn_Status_06(var_Status_06),
    var_conn_Status_07(var_Status_07),
    var_conn_Status_08(var_Status_08),
    var_conn_Status_09(var_Status_09),
    var_conn_Status_10(var_Status_10),
    var_conn_Status_11(var_Status_11),
    var_conn_Status_12(var_Status_12),
    var_conn_Status_13(var_Status_13),
    var_conn_Status_14(var_Status_14),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalOutput_1A(nullptr),
    conn_DigitalOutput_1B(nullptr),
    conn_DigitalOutput_2A(nullptr),
    conn_DigitalOutput_2B(nullptr),
    conn_DigitalOutput_3A(nullptr),
    conn_DigitalOutput_3B(nullptr),
    conn_DigitalOutput_4A(nullptr),
    conn_DigitalOutput_4B(nullptr),
    conn_DigitalOutput_5A(nullptr),
    conn_DigitalOutput_5B(nullptr),
    conn_DigitalOutput_6A(nullptr),
    conn_DigitalOutput_6B(nullptr),
    conn_DigitalOutput_7A(nullptr),
    conn_DigitalOutput_7B(nullptr),
    conn_DigitalOutput_8A(nullptr),
    conn_DigitalOutput_8B(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS),
    conn_qTimedOut(this, 2, &var_conn_qTimedOut),
    conn_Version(this, 3, &var_conn_Version),
    conn_Revision(this, 4, &var_conn_Revision),
    conn_Fault_Code(this, 5, &var_conn_Fault_Code),
    conn_User_ID(this, 6, &var_conn_User_ID),
    conn_Hardware_Version(this, 7, &var_conn_Hardware_Version),
    conn_Status_01(this, 8, &var_conn_Status_01),
    conn_Status_02(this, 9, &var_conn_Status_02),
    conn_Status_03(this, 10, &var_conn_Status_03),
    conn_Status_04(this, 11, &var_conn_Status_04),
    conn_Status_05(this, 12, &var_conn_Status_05),
    conn_Status_06(this, 13, &var_conn_Status_06),
    conn_Status_07(this, 14, &var_conn_Status_07),
    conn_Status_08(this, 15, &var_conn_Status_08),
    conn_Status_09(this, 16, &var_conn_Status_09),
    conn_Status_10(this, 17, &var_conn_Status_10),
    conn_Status_11(this, 18, &var_conn_Status_11),
    conn_Status_12(this, 19, &var_conn_Status_12),
    conn_Status_13(this, 20, &var_conn_Status_13),
    conn_Status_14(this, 21, &var_conn_Status_14) {
};

void FORTE_DataPanel_xDB_16_DO_SAE_J1939::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 224_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalOutput_1A = u""_WSTRING;
  var_DigitalOutput_1B = u""_WSTRING;
  var_DigitalOutput_2A = u""_WSTRING;
  var_DigitalOutput_2B = u""_WSTRING;
  var_DigitalOutput_3A = u""_WSTRING;
  var_DigitalOutput_3B = u""_WSTRING;
  var_DigitalOutput_4A = u""_WSTRING;
  var_DigitalOutput_4B = u""_WSTRING;
  var_DigitalOutput_5A = u""_WSTRING;
  var_DigitalOutput_5B = u""_WSTRING;
  var_DigitalOutput_6A = u""_WSTRING;
  var_DigitalOutput_6B = u""_WSTRING;
  var_DigitalOutput_7A = u""_WSTRING;
  var_DigitalOutput_7B = u""_WSTRING;
  var_DigitalOutput_8A = u""_WSTRING;
  var_DigitalOutput_8B = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
  var_qTimedOut = 0_BOOL;
  var_Version = 0_USINT;
  var_Revision = 0_USINT;
  var_Fault_Code = 0_USINT;
  var_User_ID = 0_USINT;
  var_Hardware_Version = 0_USINT;
  var_Status_01 = 0_BOOL;
  var_Status_02 = 0_BOOL;
  var_Status_03 = 0_BOOL;
  var_Status_04 = 0_BOOL;
  var_Status_05 = 0_BOOL;
  var_Status_06 = 0_BOOL;
  var_Status_07 = 0_BOOL;
  var_Status_08 = 0_BOOL;
  var_Status_09 = 0_BOOL;
  var_Status_10 = 0_BOOL;
  var_Status_11 = 0_BOOL;
  var_Status_12 = 0_BOOL;
  var_Status_13 = 0_BOOL;
  var_Status_14 = 0_BOOL;
}

void FORTE_DataPanel_xDB_16_DO_SAE_J1939::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;

      handle_1A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_1A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin1A));
      handle_1B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_1B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin1B));
      handle_2A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_2A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin2A));
      handle_2B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_2B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin2B));
      handle_3A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_3A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin3A));
      handle_3B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_3B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin3B));
      handle_4A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_4A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin4A));
      handle_4B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_4B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin4B));
      handle_5A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_5A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin5A));
      handle_5B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_5B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin5B));
      handle_6A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_6A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin6A));
      handle_6B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_6B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin6B));
      handle_7A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_7A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin7A));
      handle_7B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_7B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin7B));
      handle_8A = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_8A.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin8A));
      handle_8B = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalOutput_8B.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::Out, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::DataPanelDigitalOutput, CommonIOEnums::PinNumber::Pin8B));

      var_STATUS = scmOK;

      this->userParam.u8SAIndex = (TForteUInt8)var_u8SAMember - u8SAMember_BASE_e::u8SAMember_BASE;
      g_vts[this->userParam.u8SAIndex] = this;
      DEVLOG_INFO(" FORTE_DataPanel_xDB_16_DO_SAE_J1939::  Block configured for SA=%i %i\n", (TForteUInt8)var_u8SAMember, this->userParam.u8SAIndex);
      sendOutputEvent(scmEventINITOID);
      break;
    case cg_nExternalEventID:
      sendOutputEvent(scmEventINDID);
      break;
  }
}

void FORTE_DataPanel_xDB_16_DO_SAE_J1939::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalOutput_1A, conn_DigitalOutput_1A);
      readData(4, var_DigitalOutput_1B, conn_DigitalOutput_1B);
      readData(5, var_DigitalOutput_2A, conn_DigitalOutput_2A);
      readData(6, var_DigitalOutput_2B, conn_DigitalOutput_2B);
      readData(7, var_DigitalOutput_3A, conn_DigitalOutput_3A);
      readData(8, var_DigitalOutput_3B, conn_DigitalOutput_3B);
      readData(9, var_DigitalOutput_4A, conn_DigitalOutput_4A);
      readData(10, var_DigitalOutput_4B, conn_DigitalOutput_4B);
      readData(11, var_DigitalOutput_5A, conn_DigitalOutput_5A);
      readData(12, var_DigitalOutput_5B, conn_DigitalOutput_5B);
      readData(13, var_DigitalOutput_6A, conn_DigitalOutput_6A);
      readData(14, var_DigitalOutput_6B, conn_DigitalOutput_6B);
      readData(15, var_DigitalOutput_7A, conn_DigitalOutput_7A);
      readData(16, var_DigitalOutput_7B, conn_DigitalOutput_7B);
      readData(17, var_DigitalOutput_8A, conn_DigitalOutput_8A);
      readData(18, var_DigitalOutput_8B, conn_DigitalOutput_8B);
      break;
    }
    default:
      break;
  }
}

void FORTE_DataPanel_xDB_16_DO_SAE_J1939::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      writeData(2, var_qTimedOut, conn_qTimedOut);
      writeData(3, var_Version, conn_Version);
      writeData(4, var_Revision, conn_Revision);
      writeData(5, var_Fault_Code, conn_Fault_Code);
      writeData(6, var_User_ID, conn_User_ID);
      writeData(7, var_Hardware_Version, conn_Hardware_Version);
      writeData(8, var_Status_01, conn_Status_01);
      writeData(9, var_Status_02, conn_Status_02);
      writeData(10, var_Status_03, conn_Status_03);
      writeData(11, var_Status_04, conn_Status_04);
      writeData(12, var_Status_05, conn_Status_05);
      writeData(13, var_Status_06, conn_Status_06);
      writeData(14, var_Status_07, conn_Status_07);
      writeData(15, var_Status_08, conn_Status_08);
      writeData(16, var_Status_09, conn_Status_09);
      writeData(17, var_Status_10, conn_Status_10);
      writeData(18, var_Status_11, conn_Status_11);
      writeData(19, var_Status_12, conn_Status_12);
      writeData(20, var_Status_13, conn_Status_13);
      writeData(21, var_Status_14, conn_Status_14);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalOutput_1A;
    case 4: return &var_DigitalOutput_1B;
    case 5: return &var_DigitalOutput_2A;
    case 6: return &var_DigitalOutput_2B;
    case 7: return &var_DigitalOutput_3A;
    case 8: return &var_DigitalOutput_3B;
    case 9: return &var_DigitalOutput_4A;
    case 10: return &var_DigitalOutput_4B;
    case 11: return &var_DigitalOutput_5A;
    case 12: return &var_DigitalOutput_5B;
    case 13: return &var_DigitalOutput_6A;
    case 14: return &var_DigitalOutput_6B;
    case 15: return &var_DigitalOutput_7A;
    case 16: return &var_DigitalOutput_7B;
    case 17: return &var_DigitalOutput_8A;
    case 18: return &var_DigitalOutput_8B;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
    case 2: return &var_qTimedOut;
    case 3: return &var_Version;
    case 4: return &var_Revision;
    case 5: return &var_Fault_Code;
    case 6: return &var_User_ID;
    case 7: return &var_Hardware_Version;
    case 8: return &var_Status_01;
    case 9: return &var_Status_02;
    case 10: return &var_Status_03;
    case 11: return &var_Status_04;
    case 12: return &var_Status_05;
    case 13: return &var_Status_06;
    case 14: return &var_Status_07;
    case 15: return &var_Status_08;
    case 16: return &var_Status_09;
    case 17: return &var_Status_10;
    case 18: return &var_Status_11;
    case 19: return &var_Status_12;
    case 20: return &var_Status_13;
    case 21: return &var_Status_14;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalOutput_1A;
    case 4: return &conn_DigitalOutput_1B;
    case 5: return &conn_DigitalOutput_2A;
    case 6: return &conn_DigitalOutput_2B;
    case 7: return &conn_DigitalOutput_3A;
    case 8: return &conn_DigitalOutput_3B;
    case 9: return &conn_DigitalOutput_4A;
    case 10: return &conn_DigitalOutput_4B;
    case 11: return &conn_DigitalOutput_5A;
    case 12: return &conn_DigitalOutput_5B;
    case 13: return &conn_DigitalOutput_6A;
    case 14: return &conn_DigitalOutput_6B;
    case 15: return &conn_DigitalOutput_7A;
    case 16: return &conn_DigitalOutput_7B;
    case 17: return &conn_DigitalOutput_8A;
    case 18: return &conn_DigitalOutput_8B;
  }
  return nullptr;
}

CDataConnection *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
    case 2: return &conn_qTimedOut;
    case 3: return &conn_Version;
    case 4: return &conn_Revision;
    case 5: return &conn_Fault_Code;
    case 6: return &conn_User_ID;
    case 7: return &conn_Hardware_Version;
    case 8: return &conn_Status_01;
    case 9: return &conn_Status_02;
    case 10: return &conn_Status_03;
    case 11: return &conn_Status_04;
    case 12: return &conn_Status_05;
    case 13: return &conn_Status_06;
    case 14: return &conn_Status_07;
    case 15: return &conn_Status_08;
    case 16: return &conn_Status_09;
    case 17: return &conn_Status_10;
    case 18: return &conn_Status_11;
    case 19: return &conn_Status_12;
    case 20: return &conn_Status_13;
    case 21: return &conn_Status_14;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_DataPanel_xDB_16_DO_SAE_J1939::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

