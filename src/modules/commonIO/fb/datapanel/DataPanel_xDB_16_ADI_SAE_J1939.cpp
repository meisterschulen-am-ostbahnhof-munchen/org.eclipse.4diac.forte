/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DataPanel_xDB_16_ADI_SAE_J1939
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "DataPanel_xDB_16_ADI_SAE_J1939.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "DataPanel_xDB_16_ADI_SAE_J1939_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_DataPanel_xDB_16_ADI_SAE_J1939, g_nStringIdDataPanel_xDB_16_ADI_SAE_J1939)

const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalInput_1A, g_nStringIdDigitalInput_1B, g_nStringIdDigitalInput_2A, g_nStringIdDigitalInput_2B, g_nStringIdDigitalInput_3A, g_nStringIdDigitalInput_3B, g_nStringIdDigitalInput_4A, g_nStringIdDigitalInput_4B, g_nStringIdDigitalInput_5A, g_nStringIdDigitalInput_5B, g_nStringIdDigitalInput_6A, g_nStringIdDigitalInput_6B, g_nStringIdDigitalInput_7A, g_nStringIdDigitalInput_7B, g_nStringIdDigitalInput_8A, g_nStringIdDigitalInput_8B};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_DataPanel_xDB_16_ADI_SAE_J1939::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  19, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_DataPanel_xDB_16_ADI_SAE_J1939::FORTE_DataPanel_xDB_16_ADI_SAE_J1939(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(224_USINT),
    var_u16ObjId(65535_UINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalInput_1A(nullptr),
    conn_DigitalInput_1B(nullptr),
    conn_DigitalInput_2A(nullptr),
    conn_DigitalInput_2B(nullptr),
    conn_DigitalInput_3A(nullptr),
    conn_DigitalInput_3B(nullptr),
    conn_DigitalInput_4A(nullptr),
    conn_DigitalInput_4B(nullptr),
    conn_DigitalInput_5A(nullptr),
    conn_DigitalInput_5B(nullptr),
    conn_DigitalInput_6A(nullptr),
    conn_DigitalInput_6B(nullptr),
    conn_DigitalInput_7A(nullptr),
    conn_DigitalInput_7B(nullptr),
    conn_DigitalInput_8A(nullptr),
    conn_DigitalInput_8B(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_DataPanel_xDB_16_ADI_SAE_J1939::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 224_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalInput_1A = u""_WSTRING;
  var_DigitalInput_1B = u""_WSTRING;
  var_DigitalInput_2A = u""_WSTRING;
  var_DigitalInput_2B = u""_WSTRING;
  var_DigitalInput_3A = u""_WSTRING;
  var_DigitalInput_3B = u""_WSTRING;
  var_DigitalInput_4A = u""_WSTRING;
  var_DigitalInput_4B = u""_WSTRING;
  var_DigitalInput_5A = u""_WSTRING;
  var_DigitalInput_5B = u""_WSTRING;
  var_DigitalInput_6A = u""_WSTRING;
  var_DigitalInput_6B = u""_WSTRING;
  var_DigitalInput_7A = u""_WSTRING;
  var_DigitalInput_7B = u""_WSTRING;
  var_DigitalInput_8A = u""_WSTRING;
  var_DigitalInput_8B = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_DataPanel_xDB_16_ADI_SAE_J1939::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      #error add code for INIT event!
      /*
        do not forget to send output event, calling e.g.
          sendOutputEvent(scmEventCNFID);
      */
      break;
  }
}

void FORTE_DataPanel_xDB_16_ADI_SAE_J1939::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalInput_1A, conn_DigitalInput_1A);
      readData(4, var_DigitalInput_1B, conn_DigitalInput_1B);
      readData(5, var_DigitalInput_2A, conn_DigitalInput_2A);
      readData(6, var_DigitalInput_2B, conn_DigitalInput_2B);
      readData(7, var_DigitalInput_3A, conn_DigitalInput_3A);
      readData(8, var_DigitalInput_3B, conn_DigitalInput_3B);
      readData(9, var_DigitalInput_4A, conn_DigitalInput_4A);
      readData(10, var_DigitalInput_4B, conn_DigitalInput_4B);
      readData(11, var_DigitalInput_5A, conn_DigitalInput_5A);
      readData(12, var_DigitalInput_5B, conn_DigitalInput_5B);
      readData(13, var_DigitalInput_6A, conn_DigitalInput_6A);
      readData(14, var_DigitalInput_6B, conn_DigitalInput_6B);
      readData(15, var_DigitalInput_7A, conn_DigitalInput_7A);
      readData(16, var_DigitalInput_7B, conn_DigitalInput_7B);
      readData(17, var_DigitalInput_8A, conn_DigitalInput_8A);
      readData(18, var_DigitalInput_8B, conn_DigitalInput_8B);
      break;
    }
    default:
      break;
  }
}

void FORTE_DataPanel_xDB_16_ADI_SAE_J1939::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalInput_1A;
    case 4: return &var_DigitalInput_1B;
    case 5: return &var_DigitalInput_2A;
    case 6: return &var_DigitalInput_2B;
    case 7: return &var_DigitalInput_3A;
    case 8: return &var_DigitalInput_3B;
    case 9: return &var_DigitalInput_4A;
    case 10: return &var_DigitalInput_4B;
    case 11: return &var_DigitalInput_5A;
    case 12: return &var_DigitalInput_5B;
    case 13: return &var_DigitalInput_6A;
    case 14: return &var_DigitalInput_6B;
    case 15: return &var_DigitalInput_7A;
    case 16: return &var_DigitalInput_7B;
    case 17: return &var_DigitalInput_8A;
    case 18: return &var_DigitalInput_8B;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalInput_1A;
    case 4: return &conn_DigitalInput_1B;
    case 5: return &conn_DigitalInput_2A;
    case 6: return &conn_DigitalInput_2B;
    case 7: return &conn_DigitalInput_3A;
    case 8: return &conn_DigitalInput_3B;
    case 9: return &conn_DigitalInput_4A;
    case 10: return &conn_DigitalInput_4B;
    case 11: return &conn_DigitalInput_5A;
    case 12: return &conn_DigitalInput_5B;
    case 13: return &conn_DigitalInput_6A;
    case 14: return &conn_DigitalInput_6B;
    case 15: return &conn_DigitalInput_7A;
    case 16: return &conn_DigitalInput_7B;
    case 17: return &conn_DigitalInput_8A;
    case 18: return &conn_DigitalInput_8B;
  }
  return nullptr;
}

CDataConnection *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_DataPanel_xDB_16_ADI_SAE_J1939::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

