/*
 * DataPanelFunctionBlock.cpp
 *
 *  Created on: 13.08.2022
 *      Author: franz
 */

#include "DataPanelFunctionBlock.h"
#include <extevhandlerhelper.h>

#include <cstdbool>
#include "IsoDef.h"
#include "DatapanelAddresses.h"
#include "DatapanelStatus.h"

void CbPGNReceiveDataPanelSt(const PGNDAT_T* psData) {
  DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::CbPGNReceiveDataPanelSt2(psData); //forward Call
}

    DataPanelFunctionBlock::DataPanelFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
            CStringDictionary::TStringId pa_nInstanceNameId)
    :CommonIOFunctionBlock( pa_poSrcRes, pa_pstInterfaceSpec, pa_nInstanceNameId) {
    	setEventChainExecutor(pa_poSrcRes->getResourceEventExecution());
    }

DataPanelFunctionBlock::~DataPanelFunctionBlock() = default;









DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler * DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::g_vth = nullptr;  //Pointer to instance
DataPanelFunctionBlock                                  * DataPanelFunctionBlock::g_vts[16]                              = {};  //Pointer to instance




DEFINE_HANDLER(DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler);


DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::CbDataPanelFunctionBlockHandler(CDeviceExecution& pa_poDeviceExecution)
: CExternalEventHandler(pa_poDeviceExecution)
{
  g_vth = this;
}


DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::~CbDataPanelFunctionBlockHandler() {
  g_vth = nullptr;
};


void DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::CbPGNReceiveDataPanelSt2(const PGNDAT_T *psData) {
  if(g_vth == nullptr)
  {
    //no callback registered
  } else {
    g_vth->CbPGNReceiveDataPanelSt3(psData);
  }
}

void DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::CbPGNReceiveDataPanelSt3(const PGNDAT_T *psData) {


  if(psData->qTimedOut)
  {
    DEVLOG_ERROR(" DataPanelFunctionBlock::  qTimedOut    %i \n", psData->userParam.u8SAIndex);
    memset(&au8RXDataPanelSt[psData->userParam.u8SAIndex], 0, sizeof(datapanelStatus_s));       //Zero out the transaction
  }


  DataPanelFunctionBlock* c = g_vts[psData->userParam.u8SAIndex];
  if(c != nullptr)
  {
	if(E_FBStates::Running == c->getState()){
	  c->CbPGNReceiveDataPanelSt4(psData);
	  startNewEventChain(c);
    }
  }
}

void DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::enableHandler() {
  //do nothing
}

void DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::disableHandler() {
  //do nothing
}

void DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::setPriority(int) {
  //do nothing
}
int DataPanelFunctionBlock::CbDataPanelFunctionBlockHandler::getPriority() const {
  return 0;
}

