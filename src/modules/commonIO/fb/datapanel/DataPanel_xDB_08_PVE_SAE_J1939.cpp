/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: DataPanel_xDB_08_PVE_SAE_J1939
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "DataPanel_xDB_08_PVE_SAE_J1939.h"
#include "DatapanelAddresses.h"
#include "DatapanelStatus.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "DataPanel_xDB_08_PVE_SAE_J1939_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_DataPanel_xDB_08_PVE_SAE_J1939, g_nStringIdDataPanel_xDB_08_PVE_SAE_J1939)

const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalOutput_1A, g_nStringIdDigitalOutput_2A, g_nStringIdDigitalOutput_3A, g_nStringIdDigitalOutput_4A, g_nStringIdDigitalOutput_5A, g_nStringIdDigitalOutput_6A, g_nStringIdDigitalOutput_7A, g_nStringIdDigitalOutput_8A};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING};
const TDataIOID FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_DataPanel_xDB_08_PVE_SAE_J1939::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  11, scmDataInputNames, scmDataInputTypeIds,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_DataPanel_xDB_08_PVE_SAE_J1939::FORTE_DataPanel_xDB_08_PVE_SAE_J1939(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    DataPanelFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(224_USINT),
    var_u16ObjId(65535_UINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalOutput_1A(nullptr),
    conn_DigitalOutput_2A(nullptr),
    conn_DigitalOutput_3A(nullptr),
    conn_DigitalOutput_4A(nullptr),
    conn_DigitalOutput_5A(nullptr),
    conn_DigitalOutput_6A(nullptr),
    conn_DigitalOutput_7A(nullptr),
    conn_DigitalOutput_8A(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS) {
};

void FORTE_DataPanel_xDB_08_PVE_SAE_J1939::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 224_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalOutput_1A = u""_WSTRING;
  var_DigitalOutput_2A = u""_WSTRING;
  var_DigitalOutput_3A = u""_WSTRING;
  var_DigitalOutput_4A = u""_WSTRING;
  var_DigitalOutput_5A = u""_WSTRING;
  var_DigitalOutput_6A = u""_WSTRING;
  var_DigitalOutput_7A = u""_WSTRING;
  var_DigitalOutput_8A = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
}

void FORTE_DataPanel_xDB_08_PVE_SAE_J1939::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      #error add code for INIT event!
      /*
        do not forget to send output event, calling e.g.
          sendOutputEvent(scmEventCNFID);
      */
      break;
  }
}

void FORTE_DataPanel_xDB_08_PVE_SAE_J1939::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalOutput_1A, conn_DigitalOutput_1A);
      readData(4, var_DigitalOutput_2A, conn_DigitalOutput_2A);
      readData(5, var_DigitalOutput_3A, conn_DigitalOutput_3A);
      readData(6, var_DigitalOutput_4A, conn_DigitalOutput_4A);
      readData(7, var_DigitalOutput_5A, conn_DigitalOutput_5A);
      readData(8, var_DigitalOutput_6A, conn_DigitalOutput_6A);
      readData(9, var_DigitalOutput_7A, conn_DigitalOutput_7A);
      readData(10, var_DigitalOutput_8A, conn_DigitalOutput_8A);
      break;
    }
    default:
      break;
  }
}

void FORTE_DataPanel_xDB_08_PVE_SAE_J1939::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalOutput_1A;
    case 4: return &var_DigitalOutput_2A;
    case 5: return &var_DigitalOutput_3A;
    case 6: return &var_DigitalOutput_4A;
    case 7: return &var_DigitalOutput_5A;
    case 8: return &var_DigitalOutput_6A;
    case 9: return &var_DigitalOutput_7A;
    case 10: return &var_DigitalOutput_8A;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
  }
  return nullptr;
}

CIEC_ANY *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalOutput_1A;
    case 4: return &conn_DigitalOutput_2A;
    case 5: return &conn_DigitalOutput_3A;
    case 6: return &conn_DigitalOutput_4A;
    case 7: return &conn_DigitalOutput_5A;
    case 8: return &conn_DigitalOutput_6A;
    case 9: return &conn_DigitalOutput_7A;
    case 10: return &conn_DigitalOutput_8A;
  }
  return nullptr;
}

CDataConnection *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_DataPanel_xDB_08_PVE_SAE_J1939::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

