/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: PKP_2200
 *************************************************************************/

#ifndef _PKP_2200_H_
#define _PKP_2200_H_

#include "forte_usint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

extern const CIEC_USINT st_global_PKP_2200_DEFAULT;

#endif // _PKP_2200_H_
