/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: PKP_2200_LI_4_DI
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CommonIOFunctionBlock.h"
#include "forte_bool.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_wstring.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


class FORTE_PKP_2200_LI_4_DI final : public CommonIOFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_PKP_2200_LI_4_DI)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  std::unique_ptr<forte::core::io::IOHandle> handle_Key_1;
  std::unique_ptr<forte::core::io::IOHandle> handle_Key_2;
  std::unique_ptr<forte::core::io::IOHandle> handle_Key_3;
  std::unique_ptr<forte::core::io::IOHandle> handle_Key_4;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_PKP_2200_LI_4_DI(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_BOOL var_QI;
  CIEC_USINT var_u8SAMember;
  CIEC_UINT var_u16ObjId;
  CIEC_WSTRING var_DigitalInput_Key_1;
  CIEC_WSTRING var_DigitalInput_Key_2;
  CIEC_WSTRING var_DigitalInput_Key_3;
  CIEC_WSTRING var_DigitalInput_Key_4;

  CIEC_BOOL var_QO;
  CIEC_STRING var_STATUS;
  CIEC_BOOL var_qTimedOut;

  CIEC_BOOL var_conn_QO;
  CIEC_STRING var_conn_STATUS;
  CIEC_BOOL var_conn_qTimedOut;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection *conn_QI;
  CDataConnection *conn_u8SAMember;
  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_DigitalInput_Key_1;
  CDataConnection *conn_DigitalInput_Key_2;
  CDataConnection *conn_DigitalInput_Key_3;
  CDataConnection *conn_DigitalInput_Key_4;

  CDataConnection conn_QO;
  CDataConnection conn_STATUS;
  CDataConnection conn_qTimedOut;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalInput_Key_1, const CIEC_WSTRING &paDigitalInput_Key_2, const CIEC_WSTRING &paDigitalInput_Key_3, const CIEC_WSTRING &paDigitalInput_Key_4, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS, CIEC_BOOL &paqTimedOut) {
    var_QI = paQI;
    var_u8SAMember = pau8SAMember;
    var_u16ObjId = pau16ObjId;
    var_DigitalInput_Key_1 = paDigitalInput_Key_1;
    var_DigitalInput_Key_2 = paDigitalInput_Key_2;
    var_DigitalInput_Key_3 = paDigitalInput_Key_3;
    var_DigitalInput_Key_4 = paDigitalInput_Key_4;
    receiveInputEvent(scmEventINITID, nullptr);
    paQO = var_QO;
    paSTATUS = var_STATUS;
    paqTimedOut = var_qTimedOut;
  }

  void operator()(const CIEC_BOOL &paQI, const CIEC_USINT &pau8SAMember, const CIEC_UINT &pau16ObjId, const CIEC_WSTRING &paDigitalInput_Key_1, const CIEC_WSTRING &paDigitalInput_Key_2, const CIEC_WSTRING &paDigitalInput_Key_3, const CIEC_WSTRING &paDigitalInput_Key_4, CIEC_BOOL &paQO, CIEC_STRING &paSTATUS, CIEC_BOOL &paqTimedOut) {
    evt_INIT(paQI, pau8SAMember, pau16ObjId, paDigitalInput_Key_1, paDigitalInput_Key_2, paDigitalInput_Key_3, paDigitalInput_Key_4, paQO, paSTATUS, paqTimedOut);
  }
};


