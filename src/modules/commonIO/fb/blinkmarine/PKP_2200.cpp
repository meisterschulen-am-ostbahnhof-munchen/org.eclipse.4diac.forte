/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: PKP_2200
 *************************************************************************/

#include "PKP_2200.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "PKP_2200_gen.cpp"
#endif

#include "iec61131_functions.h"
#include "forte_usint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

const CIEC_USINT st_global_PKP_2200_DEFAULT = 33_USINT;
