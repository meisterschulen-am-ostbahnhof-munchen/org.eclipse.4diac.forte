/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: PKP_2200_LI_4_DI
 *** Description: Service Interface Function Block Type
 *** Version:
 ***     1.0: 2018-12-05/Jose Cabral -  -
 ***     1.1: 2022-08-04/Franz Höpfinger - HR Agrartechnik GmbH -
 ***     1.2: 2023-01-05/Franz Höpfinger - HR Agrartechnik GmbH -
 *************************************************************************/

#include "PKP_2200_LI_4_DI.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "PKP_2200_LI_4_DI_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

DEFINE_FIRMWARE_FB(FORTE_PKP_2200_LI_4_DI, g_nStringIdPKP_2200_LI_4_DI)

const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmDataInputNames[] = {g_nStringIdQI, g_nStringIdu8SAMember, g_nStringIdu16ObjId, g_nStringIdDigitalInput_Key_1, g_nStringIdDigitalInput_Key_2, g_nStringIdDigitalInput_Key_3, g_nStringIdDigitalInput_Key_4};
const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmDataInputTypeIds[] = {g_nStringIdBOOL, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING, g_nStringIdWSTRING};
const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmDataOutputNames[] = {g_nStringIdQO, g_nStringIdSTATUS, g_nStringIdqTimedOut};
const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmDataOutputTypeIds[] = {g_nStringIdBOOL, g_nStringIdSTRING, g_nStringIdBOOL};
const TDataIOID FORTE_PKP_2200_LI_4_DI::scmEIWith[] = {0, 1, 2, 3, 4, 5, 6, scmWithListDelimiter};
const TForteInt16 FORTE_PKP_2200_LI_4_DI::scmEIWithIndexes[] = {0};
const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_PKP_2200_LI_4_DI::scmEOWith[] = {0, scmWithListDelimiter, 0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_PKP_2200_LI_4_DI::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_PKP_2200_LI_4_DI::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_PKP_2200_LI_4_DI::scmFBInterfaceSpec = {
  1, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  7, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_PKP_2200_LI_4_DI::FORTE_PKP_2200_LI_4_DI(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CommonIOFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_u8SAMember(224_USINT),
    var_u16ObjId(65535_UINT),
    var_conn_QO(var_QO),
    var_conn_STATUS(var_STATUS),
    var_conn_qTimedOut(var_qTimedOut),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_QI(nullptr),
    conn_u8SAMember(nullptr),
    conn_u16ObjId(nullptr),
    conn_DigitalInput_Key_1(nullptr),
    conn_DigitalInput_Key_2(nullptr),
    conn_DigitalInput_Key_3(nullptr),
    conn_DigitalInput_Key_4(nullptr),
    conn_QO(this, 0, &var_conn_QO),
    conn_STATUS(this, 1, &var_conn_STATUS),
    conn_qTimedOut(this, 2, &var_conn_qTimedOut) {
};

void FORTE_PKP_2200_LI_4_DI::setInitialValues() {
  var_QI = 0_BOOL;
  var_u8SAMember = 224_USINT;
  var_u16ObjId = 65535_UINT;
  var_DigitalInput_Key_1 = u""_WSTRING;
  var_DigitalInput_Key_2 = u""_WSTRING;
  var_DigitalInput_Key_3 = u""_WSTRING;
  var_DigitalInput_Key_4 = u""_WSTRING;
  var_QO = 0_BOOL;
  var_STATUS = ""_STRING;
  var_qTimedOut = 0_BOOL;
}

void FORTE_PKP_2200_LI_4_DI::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      var_QO = var_QI;
      //TODO !! avoid 2 identical u16ObjId's !!!!
      handle_Key_1 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_Key_1.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::BlinkMarineKeypadKey, CommonIOEnums::PinNumber::Key1));
      handle_Key_2 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_Key_2.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::BlinkMarineKeypadKey, CommonIOEnums::PinNumber::Key2));
      handle_Key_3 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_Key_3.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::BlinkMarineKeypadKey, CommonIOEnums::PinNumber::Key3));
      handle_Key_4 = std::unique_ptr<forte::core::io::IOHandle>(mapPin(var_DigitalInput_Key_4.getValue(), var_u8SAMember, var_u16ObjId, forte::core::io::IOMapper::In, CIEC_ANY::e_BOOL, CommonIODeviceController::HandleType::BlinkMarineKeypadKey, CommonIOEnums::PinNumber::Key4));

      var_STATUS = scmOK;

      sendOutputEvent(scmEventINITOID);
      break;
    case cgExternalEventID:
      sendOutputEvent(scmEventINDID);
      break;
  }
}

void FORTE_PKP_2200_LI_4_DI::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_QI, conn_QI);
      readData(1, var_u8SAMember, conn_u8SAMember);
      readData(2, var_u16ObjId, conn_u16ObjId);
      readData(3, var_DigitalInput_Key_1, conn_DigitalInput_Key_1);
      readData(4, var_DigitalInput_Key_2, conn_DigitalInput_Key_2);
      readData(5, var_DigitalInput_Key_3, conn_DigitalInput_Key_3);
      readData(6, var_DigitalInput_Key_4, conn_DigitalInput_Key_4);
      break;
    }
    default:
      break;
  }
}

void FORTE_PKP_2200_LI_4_DI::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_QO, conn_QO);
      writeData(1, var_STATUS, conn_STATUS);
      writeData(2, var_qTimedOut, conn_qTimedOut);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_PKP_2200_LI_4_DI::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QI;
    case 1: return &var_u8SAMember;
    case 2: return &var_u16ObjId;
    case 3: return &var_DigitalInput_Key_1;
    case 4: return &var_DigitalInput_Key_2;
    case 5: return &var_DigitalInput_Key_3;
    case 6: return &var_DigitalInput_Key_4;
  }
  return nullptr;
}

CIEC_ANY *FORTE_PKP_2200_LI_4_DI::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_QO;
    case 1: return &var_STATUS;
    case 2: return &var_qTimedOut;
  }
  return nullptr;
}

CIEC_ANY *FORTE_PKP_2200_LI_4_DI::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_PKP_2200_LI_4_DI::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_PKP_2200_LI_4_DI::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QI;
    case 1: return &conn_u8SAMember;
    case 2: return &conn_u16ObjId;
    case 3: return &conn_DigitalInput_Key_1;
    case 4: return &conn_DigitalInput_Key_2;
    case 5: return &conn_DigitalInput_Key_3;
    case 6: return &conn_DigitalInput_Key_4;
  }
  return nullptr;
}

CDataConnection *FORTE_PKP_2200_LI_4_DI::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_QO;
    case 1: return &conn_STATUS;
    case 2: return &conn_qTimedOut;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_PKP_2200_LI_4_DI::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_PKP_2200_LI_4_DI::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

