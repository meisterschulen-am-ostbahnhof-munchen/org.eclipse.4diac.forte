/*
 * CommonIOEnums.h
 *
 *  Created on: 13.08.2022
 *      Author: franz
 */

#ifndef SRC_MODULES_COMMON_IO_COMMON_IOENUMS_H_
#define SRC_MODULES_COMMON_IO_COMMON_IOENUMS_H_



class CommonIOEnums {

  public:


    enum ThreePositionSwitch {
      ThreePositionSwitch_Invalid = -1,
      UP,
      DOWN,
      RIGHT,
      LEFT,
      AXIS_1,
      AXIS_2,
      COUNTER,
      CALIBRATION,
      ThreePositionSwitch_MAX
    };


    enum PinNumber {
      PinNumber_Invalid = -1,
      //Digital In
      PinI1,
      PinI2,
      PinI3,
      //Analog Mulitplex Digital in
      PinIA1,
      PinIA2,
      PinIA3,
      PinIA4,
      PinIA5,
      PinIA6,
      //Digital Out
      PinQ1,
      PinQ2,
      PinQ3,
      PinQ4,
      //Data Panel Output
      Pin1A,
      Pin1B,
      Pin2A,
      Pin2B,
      Pin3A,
      Pin3B,
      Pin4A,
      Pin4B,
      Pin5A,
      Pin5B,
      Pin6A,
      Pin6B,
      Pin7A,
      Pin7B,
      Pin8A,
      Pin8B,
      //Data Panel InputPower
      PinIP1,
      PinIP2,
      PinIP3,
      PinIP4,
      PinIP5,
      PinIP6,
      PinIP7,
      PinIP8,
      // Blink Marine Keypad
      Key1,
      Key2,
      Key3,
      Key4,
    };
};







#endif /* SRC_MODULES_COMMON_IO_COMMON_IOENUMS_H_ */
