/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#include "CommonIODeviceController.h"
#include "CommonIODeviceController.h"

#if defined(FORTE_MODULE_DATAPANEL)
#include "IOHandleDataPanelDigitalOutput.h"
#include "IOHandleDataPanelDigitalInput.h"
#endif //FORTE_MODULE_DATAPANEL

#if defined(FORTE_MODULE_BLINKMARINE)
#include "IOHandleBlinkMarineDigitalInput.h"
#endif //FORTE_MODULE_DATAPANEL

#if defined(FORTE_MODULE_ISOBUS)
#include "IOHandleSoftKey.h"
#include "IOHandleSoftKey_DOWN.h"
#include "IOHandleSoftKey_HOLD.h"
#include "IOHandleSoftKey_UP.h"
#include "IOHandleNumericValue.h"
#include "IOHandleButton.h"
#include "IOHandleButton_DOWN.h"
#include "IOHandleButton_HOLD.h"
#include "IOHandleButton_UP.h"
#include "IOHandleAuxiliaryFunction_00.h"
#include "IOHandleAuxiliaryFunction_01.h"
#include "IOHandleAuxiliaryFunction_02.h"
#include "IOHandleAuxiliaryFunction_03.h"
#include "IOHandleAuxiliaryFunction_04.h"
#include "IOHandleAuxiliaryFunction_05.h"
#include "IOHandleAuxiliaryFunction_06.h"
#include "IOHandleAuxiliaryFunction_07.h"
#include "IOHandleAuxiliaryFunction_08.h"
#include "IOHandleAuxiliaryFunction_09.h"
#include "IOHandleAuxiliaryFunction_10.h"
#include "IOHandleAuxiliaryFunction_11.h"
#include "IOHandleAuxiliaryFunction_12.h"
#include "IOHandleAuxiliaryFunction_13.h"
#include "IOHandleAuxiliaryFunction_13.h"
#include "IOHandleAuxiliaryFunction_14.h"
#endif //FORTE_MODULE_ISOBUS

#if defined(FORTE_MODULE_ESP32_DIGITAL_OUT)
#include "IOHandleESP32DigitalO.h"
#endif //FORTE_MODULE_ESP32_DIGITAL_OUT

#if defined(FORTE_MODULE_ESP32_DIGITAL_IN)
#include "IOHandleESP32Button.h"
#include "IOHandleESP32Button_DOWN.h"
#include "IOHandleESP32Button_UP.h"
#include "IOHandleESP32Button_REPEAT.h"
#include "IOHandleESP32Button_SINGLE_CLICK.h"
#include "IOHandleESP32Button_DOUBLE_CLICK.h"
#include "IOHandleESP32Button_LONG_PRESS_START.h"
#include "IOHandleESP32Button_LONG_PRESS_HOLD.h"
#endif //FORTE_MODULE_ESP32_DIGITAL_IN

DEFINE_HANDLER(CommonIODeviceController);

CommonIODeviceController::CommonIODeviceController(CDeviceExecution& paDeviceExecution) 
: forte::core::io::IODeviceController(paDeviceExecution){}

CommonIODeviceController::~CommonIODeviceController() = default;

forte::core::io::IOHandle *
CommonIODeviceController::initHandle(forte::core::io::IODeviceController::HandleDescriptor *paHandleDescriptor) {
  auto desc = static_cast<CommonIOHandleDescriptor *>(paHandleDescriptor);
  forte::core::io::IOHandle *handle = nullptr;

  switch (desc->mHandleType) {
#if defined(FORTE_MODULE_DATAPANEL)
  case HandleType::DataPanelDigitalOutput:       handle = new IOHandleDataPanelDigitalOutput      (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::DataPanelDigitalInput:        handle = new IOHandleDataPanelDigitalInput       (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
#endif //FORTE_MODULE_DATAPANEL

#if defined(FORTE_MODULE_BLINKMARINE)
  case HandleType::BlinkMarineKeypadKey:         handle = new IOHandleBlinkMarineDigitalInput     (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
#endif //FORTE_MODULE_BLINKMARINE

#if defined(FORTE_MODULE_ESP32_DIGITAL_OUT)
  case HandleType::ESP32DigitalO:                handle = new IOHandleESP32DigitalO               (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
#endif //FORTE_MODULE_ESP32

#if defined(FORTE_MODULE_ESP32_DIGITAL_IN)
  case HandleType::ESP32Button:                  handle = new IOHandleESP32Button                 (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_DOWN:             handle = new IOHandleESP32Button_DOWN            (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_UP:               handle = new IOHandleESP32Button_UP              (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_REPEAT:           handle = new IOHandleESP32Button_REPEAT          (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_SINGLE_CLICK:     handle = new IOHandleESP32Button_SINGLE_CLICK    (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_DOUBLE_CLICK:     handle = new IOHandleESP32Button_DOUBLE_CLICK    (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_LONG_PRESS_START: handle = new IOHandleESP32Button_LONG_PRESS_START(this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::ESP32Button_LONG_PRESS_HOLD:  handle = new IOHandleESP32Button_LONG_PRESS_HOLD (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
#endif //FORTE_MODULE_ESP32

#if defined(FORTE_MODULE_ISOBUS)
  case HandleType::SoftKey:                      handle = new IOHandleSoftKey                     (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::SoftKey_DOWN:                 handle = new IOHandleSoftKey_DOWN                (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::SoftKey_HOLD:                 handle = new IOHandleSoftKey_HOLD                (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::SoftKey_UP:                   handle = new IOHandleSoftKey_UP                  (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::NumericValueInput:            handle = new IOHandleNumericValue                (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::Button:                       handle = new IOHandleButton                      (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::Button_DOWN:                  handle = new IOHandleButton_DOWN                 (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::Button_HOLD:                  handle = new IOHandleButton_HOLD                 (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::Button_UP:                    handle = new IOHandleButton_UP                   (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_00:         handle = new IOHandleAuxiliaryFunction_00        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_01:         handle = new IOHandleAuxiliaryFunction_01        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_02:         handle = new IOHandleAuxiliaryFunction_02        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_03:         handle = new IOHandleAuxiliaryFunction_03        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_04:         handle = new IOHandleAuxiliaryFunction_04        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin); break;
  case HandleType::AuxiliaryFunction_05:         handle = new IOHandleAuxiliaryFunction_05        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_06:         handle = new IOHandleAuxiliaryFunction_06        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_07:         handle = new IOHandleAuxiliaryFunction_07        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_08:         handle = new IOHandleAuxiliaryFunction_08        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_09:         handle = new IOHandleAuxiliaryFunction_09        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_10:         handle = new IOHandleAuxiliaryFunction_10        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_11:         handle = new IOHandleAuxiliaryFunction_11        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_12:         handle = new IOHandleAuxiliaryFunction_12        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_13:         handle = new IOHandleAuxiliaryFunction_13        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
  case HandleType::AuxiliaryFunction_14:         handle = new IOHandleAuxiliaryFunction_14        (this, desc->mU8SAMember, desc->mU16ObjId, desc->mDirection, desc->mDataType, desc->mPin, desc->mSwitch); break;
#endif //FORTE_MODULE_ISOBUS

  }
  return static_cast<forte::core::io::IOHandle *>(handle);
}

CommonIODeviceController::CommonIOHandleDescriptor::CommonIOHandleDescriptor(std::string const &paId, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIODeviceController::HandleType paHandleType, CommonIOEnums::PinNumber paPin, CommonIOEnums::ThreePositionSwitch paSwitch)
: forte::core::io::IODeviceController::HandleDescriptor(paId, paDirection)
, mU8SAMember(paU8SAMember)
, mU16ObjId(paU16ObjId)
, mDataType(paDataType)
, mHandleType(paHandleType)
, mPin(paPin)
, mSwitch(paSwitch)
{}


