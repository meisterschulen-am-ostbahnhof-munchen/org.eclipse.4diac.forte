/*******************************************************************************
 * Copyright (c) 2021, 2022 Jonathan Lainer (kontakt@lainer.co.at)
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Jonathan Lainer - Initial implementation.
 *******************************************************************************/

#ifndef COMMON_IO_DEVICE_CONTROLLER_H
#define COMMON_IO_DEVICE_CONTROLLER_H

#include "core/io/device/io_controller.h"
#include "core/io/mapper/io_handle.h"
#include "extevhan.h"
#include "CommonIOEnums.h"


class CommonIODeviceController : public forte::core::io::IODeviceController {
public:

  DECLARE_HANDLER(CommonIODeviceController);




  //TODO add Analog, PWM etc..
  enum HandleType {
    Invalid = -1,
    DataPanelDigitalOutput,
    DataPanelDigitalInput,
    ESP32DigitalO,
    ESP32Button,
    ESP32Button_DOWN,
    ESP32Button_UP,
    ESP32Button_REPEAT,
    ESP32Button_SINGLE_CLICK,
    ESP32Button_DOUBLE_CLICK,
    ESP32Button_LONG_PRESS_START,
    ESP32Button_LONG_PRESS_HOLD,
    SoftKey,           //Soft Key                       – ISO 11783-6 – B.6
    SoftKey_DOWN,      //Soft Key                       – ISO 11783-6 – B.6
    SoftKey_HOLD,      //Soft Key                       – ISO 11783-6 – B.6
    SoftKey_UP,        //Soft Key                       – ISO 11783-6 – B.6
    Button,            //Button                         – ISO 11783-6 – B.7
    Button_DOWN,       //Button                         – ISO 11783-6 – B.7
    Button_HOLD,       //Button                         – ISO 11783-6 – B.7
    Button_UP,         //Button                         – ISO 11783-6 – B.7
    AuxiliaryFunction_00, //Auxiliary Function Type 0   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_01, //Auxiliary Function Type 1   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_02, //Auxiliary Function Type 2   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_03, //Auxiliary Function Type 3   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_04, //Auxiliary Function Type 4   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_05, //Auxiliary Function Type 5   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_06, //Auxiliary Function Type 6   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_07, //Auxiliary Function Type 7   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_08, //Auxiliary Function Type 8   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_09, //Auxiliary Function Type 9   – ISO 11783-6 – J.4.3
    AuxiliaryFunction_10, //Auxiliary Function Type 10  – ISO 11783-6 – J.4.3
    AuxiliaryFunction_11, //Auxiliary Function Type 11  – ISO 11783-6 – J.4.3
    AuxiliaryFunction_12, //Auxiliary Function Type 12  – ISO 11783-6 – J.4.3
    AuxiliaryFunction_13, //Auxiliary Function Type 13  – ISO 11783-6 – J.4.3
    AuxiliaryFunction_14, //Auxiliary Function Type 14  – ISO 11783-6 – J.4.3
    NumericValueInput, //NumericValueInput              – ISO 11783-6 – TODO
    BlinkMarineKeypadKey,
  };



  class CommonIOHandleDescriptor
      : public forte::core::io::IODeviceController::HandleDescriptor {
  public:
    CIEC_USINT                          &mU8SAMember;
    CIEC_UINT                           &mU16ObjId;
    CIEC_ANY::EDataTypeID                mDataType;
    CommonIODeviceController::HandleType mHandleType;
    CommonIOEnums::PinNumber             mPin;
    CommonIOEnums::ThreePositionSwitch   mSwitch;


    CommonIOHandleDescriptor(std::string const &paId, CIEC_USINT &paU8SAMember, CIEC_UINT &paU16ObjId, forte::core::io::IOMapper::Direction paDirection, CIEC_ANY::EDataTypeID paDataType, CommonIODeviceController::HandleType paHandleType, CommonIOEnums::PinNumber paPin, CommonIOEnums::ThreePositionSwitch paSwitch);
  };


  forte::core::io::IOHandle* initHandle(forte::core::io::IODeviceController::HandleDescriptor* paHandleDescriptor);

  void setConfig(Config *paConfig) {}

  const char *init() {
    const char *x = "";
    return x;
  }


  void runLoop() {}

  void deInit() {}



};

#endif /* ifndef COMMON_IO_DEVICE_CONTROLLER_H */
