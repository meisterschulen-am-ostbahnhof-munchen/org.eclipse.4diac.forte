/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: colours
 *************************************************************************/

#include "colours.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "colours_gen.cpp"
#endif

#include "iec61131_functions.h"
#include "forte_usint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

const CIEC_USINT st_global_COLOR_BLACK = 0_USINT;
const CIEC_USINT st_global_COLOR_WHITE = 1_USINT;
const CIEC_USINT st_global_COLOR_GREEN = 2_USINT;
const CIEC_USINT st_global_COLOR_TEAL = 3_USINT;
const CIEC_USINT st_global_COLOR_MAROON = 4_USINT;
const CIEC_USINT st_global_COLOR_PURPLE = 5_USINT;
const CIEC_USINT st_global_COLOR_OLIVE = 6_USINT;
const CIEC_USINT st_global_COLOR_SILVER = 7_USINT;
const CIEC_USINT st_global_COLOR_GREY = 8_USINT;
const CIEC_USINT st_global_COLOR_BLUE = 9_USINT;
const CIEC_USINT st_global_COLOR_LIME = 10_USINT;
const CIEC_USINT st_global_COLOR_CYAN = 11_USINT;
const CIEC_USINT st_global_COLOR_RED = 12_USINT;
const CIEC_USINT st_global_COLOR_MAGENTA = 13_USINT;
const CIEC_USINT st_global_COLOR_YELLOW = 14_USINT;
const CIEC_USINT st_global_COLOR_NAVY = 15_USINT;
