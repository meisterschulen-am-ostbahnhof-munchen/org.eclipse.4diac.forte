/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SelectActiveWorkingSet
 *** Description: Command Select Active Working Set( Part 6 - F.64 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_SelectActiveWorkingSet final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_SelectActiveWorkingSet)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendSelectActiveWorkingSet(void);
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_SelectActiveWorkingSet(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 7> var_pau8Name;

  CIEC_STRING var_STATUS;
  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 7> var_pau8OldName;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 7> var_conn_pau8OldName;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_pau8Name;

  CDataConnection conn_STATUS;
  CDataConnection conn_pau8OldName;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_ARRAY_COMMON<CIEC_USINT> &papau8Name, CIEC_STRING &paSTATUS, CIEC_ARRAY_COMMON<CIEC_USINT> &papau8OldName, CIEC_INT &pas16result) {
    var_pau8Name = papau8Name;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    papau8OldName = var_pau8OldName;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_ARRAY_COMMON<CIEC_USINT> &papau8Name, CIEC_STRING &paSTATUS, CIEC_ARRAY_COMMON<CIEC_USINT> &papau8OldName, CIEC_INT &pas16result) {
    var_pau8Name = papau8Name;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    papau8OldName = var_pau8OldName;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_ARRAY_COMMON<CIEC_USINT> &papau8Name, CIEC_STRING &paSTATUS, CIEC_ARRAY_COMMON<CIEC_USINT> &papau8OldName, CIEC_INT &pas16result) {
    evt_INIT(papau8Name, paSTATUS, papau8OldName, pas16result);
  }
};


