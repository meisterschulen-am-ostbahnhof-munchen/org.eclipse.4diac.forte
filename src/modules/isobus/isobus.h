/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: isobus
 *************************************************************************/

#ifndef _ISOBUS_H_
#define _ISOBUS_H_

#include "forte_uint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

extern const CIEC_UINT st_global_ID_NULL;

#endif // _ISOBUS_H_
