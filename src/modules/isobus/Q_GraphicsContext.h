/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_GraphicsContext
 *** Description: Command Graphics Context ( Part 6 - F.56 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_GraphicsContext final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_GraphicsContext)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendGraphicsContext(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_GraphicsContext(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_USINT var_u8SubCommandID;
  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 4> var_u8ParaList;
  CIEC_UINT var_u16LenBytes;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldSubCommandID;
  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 4> var_u8OldParaList;
  CIEC_UINT var_u16OldLenBytes;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldSubCommandID;
  CIEC_ARRAY_FIXED<CIEC_USINT, 0, 4> var_conn_u8OldParaList;
  CIEC_UINT var_conn_u16OldLenBytes;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u8SubCommandID;
  CDataConnection *conn_u8ParaList;
  CDataConnection *conn_u16LenBytes;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldSubCommandID;
  CDataConnection conn_u8OldParaList;
  CDataConnection conn_u16OldLenBytes;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8SubCommandID, const CIEC_ARRAY_COMMON<CIEC_USINT> &pau8ParaList, const CIEC_UINT &pau16LenBytes, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldSubCommandID, CIEC_ARRAY_COMMON<CIEC_USINT> &pau8OldParaList, CIEC_UINT &pau16OldLenBytes, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8SubCommandID = pau8SubCommandID;
    var_u8ParaList = pau8ParaList;
    var_u16LenBytes = pau16LenBytes;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldSubCommandID = var_u8OldSubCommandID;
    pau8OldParaList = var_u8OldParaList;
    pau16OldLenBytes = var_u16OldLenBytes;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8SubCommandID, const CIEC_ARRAY_COMMON<CIEC_USINT> &pau8ParaList, const CIEC_UINT &pau16LenBytes, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldSubCommandID, CIEC_ARRAY_COMMON<CIEC_USINT> &pau8OldParaList, CIEC_UINT &pau16OldLenBytes, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8SubCommandID = pau8SubCommandID;
    var_u8ParaList = pau8ParaList;
    var_u16LenBytes = pau16LenBytes;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldSubCommandID = var_u8OldSubCommandID;
    pau8OldParaList = var_u8OldParaList;
    pau16OldLenBytes = var_u16OldLenBytes;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8SubCommandID, const CIEC_ARRAY_COMMON<CIEC_USINT> &pau8ParaList, const CIEC_UINT &pau16LenBytes, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldSubCommandID, CIEC_ARRAY_COMMON<CIEC_USINT> &pau8OldParaList, CIEC_UINT &pau16OldLenBytes, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau8SubCommandID, pau8ParaList, pau16LenBytes, paSTATUS, pau8OldSubCommandID, pau8OldParaList, pau16OldLenBytes, pas16result);
  }
};


