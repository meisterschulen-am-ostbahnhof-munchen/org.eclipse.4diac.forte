/*
 * CisobusFunctionBlock.cpp
 *
 *  Created on: 30.07.2022
 *      Author: franz
 */

#include "CisobusFunctionBlock.h"


const CIEC_STRING CisobusFunctionBlock::scmOK                 = "OK"_STRING;
const CIEC_STRING CisobusFunctionBlock::scmNotInitialised     = "Not initialized"_STRING;
const CIEC_STRING CisobusFunctionBlock::scmInitialised        = "initialized"_STRING;
const CIEC_STRING CisobusFunctionBlock::CfHandleMasterunvalid = "CfHandleMaster unvalid"_STRING;
const CIEC_STRING CisobusFunctionBlock::VtcInstanceunvalid    = "VtcInstance unvalid"_STRING;
const CIEC_STRING CisobusFunctionBlock::scmOK_oldIsNew        = "OK, but no different Value (old is same as new_STRING"_STRING;
const CIEC_STRING CisobusFunctionBlock::scmE_OVERFLOW         = "buffer overflow"_STRING; //- buffer overflow
const CIEC_STRING CisobusFunctionBlock::scmE_NOACT            = "Command not possible in current state"_STRING;//- Command not possible in current state
const CIEC_STRING CisobusFunctionBlock::scmE_NO_INSTANCE      = "No VT client available"_STRING; //- No VT client available
const CIEC_STRING CisobusFunctionBlock::scmERR                = "ERROR"_STRING; //Unknown Error



CisobusFunctionBlock::CisobusFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
        CStringDictionary::TStringId pa_nInstanceNameId) :
CFunctionBlock(pa_poSrcRes, pa_pstInterfaceSpec, pa_nInstanceNameId) {
};

CisobusFunctionBlock::~CisobusFunctionBlock() = default;





