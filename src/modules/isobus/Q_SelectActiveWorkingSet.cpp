/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SelectActiveWorkingSet
 *** Description: Command Select Active Working Set( Part 6 - F.64 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_SelectActiveWorkingSet.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_SelectActiveWorkingSet_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_SelectActiveWorkingSet, g_nStringIdQ_SelectActiveWorkingSet)

const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmDataInputNames[] = {g_nStringIdpau8Name};
const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmDataInputTypeIds[] = {g_nStringIdARRAY, static_cast<CStringDictionary::TStringId>(0), static_cast<CStringDictionary::TStringId>(7), g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdpau8OldName, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdARRAY, static_cast<CStringDictionary::TStringId>(0), static_cast<CStringDictionary::TStringId>(7), g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_SelectActiveWorkingSet::scmEIWith[] = {0, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SelectActiveWorkingSet::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_SelectActiveWorkingSet::scmEOWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SelectActiveWorkingSet::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SelectActiveWorkingSet::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_SelectActiveWorkingSet::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  1, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_SelectActiveWorkingSet::FORTE_Q_SelectActiveWorkingSet(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_pau8OldName(var_pau8OldName),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_pau8Name(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_pau8OldName(this, 1, &var_conn_pau8OldName),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_SelectActiveWorkingSet::setInitialValues() {
  var_pau8Name = CIEC_ARRAY_FIXED<CIEC_USINT, 0, 7>{};
  var_STATUS = ""_STRING;
  var_pau8OldName = CIEC_ARRAY_FIXED<CIEC_USINT, 0, 7>{};
  var_s16result = 0_INT;
}

void FORTE_Q_SelectActiveWorkingSet::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendSelectActiveWorkingSet();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_SelectActiveWorkingSet::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_pau8Name, conn_pau8Name);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_SelectActiveWorkingSet::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_pau8OldName, conn_pau8OldName);
      writeData(2, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_SelectActiveWorkingSet::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_pau8Name;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SelectActiveWorkingSet::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_pau8OldName;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SelectActiveWorkingSet::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_SelectActiveWorkingSet::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_SelectActiveWorkingSet::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_pau8Name;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_SelectActiveWorkingSet::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_pau8OldName;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_SelectActiveWorkingSet::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_SelectActiveWorkingSet::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

void FORTE_Q_SelectActiveWorkingSet::iso_init(void) {
  // Set the "old Value" to invalid.
  var_pau8OldName[0] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[1] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[2] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[3] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[4] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[5] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[6] = static_cast<CIEC_USINT>(0xFF);
  var_pau8OldName[7] = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_SelectActiveWorkingSet::sendSelectActiveWorkingSet(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_pau8OldName[0], var_pau8Name[0]) //
      || func_NE(var_pau8OldName[1], var_pau8Name[1]) //
        || func_NE(var_pau8OldName[2], var_pau8Name[2]) //
        || func_NE(var_pau8OldName[3], var_pau8Name[3]) //
        || func_NE(var_pau8OldName[4], var_pau8Name[4]) //
        || func_NE(var_pau8OldName[5], var_pau8Name[5]) //
        || func_NE(var_pau8OldName[6], var_pau8Name[6]) //
        || func_NE(var_pau8OldName[7], var_pau8Name[7])) {
        ret = IsoVtcCmd_SelectActiveWorkingSet(u8VtcInstance, var_pau8Name.getDataPtr());
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_pau8OldName = var_pau8Name; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


