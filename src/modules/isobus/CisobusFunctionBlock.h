/*
 * CisobusFunctionBlock.h
 *
 *  Created on: 30.07.2022
 *      Author: franz
 */

#ifndef SRC_MODULES_ISOBUS_CISOBUSFUNCTIONBLOCK_H_
#define SRC_MODULES_ISOBUS_CISOBUSFUNCTIONBLOCK_H_

#include "funcbloc.h"

class CisobusFunctionBlock : public CFunctionBlock {


public:
    CisobusFunctionBlock(CResource *pa_poSrcRes, const SFBInterfaceSpec *pa_pstInterfaceSpec,
            CStringDictionary::TStringId pa_nInstanceNameId);

    ~CisobusFunctionBlock() override;

protected:
    static const CIEC_STRING scmOK;
    static const CIEC_STRING scmNotInitialised;
    static const CIEC_STRING scmInitialised;
    static const CIEC_STRING CfHandleMasterunvalid;
    static const CIEC_STRING VtcInstanceunvalid;
    static const CIEC_STRING scmOK_oldIsNew;
    static const CIEC_STRING scmE_OVERFLOW;
    static const CIEC_STRING scmE_NOACT;
    static const CIEC_STRING scmE_NO_INSTANCE;
    static const CIEC_STRING scmERR;
};


#endif /* SRC_MODULES_ISOBUS_CISOBUSFUNCTIONBLOCK_H_ */
