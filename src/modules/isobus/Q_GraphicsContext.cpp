/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_GraphicsContext
 *** Description: Command Graphics Context ( Part 6 - F.56 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_GraphicsContext.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_GraphicsContext_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_GraphicsContext, g_nStringIdQ_GraphicsContext)

const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8SubCommandID, g_nStringIdu8ParaList, g_nStringIdu16LenBytes};
const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdARRAY, static_cast<CStringDictionary::TStringId>(0), static_cast<CStringDictionary::TStringId>(4), g_nStringIdUSINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldSubCommandID, g_nStringIdu8OldParaList, g_nStringIdu16OldLenBytes, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdARRAY, static_cast<CStringDictionary::TStringId>(0), static_cast<CStringDictionary::TStringId>(4), g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_GraphicsContext::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_GraphicsContext::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_GraphicsContext::scmEOWith[] = {0, 4, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_GraphicsContext::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_GraphicsContext::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_GraphicsContext::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_GraphicsContext::FORTE_Q_GraphicsContext(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldSubCommandID(var_u8OldSubCommandID),
    var_conn_u8OldParaList(var_u8OldParaList),
    var_conn_u16OldLenBytes(var_u16OldLenBytes),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8SubCommandID(nullptr),
    conn_u8ParaList(nullptr),
    conn_u16LenBytes(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldSubCommandID(this, 1, &var_conn_u8OldSubCommandID),
    conn_u8OldParaList(this, 2, &var_conn_u8OldParaList),
    conn_u16OldLenBytes(this, 3, &var_conn_u16OldLenBytes),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_GraphicsContext::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8SubCommandID = 0_USINT;
  var_u8ParaList = CIEC_ARRAY_FIXED<CIEC_USINT, 0, 4>{};
  var_u16LenBytes = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldSubCommandID = 0_USINT;
  var_u8OldParaList = CIEC_ARRAY_FIXED<CIEC_USINT, 0, 4>{};
  var_u16OldLenBytes = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_GraphicsContext::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendGraphicsContext();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_GraphicsContext::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8SubCommandID, conn_u8SubCommandID);
      readData(2, var_u8ParaList, conn_u8ParaList);
      readData(3, var_u16LenBytes, conn_u16LenBytes);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_GraphicsContext::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(4, var_s16result, conn_s16result);
      writeData(1, var_u8OldSubCommandID, conn_u8OldSubCommandID);
      writeData(2, var_u8OldParaList, conn_u8OldParaList);
      writeData(3, var_u16OldLenBytes, conn_u16OldLenBytes);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_GraphicsContext::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8SubCommandID;
    case 2: return &var_u8ParaList;
    case 3: return &var_u16LenBytes;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_GraphicsContext::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldSubCommandID;
    case 2: return &var_u8OldParaList;
    case 3: return &var_u16OldLenBytes;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_GraphicsContext::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_GraphicsContext::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_GraphicsContext::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8SubCommandID;
    case 2: return &conn_u8ParaList;
    case 3: return &conn_u16LenBytes;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_GraphicsContext::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldSubCommandID;
    case 2: return &conn_u8OldParaList;
    case 3: return &conn_u16OldLenBytes;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_GraphicsContext::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_GraphicsContext::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_GraphicsContext::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldSubCommandID = static_cast<CIEC_USINT>(0xFF);
  var_u8OldParaList[0] = static_cast<CIEC_USINT>(0xFF);
  var_u16OldLenBytes = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_GraphicsContext::sendGraphicsContext(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldSubCommandID, var_u8SubCommandID) //
      || func_NE(var_u8OldParaList[0], var_u8ParaList[0]) //
      || func_NE(var_u16OldLenBytes, var_u16LenBytes)) {
        ret = IsoVtcCmd_GraphicsContext(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8SubCommandID), var_u8ParaList.getDataPtr(), static_cast<iso_u16>(var_u16LenBytes));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldSubCommandID = var_u8SubCommandID; //store this value.
            var_u8OldParaList = var_u8ParaList; //store this value.
            var_u16OldLenBytes = var_u16LenBytes; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


