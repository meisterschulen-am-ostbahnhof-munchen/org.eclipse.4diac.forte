/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_Size
 *** Description: Command change size ( Part 6 - F.18 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_Size.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_Size_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"


DEFINE_FIRMWARE_FB(FORTE_Q_Size, g_nStringIdQ_Size)

const CStringDictionary::TStringId FORTE_Q_Size::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu16Width, g_nStringIdu16Height};
const CStringDictionary::TStringId FORTE_Q_Size::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_Size::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu16OldWidth, g_nStringIdu16OldHeight, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_Size::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_Size::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_Size::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_Size::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_Size::scmEOWith[] = {0, 3, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_Size::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_Size::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_Size::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  3, scmDataInputNames, scmDataInputTypeIds,
  4, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_Size::FORTE_Q_Size(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u16OldWidth(var_u16OldWidth),
    var_conn_u16OldHeight(var_u16OldHeight),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u16Width(nullptr),
    conn_u16Height(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u16OldWidth(this, 1, &var_conn_u16OldWidth),
    conn_u16OldHeight(this, 2, &var_conn_u16OldHeight),
    conn_s16result(this, 3, &var_conn_s16result) {
};

void FORTE_Q_Size::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u16Width = 0_UINT;
  var_u16Height = 0_UINT;
  var_STATUS = ""_STRING;
  var_u16OldWidth = 0_UINT;
  var_u16OldHeight = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_Size::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendSize();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_Size::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u16Width, conn_u16Width);
      readData(2, var_u16Height, conn_u16Height);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_Size::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_s16result, conn_s16result);
      writeData(1, var_u16OldWidth, conn_u16OldWidth);
      writeData(2, var_u16OldHeight, conn_u16OldHeight);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_Size::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u16Width;
    case 2: return &var_u16Height;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_Size::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u16OldWidth;
    case 2: return &var_u16OldHeight;
    case 3: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_Size::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_Size::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_Size::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u16Width;
    case 2: return &conn_u16Height;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_Size::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u16OldWidth;
    case 2: return &conn_u16OldHeight;
    case 3: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_Size::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_Size::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_Size::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u16OldWidth = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldHeight = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_Size::sendSize(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u16OldWidth, var_u16Width) //
      || func_NE(var_u16OldHeight, var_u16Height)) {
        ret = IsoVtcCmd_Size(u8VtcInstance, u16ObjId, static_cast<iso_u16>(var_u16Width), static_cast<iso_u16>(var_u16Height));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u16OldWidth = var_u16Width; //store this value.
            var_u16OldHeight = var_u16Height; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

