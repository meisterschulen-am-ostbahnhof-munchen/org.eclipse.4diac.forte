/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ObjEnableDisable
 *** Description: Command enable/disable (input) object - ( Part 6 - F.4 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ObjEnableDisable.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ObjEnableDisable_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"
//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_ObjEnableDisable, g_nStringIdQ_ObjEnableDisable)

const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdqAbility};
const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdBOOL};
const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdqOldAbility, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdBOOL, g_nStringIdINT};
const TDataIOID FORTE_Q_ObjEnableDisable::scmEIWith[] = {0, scmWithListDelimiter, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ObjEnableDisable::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ObjEnableDisable::scmEOWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ObjEnableDisable::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ObjEnableDisable::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ObjEnableDisable::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ObjEnableDisable::FORTE_Q_ObjEnableDisable(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_qOldAbility(var_qOldAbility),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_qAbility(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_qOldAbility(this, 1, &var_conn_qOldAbility),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_ObjEnableDisable::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_qAbility = 0_BOOL;
  var_STATUS = ""_STRING;
  var_qOldAbility = 0_BOOL;
  var_s16result = 0_INT;
}

void FORTE_Q_ObjEnableDisable::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
        iso_init();
        sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
        sendObjEnableDisable();
        sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ObjEnableDisable::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_qAbility, conn_qAbility);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ObjEnableDisable::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_qOldAbility, conn_qOldAbility);
      writeData(2, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ObjEnableDisable::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_qAbility;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ObjEnableDisable::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_qOldAbility;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ObjEnableDisable::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ObjEnableDisable::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ObjEnableDisable::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_qAbility;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ObjEnableDisable::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_qOldAbility;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ObjEnableDisable::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ObjEnableDisable::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ObjEnableDisable::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_qOldAbility = static_cast<CIEC_BOOL>(0xFF) ; //TODO !! is false or true right ?
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ObjEnableDisable::sendObjEnableDisable(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_qOldAbility, var_qAbility)) {
        ret = IsoVtcCmd_ObjEnableDisable(u8VtcInstance, u16ObjId, var_qAbility);
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_qOldAbility = var_qAbility; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


