/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: CbVtStatus
 *** Description: This function is called in case of every page change - you can do e. g. initialisations ...
 *** Version:
 ***     1.0: 2022-07-20/franz -  -
 *************************************************************************/

#include "CbVtStatus.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "CbVtStatus_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


void CbVtStatus(const ISOVT_STATUS_DATA_T* psStatusData) {
  FORTE_CbVtStatus::CbVtStatusHandler::CbVtStatus(psStatusData); //forward Call
}


const CIEC_STRING FORTE_CbVtStatus::scmOK                 = "OK"_STRING;
const CIEC_STRING FORTE_CbVtStatus::scmNotInitialised     = "Not initialized"_STRING;

DEFINE_FIRMWARE_FB(FORTE_CbVtStatus, g_nStringIdCbVtStatus)

const CStringDictionary::TStringId FORTE_CbVtStatus::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8Instance, g_nStringIdqWsActive, g_nStringIdwPage};
const CStringDictionary::TStringId FORTE_CbVtStatus::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdBOOL, g_nStringIdUINT};
const TForteInt16 FORTE_CbVtStatus::scmEIWithIndexes[] = {-1};
const CStringDictionary::TStringId FORTE_CbVtStatus::scmEventInputNames[] = {g_nStringIdINIT};
const TDataIOID FORTE_CbVtStatus::scmEOWith[] = {0, scmWithListDelimiter, 0, 3, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_CbVtStatus::scmEOWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_CbVtStatus::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdIND};
const SFBInterfaceSpec FORTE_CbVtStatus::scmFBInterfaceSpec = {
  1, scmEventInputNames, nullptr, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  0, nullptr, nullptr,
  4, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_CbVtStatus::FORTE_CbVtStatus(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CEventSourceFB(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8Instance(var_u8Instance),
    var_conn_qWsActive(var_qWsActive),
    var_conn_wPage(var_wPage),
    conn_INITO(this, 0),
    conn_IND(this, 1),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8Instance(this, 1, &var_conn_u8Instance),
    conn_qWsActive(this, 2, &var_conn_qWsActive),
    conn_wPage(this, 3, &var_conn_wPage) {
};

void FORTE_CbVtStatus::setInitialValues() {
  var_STATUS = ""_STRING;
  var_u8Instance = 0_USINT;
  var_qWsActive = 0_BOOL;
  var_wPage = 0_UINT;
}

void FORTE_CbVtStatus::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case cgExternalEventID:
      sendOutputEvent(scmEventINDID);
      break;
    case scmEventINITID:
        sendOutputEvent(scmEventINITOID);
      break;
  }
}

void FORTE_CbVtStatus::readInputData(TEventID) {
  // nothing to do
}

void FORTE_CbVtStatus::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITOID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      break;
    }
    case scmEventINDID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_wPage, conn_wPage);
      writeData(2, var_qWsActive, conn_qWsActive);
      writeData(1, var_u8Instance, conn_u8Instance);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_CbVtStatus::getDI(size_t) {
  return nullptr;
}

CIEC_ANY *FORTE_CbVtStatus::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8Instance;
    case 2: return &var_qWsActive;
    case 3: return &var_wPage;
  }
  return nullptr;
}

CIEC_ANY *FORTE_CbVtStatus::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_CbVtStatus::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_IND;
  }
  return nullptr;
}

CDataConnection **FORTE_CbVtStatus::getDIConUnchecked(TPortId) {
  return nullptr;
}

CDataConnection *FORTE_CbVtStatus::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8Instance;
    case 2: return &conn_qWsActive;
    case 3: return &conn_wPage;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_CbVtStatus::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_CbVtStatus::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}



FORTE_CbVtStatus::CbVtStatusHandler* FORTE_CbVtStatus::CbVtStatusHandler::g_vth = nullptr;  //Pointer to instance
FORTE_CbVtStatus                   * FORTE_CbVtStatus::g_vts                    = nullptr;  //Pointer to instance




DEFINE_HANDLER(FORTE_CbVtStatus::CbVtStatusHandler);


FORTE_CbVtStatus::CbVtStatusHandler::CbVtStatusHandler(CDeviceExecution& pa_poDeviceExecution)
: CExternalEventHandler(pa_poDeviceExecution)
{
  g_vth = this;
}

FORTE_CbVtStatus::CbVtStatusHandler::~CbVtStatusHandler() {
  g_vth = nullptr;
}


//Static Callback
void FORTE_CbVtStatus::CbVtStatusHandler::CbVtStatus(const ISOVT_STATUS_DATA_T *psStatusData) {
  if(g_vth == nullptr)
  {
    //no callback registered
  } else {
    g_vth->CbVtStatus_ic(psStatusData);
  }
}

//in-Class Callback
void FORTE_CbVtStatus::CbVtStatusHandler::CbVtStatus_ic(const ISOVT_STATUS_DATA_T *psStatusData) {
  if(FORTE_CbVtStatus::g_vts == nullptr) {
    //no callback registered
  } else {
    FORTE_CbVtStatus::g_vts->CbVtStatus(psStatusData);
    startNewEventChain(FORTE_CbVtStatus::g_vts);
  }
}

void FORTE_CbVtStatus::CbVtStatus(const ISOVT_STATUS_DATA_T *psStatusData) {
  var_STATUS     = scmOK;
  var_u8Instance = CIEC_USINT(psStatusData->u8Instance);
  var_qWsActive  = CIEC_BOOL(psStatusData->qWsActive);
  var_wPage      = CIEC_UINT(psStatusData->wPage);
}


void FORTE_CbVtStatus::CbVtStatusHandler::enableHandler() {
  //do nothing
}

void FORTE_CbVtStatus::CbVtStatusHandler::disableHandler() {
  //do nothing
}

void FORTE_CbVtStatus::CbVtStatusHandler::setPriority(int) {
  //do nothing
}



int FORTE_CbVtStatus::CbVtStatusHandler::getPriority() const {
  return 0;
}


