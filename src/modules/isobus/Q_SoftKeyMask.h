/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SoftKeyMask
 *** Description: Command change soft key mask( Part 6 - F.36 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_SoftKeyMask final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_SoftKeyMask)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendSoftKeyMask(void);

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_SoftKeyMask(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_USINT var_u8MaskType;
  CIEC_UINT var_u16DataMaskId;
  CIEC_UINT var_u16SoftKeyMaskId;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldMaskType;
  CIEC_UINT var_u16OldDataMaskId;
  CIEC_UINT var_u16OldSoftKeyMaskId;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldMaskType;
  CIEC_UINT var_conn_u16OldDataMaskId;
  CIEC_UINT var_conn_u16OldSoftKeyMaskId;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u8MaskType;
  CDataConnection *conn_u16DataMaskId;
  CDataConnection *conn_u16SoftKeyMaskId;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldMaskType;
  CDataConnection conn_u16OldDataMaskId;
  CDataConnection conn_u16OldSoftKeyMaskId;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_USINT &pau8MaskType, const CIEC_UINT &pau16DataMaskId, const CIEC_UINT &pau16SoftKeyMaskId, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldMaskType, CIEC_UINT &pau16OldDataMaskId, CIEC_UINT &pau16OldSoftKeyMaskId, CIEC_INT &pas16result) {
    var_u8MaskType = pau8MaskType;
    var_u16DataMaskId = pau16DataMaskId;
    var_u16SoftKeyMaskId = pau16SoftKeyMaskId;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldMaskType = var_u8OldMaskType;
    pau16OldDataMaskId = var_u16OldDataMaskId;
    pau16OldSoftKeyMaskId = var_u16OldSoftKeyMaskId;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_USINT &pau8MaskType, const CIEC_UINT &pau16DataMaskId, const CIEC_UINT &pau16SoftKeyMaskId, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldMaskType, CIEC_UINT &pau16OldDataMaskId, CIEC_UINT &pau16OldSoftKeyMaskId, CIEC_INT &pas16result) {
    var_u8MaskType = pau8MaskType;
    var_u16DataMaskId = pau16DataMaskId;
    var_u16SoftKeyMaskId = pau16SoftKeyMaskId;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldMaskType = var_u8OldMaskType;
    pau16OldDataMaskId = var_u16OldDataMaskId;
    pau16OldSoftKeyMaskId = var_u16OldSoftKeyMaskId;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_USINT &pau8MaskType, const CIEC_UINT &pau16DataMaskId, const CIEC_UINT &pau16SoftKeyMaskId, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldMaskType, CIEC_UINT &pau16OldDataMaskId, CIEC_UINT &pau16OldSoftKeyMaskId, CIEC_INT &pas16result) {
    evt_INIT(pau8MaskType, pau16DataMaskId, pau16SoftKeyMaskId, paSTATUS, pau8OldMaskType, pau16OldDataMaskId, pau16OldSoftKeyMaskId, pas16result);
  }
};


