/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SelectColourMap
 *** Description: Command Select Colour Map ( VT version 4 and later ) ( Part 6 - F.60 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_SelectColourMap.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_SelectColourMap_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_SelectColourMap, g_nStringIdQ_SelectColourMap)

const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmDataInputNames[] = {g_nStringIdu16ObjIdColourMap};
const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmDataInputTypeIds[] = {g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu16OldObjIdColourMap, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_SelectColourMap::scmEIWith[] = {0, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SelectColourMap::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_SelectColourMap::scmEOWith[] = {0, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SelectColourMap::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SelectColourMap::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_SelectColourMap::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  1, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_SelectColourMap::FORTE_Q_SelectColourMap(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u16OldObjIdColourMap(var_u16OldObjIdColourMap),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjIdColourMap(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u16OldObjIdColourMap(this, 1, &var_conn_u16OldObjIdColourMap),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_SelectColourMap::setInitialValues() {
  var_u16ObjIdColourMap = 0_UINT;
  var_STATUS = ""_STRING;
  var_u16OldObjIdColourMap = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_SelectColourMap::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendSelectColourMap();
      sendOutputEvent(scmEventCNFID);
      break;

  }
}

void FORTE_Q_SelectColourMap::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjIdColourMap, conn_u16ObjIdColourMap);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_SelectColourMap::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(2, var_s16result, conn_s16result);
      writeData(1, var_u16OldObjIdColourMap, conn_u16OldObjIdColourMap);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_SelectColourMap::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjIdColourMap;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SelectColourMap::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u16OldObjIdColourMap;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SelectColourMap::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_SelectColourMap::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_SelectColourMap::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjIdColourMap;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_SelectColourMap::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u16OldObjIdColourMap;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_SelectColourMap::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_SelectColourMap::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_SelectColourMap::iso_init(void) {
    // Set the "old Value" to invalid.
    var_u16OldObjIdColourMap = static_cast<CIEC_UINT>(0xFFFF);
    var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_SelectColourMap::sendSelectColourMap(void) {
    iso_s16 ret = E_UNKNOWN_ERR;
    iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
    s16CfHandleMaster = AppGetCfHandleMaster();
    if (s16CfHandleMaster != HANDLE_UNVALID) {
        iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
        u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
        if (u8VtcInstance != ISO_INSTANCE_INVALID) {
            if (func_NE(var_u16OldObjIdColourMap, var_u16ObjIdColourMap)) {
                ret = IsoVtcCmd_SelectColourMap(u8VtcInstance, static_cast<iso_u16>(var_u16ObjIdColourMap));
                switch (ret) {
                case E_NO_ERR: //- OK
                    var_STATUS = scmOK;
                    var_u16OldObjIdColourMap = var_u16ObjIdColourMap; //store this value.
                    break;
                case E_OVERFLOW: //- buffer overflow
                    var_STATUS = scmE_OVERFLOW;
                    break;
                case E_NOACT: //- Command not possible in current state
                    var_STATUS = scmE_NOACT;
                    break;
                case E_NO_INSTANCE: //- No VT client available
                    var_STATUS = scmE_NO_INSTANCE;
                    break;
                default:
                    var_STATUS = scmERR; //- Unknown Error
                    break;
                }
            }
            else {
                var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
            }
        }
        else {
            var_STATUS = VtcInstanceunvalid;
        }
    }
    else {
        var_STATUS = CfHandleMasterunvalid;
    }
    var_s16result = static_cast<CIEC_INT>(ret);
}

