/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChangePolygonPoint
 *** Description: Command Change Polygon Point( Part 6 - F.52 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ChangePolygonPoint.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ChangePolygonPoint_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"



#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_ChangePolygonPoint, g_nStringIdQ_ChangePolygonPoint)

const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8PointIndex, g_nStringIdu16NewXValue, g_nStringIdu16NewYValue};
const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldPointIndex, g_nStringIdu16OldXValue, g_nStringIdu16OldYValue, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ChangePolygonPoint::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChangePolygonPoint::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ChangePolygonPoint::scmEOWith[] = {0, 4, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChangePolygonPoint::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ChangePolygonPoint::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ChangePolygonPoint::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ChangePolygonPoint::FORTE_Q_ChangePolygonPoint(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldPointIndex(var_u8OldPointIndex),
    var_conn_u16OldXValue(var_u16OldXValue),
    var_conn_u16OldYValue(var_u16OldYValue),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8PointIndex(nullptr),
    conn_u16NewXValue(nullptr),
    conn_u16NewYValue(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldPointIndex(this, 1, &var_conn_u8OldPointIndex),
    conn_u16OldXValue(this, 2, &var_conn_u16OldXValue),
    conn_u16OldYValue(this, 3, &var_conn_u16OldYValue),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_ChangePolygonPoint::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8PointIndex = 0_USINT;
  var_u16NewXValue = 0_UINT;
  var_u16NewYValue = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldPointIndex = 0_USINT;
  var_u16OldXValue = 0_UINT;
  var_u16OldYValue = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ChangePolygonPoint::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendChangePolygonPoint();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ChangePolygonPoint::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8PointIndex, conn_u8PointIndex);
      readData(2, var_u16NewXValue, conn_u16NewXValue);
      readData(3, var_u16NewYValue, conn_u16NewYValue);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ChangePolygonPoint::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(4, var_s16result, conn_s16result);
      writeData(1, var_u8OldPointIndex, conn_u8OldPointIndex);
      writeData(2, var_u16OldXValue, conn_u16OldXValue);
      writeData(3, var_u16OldYValue, conn_u16OldYValue);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ChangePolygonPoint::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8PointIndex;
    case 2: return &var_u16NewXValue;
    case 3: return &var_u16NewYValue;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChangePolygonPoint::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldPointIndex;
    case 2: return &var_u16OldXValue;
    case 3: return &var_u16OldYValue;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChangePolygonPoint::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ChangePolygonPoint::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ChangePolygonPoint::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8PointIndex;
    case 2: return &conn_u16NewXValue;
    case 3: return &conn_u16NewYValue;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ChangePolygonPoint::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldPointIndex;
    case 2: return &conn_u16OldXValue;
    case 3: return &conn_u16OldYValue;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ChangePolygonPoint::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ChangePolygonPoint::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}



void FORTE_Q_ChangePolygonPoint::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldPointIndex = static_cast<CIEC_USINT>(0xFF);
  var_u16OldXValue = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldYValue = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ChangePolygonPoint::sendChangePolygonPoint(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldPointIndex, var_u8PointIndex) //
      || func_NE(var_u16OldXValue, var_u16NewXValue) //
      || func_NE(var_u16OldYValue, var_u16NewYValue)) {
        ret = IsoVtcCmd_ChangePolygonPoint(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8PointIndex), static_cast<iso_u16>(var_u16NewXValue), static_cast<iso_u16>(var_u16NewYValue));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldPointIndex = var_u8PointIndex; //store this value.
            var_u16OldXValue = var_u16NewXValue; //store this value.
            var_u16OldYValue = var_u16NewYValue; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


