/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: CbVtStatus
 *** Description: This function is called in case of every page change - you can do e. g. initialisations ...
 *** Version:
 ***     1.0: 2022-07-20/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "forte_string.h"
#include "forte_usint.h"
#include "forte_bool.h"
#include "forte_uint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


#include "extevhan.h"
#include "esfb.h"
#include "resource.h"

#include "IsoDef.h"


extern "C" void CbVtStatus(const ISOVT_STATUS_DATA_T* psStatusData);


class FORTE_CbVtStatus final : public CEventSourceFB {
  DECLARE_FIRMWARE_FB(FORTE_CbVtStatus)

public:
  class CbVtStatusHandler : public CExternalEventHandler{
  DECLARE_HANDLER(CbVtStatusHandler)
    ;
    public:
      //Static Callback
      static void CbVtStatus(const ISOVT_STATUS_DATA_T* psStatusData);
      //in-Class Callback
      void CbVtStatus_ic(const ISOVT_STATUS_DATA_T* psStatusData);

      /* functions needed for the external event handler interface */
      void enableHandler() override;
      void disableHandler() override;
      void setPriority(int paPriority) override;
      int getPriority() const override;
    private:
      static FORTE_CbVtStatus::CbVtStatusHandler* g_vth;  //Pointer to instance
  };

private:
  
  static FORTE_CbVtStatus* g_vts;  //Pointer to instance

  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventINDID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;
  //in-Class Callback
  void CbVtStatus(const ISOVT_STATUS_DATA_T* psStatusData);

public:
  FORTE_CbVtStatus(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8Instance;
  CIEC_BOOL var_qWsActive;
  CIEC_UINT var_wPage;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8Instance;
  CIEC_BOOL var_conn_qWsActive;
  CIEC_UINT var_conn_wPage;

  CEventConnection conn_INITO;
  CEventConnection conn_IND;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8Instance;
  CDataConnection conn_qWsActive;
  CDataConnection conn_wPage;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(CIEC_STRING &paSTATUS, CIEC_USINT &pau8Instance, CIEC_BOOL &paqWsActive, CIEC_UINT &pawPage) {
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8Instance = var_u8Instance;
    paqWsActive = var_qWsActive;
    pawPage = var_wPage;
  }

  void operator()(CIEC_STRING &paSTATUS, CIEC_USINT &pau8Instance, CIEC_BOOL &paqWsActive, CIEC_UINT &pawPage) {
    evt_INIT(paSTATUS, pau8Instance, paqWsActive, pawPage);
  }
  
private:
  static const CIEC_STRING scmOK;
  static const CIEC_STRING scmNotInitialised;
};


