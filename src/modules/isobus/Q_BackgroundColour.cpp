/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_BackgroundColour
 *** Description: Command change background colour ( Part 6 - F.20 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_BackgroundColour.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_BackgroundColour_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX


DEFINE_FIRMWARE_FB(FORTE_Q_BackgroundColour, g_nStringIdQ_BackgroundColour)

const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8Colour};
const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldColour, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_BackgroundColour::scmEIWith[] = {0, scmWithListDelimiter, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_BackgroundColour::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_BackgroundColour::scmEOWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_BackgroundColour::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_BackgroundColour::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_BackgroundColour::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_BackgroundColour::FORTE_Q_BackgroundColour(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldColour(var_u8OldColour),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8Colour(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldColour(this, 1, &var_conn_u8OldColour),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_BackgroundColour::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8Colour = 0_USINT;
  var_STATUS = ""_STRING;
  var_u8OldColour = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_BackgroundColour::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendBackgroundColour();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_BackgroundColour::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8Colour, conn_u8Colour);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_BackgroundColour::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_u8OldColour, conn_u8OldColour);
      writeData(2, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_BackgroundColour::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8Colour;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_BackgroundColour::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldColour;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_BackgroundColour::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_BackgroundColour::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_BackgroundColour::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8Colour;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_BackgroundColour::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldColour;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_BackgroundColour::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_BackgroundColour::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

void FORTE_Q_BackgroundColour::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldColour = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_BackgroundColour::sendBackgroundColour(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldColour, var_u8Colour)) {
        ret = IsoVtcCmd_BackgroundColour(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8Colour));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldColour = var_u8Colour; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

