/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChildLocation
 *** Description: Command change child location ( Part 6 - F.14 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ChildLocation.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ChildLocation_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_ChildLocation, g_nStringIdQ_ChildLocation)

const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu16ObjIdParent, g_nStringIdu8Xchange, g_nStringIdu8Ychange};
const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldXchange, g_nStringIdu8OldYchange, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ChildLocation::scmEIWith[] = {0, 1, scmWithListDelimiter, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChildLocation::scmEIWithIndexes[] = {0, 3};
const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ChildLocation::scmEOWith[] = {0, 2, 1, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChildLocation::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ChildLocation::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ChildLocation::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  4, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ChildLocation::FORTE_Q_ChildLocation(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldXchange(var_u8OldXchange),
    var_conn_u8OldYchange(var_u8OldYchange),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u16ObjIdParent(nullptr),
    conn_u8Xchange(nullptr),
    conn_u8Ychange(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldXchange(this, 1, &var_conn_u8OldXchange),
    conn_u8OldYchange(this, 2, &var_conn_u8OldYchange),
    conn_s16result(this, 3, &var_conn_s16result) {
};

void FORTE_Q_ChildLocation::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u16ObjIdParent = 0_UINT;
  var_u8Xchange = 0_USINT;
  var_u8Ychange = 0_USINT;
  var_STATUS = ""_STRING;
  var_u8OldXchange = 0_USINT;
  var_u8OldYchange = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ChildLocation::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendChildLoation();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ChildLocation::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      readData(1, var_u16ObjIdParent, conn_u16ObjIdParent);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(2, var_u8Xchange, conn_u8Xchange);
      readData(3, var_u8Ychange, conn_u8Ychange);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ChildLocation::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(2, var_u8OldYchange, conn_u8OldYchange);
      writeData(1, var_u8OldXchange, conn_u8OldXchange);
      writeData(3, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ChildLocation::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u16ObjIdParent;
    case 2: return &var_u8Xchange;
    case 3: return &var_u8Ychange;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChildLocation::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldXchange;
    case 2: return &var_u8OldYchange;
    case 3: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChildLocation::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ChildLocation::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ChildLocation::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u16ObjIdParent;
    case 2: return &conn_u8Xchange;
    case 3: return &conn_u8Ychange;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ChildLocation::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldXchange;
    case 2: return &conn_u8OldYchange;
    case 3: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ChildLocation::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ChildLocation::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

void FORTE_Q_ChildLocation::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  u16ObjIdParent = static_cast<iso_u16>(var_u16ObjIdParent);
  // Set the "old Value" to invalid.
  var_u8OldXchange = static_cast<CIEC_USINT>(0xFF);
  var_u8OldYchange = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ChildLocation::sendChildLoation(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldXchange, var_u8Xchange) //
      || func_NE(var_u8OldYchange, var_u8Ychange)) {
        ret = IsoVtcCmd_ChildLocation(u8VtcInstance, u16ObjId, u16ObjIdParent, static_cast<iso_u8>(var_u8Xchange), static_cast<iso_u8>(var_u8Ychange));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldXchange = var_u8Xchange; //store this value.
            var_u8OldYchange = var_u8Ychange; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


