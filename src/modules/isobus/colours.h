/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: colours
 *************************************************************************/

#ifndef _COLOURS_H_
#define _COLOURS_H_

#include "forte_usint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

extern const CIEC_USINT st_global_COLOR_BLACK;
extern const CIEC_USINT st_global_COLOR_WHITE;
extern const CIEC_USINT st_global_COLOR_GREEN;
extern const CIEC_USINT st_global_COLOR_TEAL;
extern const CIEC_USINT st_global_COLOR_MAROON;
extern const CIEC_USINT st_global_COLOR_PURPLE;
extern const CIEC_USINT st_global_COLOR_OLIVE;
extern const CIEC_USINT st_global_COLOR_SILVER;
extern const CIEC_USINT st_global_COLOR_GREY;
extern const CIEC_USINT st_global_COLOR_BLUE;
extern const CIEC_USINT st_global_COLOR_LIME;
extern const CIEC_USINT st_global_COLOR_CYAN;
extern const CIEC_USINT st_global_COLOR_RED;
extern const CIEC_USINT st_global_COLOR_MAGENTA;
extern const CIEC_USINT st_global_COLOR_YELLOW;
extern const CIEC_USINT st_global_COLOR_NAVY;

#endif // _COLOURS_H_
