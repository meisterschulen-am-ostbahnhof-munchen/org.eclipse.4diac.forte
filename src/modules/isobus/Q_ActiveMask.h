/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ActiveMask
 *** Description: Command change active mask( Part 6 - F.34 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


#include "IsoDef.h"


class FORTE_Q_ActiveMask final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ActiveMask)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendActiveMask(void);

  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ActiveMask(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16WorkSetId;
  CIEC_UINT var_u16NewMaskId;

  CIEC_STRING var_STATUS;
  CIEC_UINT var_u16OldMaskId;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_UINT var_conn_u16OldMaskId;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16WorkSetId;
  CDataConnection *conn_u16NewMaskId;

  CDataConnection conn_STATUS;
  CDataConnection conn_u16OldMaskId;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16WorkSetId, const CIEC_UINT &pau16NewMaskId, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldMaskId, CIEC_INT &pas16result) {
    var_u16WorkSetId = pau16WorkSetId;
    var_u16NewMaskId = pau16NewMaskId;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldMaskId = var_u16OldMaskId;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16WorkSetId, const CIEC_UINT &pau16NewMaskId, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldMaskId, CIEC_INT &pas16result) {
    var_u16WorkSetId = pau16WorkSetId;
    var_u16NewMaskId = pau16NewMaskId;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldMaskId = var_u16OldMaskId;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16WorkSetId, const CIEC_UINT &pau16NewMaskId, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldMaskId, CIEC_INT &pas16result) {
    evt_INIT(pau16WorkSetId, pau16NewMaskId, paSTATUS, pau16OldMaskId, pas16result);
  }
};


