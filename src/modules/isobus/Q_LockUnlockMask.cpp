/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_LockUnlockMask
 *** Description: Command Lock/UnlockMask( Part 6 - F.46 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_LockUnlockMask.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_LockUnlockMask_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_LockUnlockMask, g_nStringIdQ_LockUnlockMask)

const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmDataInputNames[] = {g_nStringIdu8LockCmd, g_nStringIdu16MaskId, g_nStringIdu16LockTimeoutMs};
const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmDataInputTypeIds[] = {g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldLockCmd, g_nStringIdu16OldMaskId, g_nStringIdu16OldLockTimeoutMs, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_LockUnlockMask::scmEIWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_LockUnlockMask::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_LockUnlockMask::scmEOWith[] = {0, 4, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_LockUnlockMask::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_LockUnlockMask::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_LockUnlockMask::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  3, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_LockUnlockMask::FORTE_Q_LockUnlockMask(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldLockCmd(var_u8OldLockCmd),
    var_conn_u16OldMaskId(var_u16OldMaskId),
    var_conn_u16OldLockTimeoutMs(var_u16OldLockTimeoutMs),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u8LockCmd(nullptr),
    conn_u16MaskId(nullptr),
    conn_u16LockTimeoutMs(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldLockCmd(this, 1, &var_conn_u8OldLockCmd),
    conn_u16OldMaskId(this, 2, &var_conn_u16OldMaskId),
    conn_u16OldLockTimeoutMs(this, 3, &var_conn_u16OldLockTimeoutMs),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_LockUnlockMask::setInitialValues() {
  var_u8LockCmd = 0_USINT;
  var_u16MaskId = 0_UINT;
  var_u16LockTimeoutMs = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldLockCmd = 0_USINT;
  var_u16OldMaskId = 0_UINT;
  var_u16OldLockTimeoutMs = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_LockUnlockMask::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendLockUnlockMask();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_LockUnlockMask::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u8LockCmd, conn_u8LockCmd);
      readData(1, var_u16MaskId, conn_u16MaskId);
      readData(2, var_u16LockTimeoutMs, conn_u16LockTimeoutMs);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_LockUnlockMask::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(4, var_s16result, conn_s16result);
      writeData(1, var_u8OldLockCmd, conn_u8OldLockCmd);
      writeData(2, var_u16OldMaskId, conn_u16OldMaskId);
      writeData(3, var_u16OldLockTimeoutMs, conn_u16OldLockTimeoutMs);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_LockUnlockMask::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u8LockCmd;
    case 1: return &var_u16MaskId;
    case 2: return &var_u16LockTimeoutMs;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_LockUnlockMask::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldLockCmd;
    case 2: return &var_u16OldMaskId;
    case 3: return &var_u16OldLockTimeoutMs;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_LockUnlockMask::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_LockUnlockMask::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_LockUnlockMask::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u8LockCmd;
    case 1: return &conn_u16MaskId;
    case 2: return &conn_u16LockTimeoutMs;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_LockUnlockMask::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldLockCmd;
    case 2: return &conn_u16OldMaskId;
    case 3: return &conn_u16OldLockTimeoutMs;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_LockUnlockMask::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_LockUnlockMask::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_LockUnlockMask::iso_init(void) {
    // Set the "old Value" to invalid.
    var_u8OldLockCmd = static_cast<CIEC_USINT>(0xFF);
    var_u16OldMaskId = static_cast<CIEC_UINT>(0xFFFF);
    var_u16OldLockTimeoutMs = static_cast<CIEC_UINT>(0xFFFF);
    var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_LockUnlockMask::sendLockUnlockMask(void) {
    iso_s16 ret = E_UNKNOWN_ERR;
    iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
    s16CfHandleMaster = AppGetCfHandleMaster();
    if (s16CfHandleMaster != HANDLE_UNVALID) {
        iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
        u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
        if (u8VtcInstance != ISO_INSTANCE_INVALID) {
            ret = IsoVtcCmd_LockUnlockMask(u8VtcInstance, static_cast<iso_u8>(var_u8LockCmd), static_cast<iso_u16>(var_u16MaskId), static_cast<iso_u16>(var_u16LockTimeoutMs));
            switch (ret) {
            case E_NO_ERR: //- OK
                var_STATUS = scmOK;
                var_u8OldLockCmd = var_u8LockCmd; //store this value.
                var_u16OldMaskId = var_u16MaskId; //store this value.
                var_u16OldLockTimeoutMs = var_u16OldLockTimeoutMs; //store this value.
                break;
            case E_OVERFLOW: //- buffer overflow
                var_STATUS = scmE_OVERFLOW;
                break;
            case E_NOACT: //- Command not possible in current state
                var_STATUS = scmE_NOACT;
                break;
            case E_NO_INSTANCE: //- No VT client available
                var_STATUS = scmE_NO_INSTANCE;
                break;
            default:
                var_STATUS = scmERR; //- Unknown Error
                break;
            }
        }
        else {
            var_STATUS = VtcInstanceunvalid;
        }
    }
    else {
        var_STATUS = CfHandleMasterunvalid;
    }
    var_s16result = static_cast<CIEC_INT>(ret);
}


