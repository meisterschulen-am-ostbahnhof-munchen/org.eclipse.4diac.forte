/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_NumericValueAux
 *** Description: Command change numeric value ( Part 6 - F.22 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_NumericValueAux.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_NumericValueAux_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetGAuxu8ConnHnd.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_NumericValueAux, g_nStringIdQ_NumericValueAux)

const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu32NewValue};
const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUDINT};
const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu32OldValue, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUDINT, g_nStringIdINT};
const TDataIOID FORTE_Q_NumericValueAux::scmEIWith[] = {0, scmWithListDelimiter, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_NumericValueAux::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_NumericValueAux::scmEOWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_NumericValueAux::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_NumericValueAux::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_NumericValueAux::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_NumericValueAux::FORTE_Q_NumericValueAux(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u32OldValue(var_u32OldValue),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u32NewValue(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u32OldValue(this, 1, &var_conn_u32OldValue),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_NumericValueAux::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u32NewValue = 0_UDINT;
  var_STATUS = ""_STRING;
  var_u32OldValue = 0_UDINT;
  var_s16result = 0_INT;
}

void FORTE_Q_NumericValueAux::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendNumericValue();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_NumericValueAux::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u32NewValue, conn_u32NewValue);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_NumericValueAux::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_u32OldValue, conn_u32OldValue);
      writeData(2, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_NumericValueAux::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u32NewValue;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_NumericValueAux::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u32OldValue;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_NumericValueAux::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_NumericValueAux::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_NumericValueAux::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u32NewValue;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_NumericValueAux::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u32OldValue;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_NumericValueAux::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_NumericValueAux::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_NumericValueAux::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u32OldValue = static_cast<CIEC_UDINT>(0xFFFFFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_NumericValueAux::sendNumericValue(void) {
  iso_s16 ret = E_UNKNOWN_ERR;

  if(true) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = AppGetGAuxu8ConnHnd();
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u32OldValue, var_u32NewValue)) {
        ret = IsoVtcCmd_NumericValue(u8VtcInstance, u16ObjId, static_cast<iso_u32>(var_u32NewValue));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u32OldValue = var_u32NewValue; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}



