/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_CtrlAudioSignal
 *** Description: Command Control audio device ( Part 6 - F.10 ).
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_usint.h"
#include "forte_uint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_CtrlAudioSignal final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_CtrlAudioSignal)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendCtrlAudioSignal(void);
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_CtrlAudioSignal(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_USINT var_u8NumOfRepit;
  CIEC_UINT var_u16Frequency;
  CIEC_UINT var_u16OnTimeMs;
  CIEC_UINT var_u16OffTimeMs;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldNumOfRepit;
  CIEC_UINT var_u16OldFrequency;
  CIEC_UINT var_u16OldOnTimeMs;
  CIEC_UINT var_u16OldOffTimeMs;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldNumOfRepit;
  CIEC_UINT var_conn_u16OldFrequency;
  CIEC_UINT var_conn_u16OldOnTimeMs;
  CIEC_UINT var_conn_u16OldOffTimeMs;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u8NumOfRepit;
  CDataConnection *conn_u16Frequency;
  CDataConnection *conn_u16OnTimeMs;
  CDataConnection *conn_u16OffTimeMs;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldNumOfRepit;
  CDataConnection conn_u16OldFrequency;
  CDataConnection conn_u16OldOnTimeMs;
  CDataConnection conn_u16OldOffTimeMs;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_USINT &pau8NumOfRepit, const CIEC_UINT &pau16Frequency, const CIEC_UINT &pau16OnTimeMs, const CIEC_UINT &pau16OffTimeMs, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldNumOfRepit, CIEC_UINT &pau16OldFrequency, CIEC_UINT &pau16OldOnTimeMs, CIEC_UINT &pau16OldOffTimeMs, CIEC_INT &pas16result) {
    var_u8NumOfRepit = pau8NumOfRepit;
    var_u16Frequency = pau16Frequency;
    var_u16OnTimeMs = pau16OnTimeMs;
    var_u16OffTimeMs = pau16OffTimeMs;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldNumOfRepit = var_u8OldNumOfRepit;
    pau16OldFrequency = var_u16OldFrequency;
    pau16OldOnTimeMs = var_u16OldOnTimeMs;
    pau16OldOffTimeMs = var_u16OldOffTimeMs;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_USINT &pau8NumOfRepit, const CIEC_UINT &pau16Frequency, const CIEC_UINT &pau16OnTimeMs, const CIEC_UINT &pau16OffTimeMs, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldNumOfRepit, CIEC_UINT &pau16OldFrequency, CIEC_UINT &pau16OldOnTimeMs, CIEC_UINT &pau16OldOffTimeMs, CIEC_INT &pas16result) {
    var_u8NumOfRepit = pau8NumOfRepit;
    var_u16Frequency = pau16Frequency;
    var_u16OnTimeMs = pau16OnTimeMs;
    var_u16OffTimeMs = pau16OffTimeMs;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldNumOfRepit = var_u8OldNumOfRepit;
    pau16OldFrequency = var_u16OldFrequency;
    pau16OldOnTimeMs = var_u16OldOnTimeMs;
    pau16OldOffTimeMs = var_u16OldOffTimeMs;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_USINT &pau8NumOfRepit, const CIEC_UINT &pau16Frequency, const CIEC_UINT &pau16OnTimeMs, const CIEC_UINT &pau16OffTimeMs, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldNumOfRepit, CIEC_UINT &pau16OldFrequency, CIEC_UINT &pau16OldOnTimeMs, CIEC_UINT &pau16OldOffTimeMs, CIEC_INT &pas16result) {
    evt_INIT(pau8NumOfRepit, pau16Frequency, pau16OnTimeMs, pau16OffTimeMs, paSTATUS, pau8OldNumOfRepit, pau16OldFrequency, pau16OldOnTimeMs, pau16OldOffTimeMs, pas16result);
  }
};


