/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChildPosition
 *** Description: Command change child position ( Part 6 - F.16 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ChildPosition.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ChildPosition_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_ChildPosition, g_nStringIdQ_ChildPosition)

const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu16ObjIdParent, g_nStringIds16Xposition, g_nStringIds16Yposition};
const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT, g_nStringIdINT};
const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIds16OldXposition, g_nStringIds16OldYposition, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdINT, g_nStringIdINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ChildPosition::scmEIWith[] = {0, 1, scmWithListDelimiter, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChildPosition::scmEIWithIndexes[] = {0, 3};
const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ChildPosition::scmEOWith[] = {0, 2, 1, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChildPosition::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ChildPosition::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ChildPosition::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  4, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ChildPosition::FORTE_Q_ChildPosition(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_s16OldXposition(var_s16OldXposition),
    var_conn_s16OldYposition(var_s16OldYposition),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u16ObjIdParent(nullptr),
    conn_s16Xposition(nullptr),
    conn_s16Yposition(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_s16OldXposition(this, 1, &var_conn_s16OldXposition),
    conn_s16OldYposition(this, 2, &var_conn_s16OldYposition),
    conn_s16result(this, 3, &var_conn_s16result) {
};

void FORTE_Q_ChildPosition::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u16ObjIdParent = 0_UINT;
  var_s16Xposition = 0_INT;
  var_s16Yposition = 0_INT;
  var_STATUS = ""_STRING;
  var_s16OldXposition = 0_INT;
  var_s16OldYposition = 0_INT;
  var_s16result = 0_INT;
}

void FORTE_Q_ChildPosition::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendChildPosition();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ChildPosition::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      readData(1, var_u16ObjIdParent, conn_u16ObjIdParent);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(2, var_s16Xposition, conn_s16Xposition);
      readData(3, var_s16Yposition, conn_s16Yposition);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ChildPosition::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(2, var_s16OldYposition, conn_s16OldYposition);
      writeData(1, var_s16OldXposition, conn_s16OldXposition);
      writeData(3, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ChildPosition::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u16ObjIdParent;
    case 2: return &var_s16Xposition;
    case 3: return &var_s16Yposition;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChildPosition::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_s16OldXposition;
    case 2: return &var_s16OldYposition;
    case 3: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChildPosition::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ChildPosition::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ChildPosition::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u16ObjIdParent;
    case 2: return &conn_s16Xposition;
    case 3: return &conn_s16Yposition;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ChildPosition::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_s16OldXposition;
    case 2: return &conn_s16OldYposition;
    case 3: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ChildPosition::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ChildPosition::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ChildPosition::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  u16ObjIdParent = static_cast<iso_u16>(var_u16ObjIdParent);
  // Set the "old Value" to invalid.
  var_s16OldXposition = static_cast<CIEC_INT>(0xFFFF);
  var_s16OldYposition = static_cast<CIEC_INT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ChildPosition::sendChildPosition(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_s16OldXposition, var_s16Xposition) //
      || func_NE(var_s16OldYposition, var_s16Yposition)) {
        ret = IsoVtcCmd_ChildPosition(u8VtcInstance, u16ObjId, u16ObjIdParent, static_cast<iso_s16>(var_s16Xposition), static_cast<iso_s16>(var_s16Yposition));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_s16OldXposition = var_s16Xposition; //store this value.
            var_s16OldYposition = var_s16Yposition; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

