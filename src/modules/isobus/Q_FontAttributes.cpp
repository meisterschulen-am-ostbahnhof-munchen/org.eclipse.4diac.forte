/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_FontAttributes
 *** Description: Command change font attributes ( Part 6 - F.28 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_FontAttributes.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_FontAttributes_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_FontAttributes, g_nStringIdQ_FontAttributes)

const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8FontColour, g_nStringIdu8FontSize, g_nStringIdu8FontType, g_nStringIdu8FontStyle};
const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldFontColour, g_nStringIdu8OldFontSize, g_nStringIdu8OldFontType, g_nStringIdu8OldFontStyle, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_FontAttributes::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, 4, scmWithListDelimiter};
const TForteInt16 FORTE_Q_FontAttributes::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_FontAttributes::scmEOWith[] = {0, 3, 1, 2, 5, 4, scmWithListDelimiter};
const TForteInt16 FORTE_Q_FontAttributes::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_FontAttributes::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_FontAttributes::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  5, scmDataInputNames, scmDataInputTypeIds,
  6, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_FontAttributes::FORTE_Q_FontAttributes(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldFontColour(var_u8OldFontColour),
    var_conn_u8OldFontSize(var_u8OldFontSize),
    var_conn_u8OldFontType(var_u8OldFontType),
    var_conn_u8OldFontStyle(var_u8OldFontStyle),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8FontColour(nullptr),
    conn_u8FontSize(nullptr),
    conn_u8FontType(nullptr),
    conn_u8FontStyle(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldFontColour(this, 1, &var_conn_u8OldFontColour),
    conn_u8OldFontSize(this, 2, &var_conn_u8OldFontSize),
    conn_u8OldFontType(this, 3, &var_conn_u8OldFontType),
    conn_u8OldFontStyle(this, 4, &var_conn_u8OldFontStyle),
    conn_s16result(this, 5, &var_conn_s16result) {
};

void FORTE_Q_FontAttributes::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8FontColour = 0_USINT;
  var_u8FontSize = 0_USINT;
  var_u8FontType = 0_USINT;
  var_u8FontStyle = 0_USINT;
  var_STATUS = ""_STRING;
  var_u8OldFontColour = 0_USINT;
  var_u8OldFontSize = 0_USINT;
  var_u8OldFontType = 0_USINT;
  var_u8OldFontStyle = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_FontAttributes::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendFontAttributes();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_FontAttributes::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8FontColour, conn_u8FontColour);
      readData(2, var_u8FontSize, conn_u8FontSize);
      readData(3, var_u8FontType, conn_u8FontType);
      readData(4, var_u8FontStyle, conn_u8FontStyle);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_FontAttributes::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_u8OldFontType, conn_u8OldFontType);
      writeData(1, var_u8OldFontColour, conn_u8OldFontColour);
      writeData(2, var_u8OldFontSize, conn_u8OldFontSize);
      writeData(5, var_s16result, conn_s16result);
      writeData(4, var_u8OldFontStyle, conn_u8OldFontStyle);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_FontAttributes::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8FontColour;
    case 2: return &var_u8FontSize;
    case 3: return &var_u8FontType;
    case 4: return &var_u8FontStyle;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_FontAttributes::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldFontColour;
    case 2: return &var_u8OldFontSize;
    case 3: return &var_u8OldFontType;
    case 4: return &var_u8OldFontStyle;
    case 5: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_FontAttributes::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_FontAttributes::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_FontAttributes::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8FontColour;
    case 2: return &conn_u8FontSize;
    case 3: return &conn_u8FontType;
    case 4: return &conn_u8FontStyle;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_FontAttributes::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldFontColour;
    case 2: return &conn_u8OldFontSize;
    case 3: return &conn_u8OldFontType;
    case 4: return &conn_u8OldFontStyle;
    case 5: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_FontAttributes::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_FontAttributes::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_FontAttributes::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldFontColour = static_cast<CIEC_USINT>(0xFF);
  var_u8OldFontSize = static_cast<CIEC_USINT>(0xFF);
  var_u8OldFontType = static_cast<CIEC_USINT>(0xFF);
  var_u8OldFontStyle = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_FontAttributes::sendFontAttributes(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldFontColour, var_u8FontColour) //
      || func_NE(var_u8OldFontSize, var_u8FontSize) //
      || func_NE(var_u8OldFontType, var_u8FontType) //
      || func_NE(var_u8OldFontStyle, var_u8FontStyle)) {
        ret = IsoVtcCmd_FontAttributes(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8FontColour), static_cast<iso_u8>(var_u8FontSize), static_cast<iso_u8>(var_u8FontType), static_cast<iso_u8>(var_u8FontStyle));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldFontColour = var_u8FontColour; //store this value.
            var_u8OldFontSize = var_u8FontSize; //store this value.
            var_u8OldFontType = var_u8FontType; //store this value.
            var_u8OldFontStyle = var_u8FontStyle; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


