/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ObjSelectInput
 *** Description: Command Select input object ( Part 6 - F.6 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_ObjSelectInput final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ObjSelectInput)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendObjSelectInput(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ObjSelectInput(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_USINT var_u8Option;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldOption;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldOption;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u8Option;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldOption;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8Option, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldOption, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8Option = pau8Option;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldOption = var_u8OldOption;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8Option, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldOption, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8Option = pau8Option;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldOption = var_u8OldOption;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8Option, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldOption, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau8Option, paSTATUS, pau8OldOption, pas16result);
  }
};


