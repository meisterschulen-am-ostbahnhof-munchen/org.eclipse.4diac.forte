/*************************************************************************
 *** FORTE Language Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: isobus
 *************************************************************************/

#include "isobus.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "isobus_gen.cpp"
#endif

#include "iec61131_functions.h"
#include "forte_uint.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

const CIEC_UINT st_global_ID_NULL = 65535_UINT;
