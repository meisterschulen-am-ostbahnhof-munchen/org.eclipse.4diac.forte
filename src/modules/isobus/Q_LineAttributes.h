/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_LineAttributes
 *** Description: Command change line attributes ( Part 6 - F.30 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_LineAttributes final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_LineAttributes)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendLineAttributes(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_LineAttributes(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_USINT var_u8LineColour;
  CIEC_USINT var_u8LineWidth;
  CIEC_UINT var_u16LineArt;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldLineColour;
  CIEC_USINT var_u8OldLineWidth;
  CIEC_UINT var_u16OldLineArt;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldLineColour;
  CIEC_USINT var_conn_u8OldLineWidth;
  CIEC_UINT var_conn_u16OldLineArt;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u8LineColour;
  CDataConnection *conn_u8LineWidth;
  CDataConnection *conn_u16LineArt;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldLineColour;
  CDataConnection conn_u8OldLineWidth;
  CDataConnection conn_u16OldLineArt;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8LineColour, const CIEC_USINT &pau8LineWidth, const CIEC_UINT &pau16LineArt, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldLineColour, CIEC_USINT &pau8OldLineWidth, CIEC_UINT &pau16OldLineArt, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8LineColour = pau8LineColour;
    var_u8LineWidth = pau8LineWidth;
    var_u16LineArt = pau16LineArt;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldLineColour = var_u8OldLineColour;
    pau8OldLineWidth = var_u8OldLineWidth;
    pau16OldLineArt = var_u16OldLineArt;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8LineColour, const CIEC_USINT &pau8LineWidth, const CIEC_UINT &pau16LineArt, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldLineColour, CIEC_USINT &pau8OldLineWidth, CIEC_UINT &pau16OldLineArt, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8LineColour = pau8LineColour;
    var_u8LineWidth = pau8LineWidth;
    var_u16LineArt = pau16LineArt;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldLineColour = var_u8OldLineColour;
    pau8OldLineWidth = var_u8OldLineWidth;
    pau16OldLineArt = var_u16OldLineArt;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8LineColour, const CIEC_USINT &pau8LineWidth, const CIEC_UINT &pau16LineArt, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldLineColour, CIEC_USINT &pau8OldLineWidth, CIEC_UINT &pau16OldLineArt, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau8LineColour, pau8LineWidth, pau16LineArt, paSTATUS, pau8OldLineColour, pau8OldLineWidth, pau16OldLineArt, pas16result);
  }
};


