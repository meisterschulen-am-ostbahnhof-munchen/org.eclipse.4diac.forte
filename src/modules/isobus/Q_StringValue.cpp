/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_StringValue
 *** Description: Command Change String Value ( Part 6 - F.24 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_StringValue.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_StringValue_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX


DEFINE_FIRMWARE_FB(FORTE_Q_StringValue, g_nStringIdQ_StringValue)

const CStringDictionary::TStringId FORTE_Q_StringValue::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdpau8String};
const CStringDictionary::TStringId FORTE_Q_StringValue::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdSTRING};
const CStringDictionary::TStringId FORTE_Q_StringValue::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdpau8OldString, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_StringValue::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdSTRING, g_nStringIdINT};
const TDataIOID FORTE_Q_StringValue::scmEIWith[] = {0, scmWithListDelimiter, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_StringValue::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_StringValue::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_StringValue::scmEOWith[] = {0, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_StringValue::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_StringValue::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_StringValue::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_StringValue::FORTE_Q_StringValue(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_pau8OldString(var_pau8OldString),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_pau8String(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_pau8OldString(this, 1, &var_conn_pau8OldString),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_StringValue::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_pau8String = ""_STRING;
  var_STATUS = ""_STRING;
  var_pau8OldString = ""_STRING;
  var_s16result = 0_INT;
}

void FORTE_Q_StringValue::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendStringValue();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_StringValue::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_pau8String, conn_pau8String);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_StringValue::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_pau8OldString, conn_pau8OldString);
      writeData(2, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_StringValue::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_pau8String;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_StringValue::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_pau8OldString;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_StringValue::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_StringValue::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_StringValue::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_pau8String;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_StringValue::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_pau8OldString;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_StringValue::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_StringValue::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

void FORTE_Q_StringValue::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_pau8OldString = static_cast<CIEC_STRING>("");
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_StringValue::sendStringValue(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_pau8OldString, var_pau8String)) {
        const unsigned char *pau8String = (const unsigned char*) var_pau8String.getValue();
        ret = IsoVtcCmd_StringRef(u8VtcInstance, u16ObjId, pau8String);
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_pau8OldString = var_pau8String; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}
