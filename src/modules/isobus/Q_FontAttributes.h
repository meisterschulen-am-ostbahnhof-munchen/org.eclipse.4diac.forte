/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_FontAttributes
 *** Description: Command change font attributes ( Part 6 - F.28 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_FontAttributes final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_FontAttributes)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendFontAttributes(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_FontAttributes(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_USINT var_u8FontColour;
  CIEC_USINT var_u8FontSize;
  CIEC_USINT var_u8FontType;
  CIEC_USINT var_u8FontStyle;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldFontColour;
  CIEC_USINT var_u8OldFontSize;
  CIEC_USINT var_u8OldFontType;
  CIEC_USINT var_u8OldFontStyle;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldFontColour;
  CIEC_USINT var_conn_u8OldFontSize;
  CIEC_USINT var_conn_u8OldFontType;
  CIEC_USINT var_conn_u8OldFontStyle;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u8FontColour;
  CDataConnection *conn_u8FontSize;
  CDataConnection *conn_u8FontType;
  CDataConnection *conn_u8FontStyle;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldFontColour;
  CDataConnection conn_u8OldFontSize;
  CDataConnection conn_u8OldFontType;
  CDataConnection conn_u8OldFontStyle;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8FontColour, const CIEC_USINT &pau8FontSize, const CIEC_USINT &pau8FontType, const CIEC_USINT &pau8FontStyle, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldFontColour, CIEC_USINT &pau8OldFontSize, CIEC_USINT &pau8OldFontType, CIEC_USINT &pau8OldFontStyle, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8FontColour = pau8FontColour;
    var_u8FontSize = pau8FontSize;
    var_u8FontType = pau8FontType;
    var_u8FontStyle = pau8FontStyle;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldFontColour = var_u8OldFontColour;
    pau8OldFontSize = var_u8OldFontSize;
    pau8OldFontType = var_u8OldFontType;
    pau8OldFontStyle = var_u8OldFontStyle;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8FontColour, const CIEC_USINT &pau8FontSize, const CIEC_USINT &pau8FontType, const CIEC_USINT &pau8FontStyle, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldFontColour, CIEC_USINT &pau8OldFontSize, CIEC_USINT &pau8OldFontType, CIEC_USINT &pau8OldFontStyle, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u8FontColour = pau8FontColour;
    var_u8FontSize = pau8FontSize;
    var_u8FontType = pau8FontType;
    var_u8FontStyle = pau8FontStyle;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldFontColour = var_u8OldFontColour;
    pau8OldFontSize = var_u8OldFontSize;
    pau8OldFontType = var_u8OldFontType;
    pau8OldFontStyle = var_u8OldFontStyle;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_USINT &pau8FontColour, const CIEC_USINT &pau8FontSize, const CIEC_USINT &pau8FontType, const CIEC_USINT &pau8FontStyle, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldFontColour, CIEC_USINT &pau8OldFontSize, CIEC_USINT &pau8OldFontType, CIEC_USINT &pau8OldFontStyle, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau8FontColour, pau8FontSize, pau8FontType, pau8FontStyle, paSTATUS, pau8OldFontColour, pau8OldFontSize, pau8OldFontType, pau8OldFontStyle, pas16result);
  }
};


