/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChangeObjectLabel
 *** Description: Command Change Object Label( Part 6 - F.50 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ChangeObjectLabel.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ChangeObjectLabel_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX


DEFINE_FIRMWARE_FB(FORTE_Q_ChangeObjectLabel, g_nStringIdQ_ChangeObjectLabel)

const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu16ObIdStringVar, g_nStringIdu8FontType, g_nStringIdu16ObIdGrafic};
const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu16OldObIdStringVar, g_nStringIdu8OldFontType, g_nStringIdu16OldObIdGrafic, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ChangeObjectLabel::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChangeObjectLabel::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ChangeObjectLabel::scmEOWith[] = {0, 1, 2, 3, 4, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ChangeObjectLabel::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ChangeObjectLabel::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ChangeObjectLabel::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ChangeObjectLabel::FORTE_Q_ChangeObjectLabel(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u16OldObIdStringVar(var_u16OldObIdStringVar),
    var_conn_u8OldFontType(var_u8OldFontType),
    var_conn_u16OldObIdGrafic(var_u16OldObIdGrafic),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u16ObIdStringVar(nullptr),
    conn_u8FontType(nullptr),
    conn_u16ObIdGrafic(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u16OldObIdStringVar(this, 1, &var_conn_u16OldObIdStringVar),
    conn_u8OldFontType(this, 2, &var_conn_u8OldFontType),
    conn_u16OldObIdGrafic(this, 3, &var_conn_u16OldObIdGrafic),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_ChangeObjectLabel::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u16ObIdStringVar = 0_UINT;
  var_u8FontType = 0_USINT;
  var_u16ObIdGrafic = 0_UINT;
  var_STATUS = ""_STRING;
  var_u16OldObIdStringVar = 0_UINT;
  var_u8OldFontType = 0_USINT;
  var_u16OldObIdGrafic = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ChangeObjectLabel::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendChangeObjectLabel();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ChangeObjectLabel::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u16ObIdStringVar, conn_u16ObIdStringVar);
      readData(2, var_u8FontType, conn_u8FontType);
      readData(3, var_u16ObIdGrafic, conn_u16ObIdGrafic);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ChangeObjectLabel::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_u16OldObIdStringVar, conn_u16OldObIdStringVar);
      writeData(2, var_u8OldFontType, conn_u8OldFontType);
      writeData(3, var_u16OldObIdGrafic, conn_u16OldObIdGrafic);
      writeData(4, var_s16result, conn_s16result);
      writeData(1, var_u16OldObIdStringVar, conn_u16OldObIdStringVar);
      writeData(2, var_u8OldFontType, conn_u8OldFontType);
      writeData(3, var_u16OldObIdGrafic, conn_u16OldObIdGrafic);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ChangeObjectLabel::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u16ObIdStringVar;
    case 2: return &var_u8FontType;
    case 3: return &var_u16ObIdGrafic;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChangeObjectLabel::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u16OldObIdStringVar;
    case 2: return &var_u8OldFontType;
    case 3: return &var_u16OldObIdGrafic;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ChangeObjectLabel::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ChangeObjectLabel::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ChangeObjectLabel::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u16ObIdStringVar;
    case 2: return &conn_u8FontType;
    case 3: return &conn_u16ObIdGrafic;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ChangeObjectLabel::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u16OldObIdStringVar;
    case 2: return &conn_u8OldFontType;
    case 3: return &conn_u16OldObIdGrafic;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ChangeObjectLabel::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ChangeObjectLabel::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ChangeObjectLabel::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u16OldObIdGrafic = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldObIdStringVar = static_cast<CIEC_UINT>(0xFFFF);
  var_u8OldFontType = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ChangeObjectLabel::sendChangeObjectLabel(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u16OldObIdGrafic, var_u16ObIdGrafic) //
      || func_NE(var_u16OldObIdStringVar, var_u16ObIdStringVar) //
        || func_NE(var_u8OldFontType, var_u8FontType)) {
        ret = IsoVtcCmd_ChangeObjectLabel(u8VtcInstance, u16ObjId, static_cast<iso_u16>(var_u16ObIdStringVar), static_cast<iso_u8>(var_u8FontType), static_cast<iso_u16>(var_u16ObIdGrafic));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u16OldObIdStringVar = var_u16ObIdStringVar; //store this value.
            var_u8OldFontType = var_u8FontType; //store this value.
            var_u16OldObIdGrafic = var_u16ObIdGrafic; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


