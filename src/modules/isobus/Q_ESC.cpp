/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ESC
 *** Description: Esc Command
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ESC.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ESC_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_ESC, g_nStringIdQ_ESC)

const CStringDictionary::TStringId FORTE_Q_ESC::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ESC::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdINT};
const TForteInt16 FORTE_Q_ESC::scmEIWithIndexes[] = {-1, -1};
const CStringDictionary::TStringId FORTE_Q_ESC::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ESC::scmEOWith[] = {0, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ESC::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ESC::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ESC::scmFBInterfaceSpec = {
  2, scmEventInputNames, nullptr, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  0, nullptr, nullptr,
  2, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ESC::FORTE_Q_ESC(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_s16result(this, 1, &var_conn_s16result) {
};

void FORTE_Q_ESC::setInitialValues() {
  var_STATUS = ""_STRING;
  var_s16result = 0_INT;
}

void FORTE_Q_ESC::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendESC();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ESC::readInputData(TEventID) {
  // nothing to do
}

void FORTE_Q_ESC::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ESC::getDI(size_t) {
  return nullptr;
}

CIEC_ANY *FORTE_Q_ESC::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ESC::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ESC::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ESC::getDIConUnchecked(TPortId) {
  return nullptr;
}

CDataConnection *FORTE_Q_ESC::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ESC::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ESC::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ESC::iso_init(void) {
    var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ESC::sendESC(void) {
    iso_s16 ret = E_UNKNOWN_ERR;
    iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
    s16CfHandleMaster = AppGetCfHandleMaster();
    if (s16CfHandleMaster != HANDLE_UNVALID) {
        iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
        u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
        if (u8VtcInstance != ISO_INSTANCE_INVALID) {
            ret = IsoVtcCmd_ESC(u8VtcInstance);
            switch (ret) {
            case E_NO_ERR: //- OK
                var_STATUS = scmOK;
                break;
            case E_OVERFLOW: //- buffer overflow
                var_STATUS = scmE_OVERFLOW;
                break;
            case E_NOACT: //- Command not possible in current state
                var_STATUS = scmE_NOACT;
                break;
            case E_NO_INSTANCE: //- No VT client available
                var_STATUS = scmE_NO_INSTANCE;
                break;
            default:
                var_STATUS = scmERR; //- Unknown Error
                break;
            }
        }
        else {
            var_STATUS = VtcInstanceunvalid;
        }
    }
    else {
        var_STATUS = CfHandleMasterunvalid;
    }
    var_s16result = static_cast<CIEC_INT>(ret);
}


