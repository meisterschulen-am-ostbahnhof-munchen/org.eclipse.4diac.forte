/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ListItem
 *** Description: Command change list item ( Part 6 - F.42 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ListItem.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ListItem_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_ListItem, g_nStringIdQ_ListItem)

const CStringDictionary::TStringId FORTE_Q_ListItem::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8ListIndex, g_nStringIdu16NewObjId};
const CStringDictionary::TStringId FORTE_Q_ListItem::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_ListItem::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldListIndex, g_nStringIdu16OldObjId, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ListItem::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ListItem::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ListItem::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_ListItem::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ListItem::scmEOWith[] = {0, 3, 1, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ListItem::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ListItem::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ListItem::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  3, scmDataInputNames, scmDataInputTypeIds,
  4, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ListItem::FORTE_Q_ListItem(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldListIndex(var_u8OldListIndex),
    var_conn_u16OldObjId(var_u16OldObjId),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8ListIndex(nullptr),
    conn_u16NewObjId(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldListIndex(this, 1, &var_conn_u8OldListIndex),
    conn_u16OldObjId(this, 2, &var_conn_u16OldObjId),
    conn_s16result(this, 3, &var_conn_s16result) {
};

void FORTE_Q_ListItem::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8ListIndex = 0_USINT;
  var_u16NewObjId = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldListIndex = 0_USINT;
  var_u16OldObjId = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ListItem::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
        iso_init();
        sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
        sendListItem();
        sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ListItem::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8ListIndex, conn_u8ListIndex);
      readData(2, var_u16NewObjId, conn_u16NewObjId);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ListItem::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_s16result, conn_s16result);
      writeData(1, var_u8OldListIndex, conn_u8OldListIndex);
      writeData(2, var_u16OldObjId, conn_u16OldObjId);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ListItem::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8ListIndex;
    case 2: return &var_u16NewObjId;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ListItem::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldListIndex;
    case 2: return &var_u16OldObjId;
    case 3: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ListItem::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ListItem::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ListItem::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8ListIndex;
    case 2: return &conn_u16NewObjId;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ListItem::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldListIndex;
    case 2: return &conn_u16OldObjId;
    case 3: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ListItem::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ListItem::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ListItem::iso_init(void) {
    // Store the ObjectID, because we want 1 ObjID per Instance
    u16ObjId = static_cast<iso_u16>(var_u16ObjId);
    // Set the "old Value" to invalid.
    var_u8OldListIndex = static_cast<CIEC_USINT>(0xFF);
    var_u16OldObjId = static_cast<CIEC_UINT>(0xFFFF);
    var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ListItem::sendListItem(void) {
    iso_s16 ret = E_UNKNOWN_ERR;
    iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
    s16CfHandleMaster = AppGetCfHandleMaster();
    if (s16CfHandleMaster != HANDLE_UNVALID) {
        iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
        u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
        if (u8VtcInstance != ISO_INSTANCE_INVALID) {
            ret = IsoVtcCmd_ListItem(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8ListIndex), static_cast<iso_u16>(var_u16NewObjId));
            switch (ret) {
            case E_NO_ERR: //- OK
                var_STATUS = scmOK;
                var_u8OldListIndex = var_u8ListIndex; //store this Value.
                var_u16OldObjId = var_u16NewObjId; //store this Value.
                break;
            case E_OVERFLOW: //- buffer overflow
                var_STATUS = scmE_OVERFLOW;
                break;
            case E_NOACT: //- Command not possible in current state
                var_STATUS = scmE_NOACT;
                break;
            case E_NO_INSTANCE: //- No VT client available
                var_STATUS = scmE_NO_INSTANCE;
                break;
            default:
                var_STATUS = scmERR; //- Unknown Error
                break;
            }
        }
        else {
            var_STATUS = VtcInstanceunvalid;
        }
    }
    else {
        var_STATUS = CfHandleMasterunvalid;
    }
    var_s16result = static_cast<CIEC_INT>(ret);
}

