/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChildLocation
 *** Description: Command change child location ( Part 6 - F.14 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_ChildLocation final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ChildLocation)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendChildLoation(void);

  iso_u16 u16ObjId;
  iso_u16 u16ObjIdParent;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ChildLocation(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_UINT var_u16ObjIdParent;
  CIEC_USINT var_u8Xchange;
  CIEC_USINT var_u8Ychange;

  CIEC_STRING var_STATUS;
  CIEC_USINT var_u8OldXchange;
  CIEC_USINT var_u8OldYchange;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_USINT var_conn_u8OldXchange;
  CIEC_USINT var_conn_u8OldYchange;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u16ObjIdParent;
  CDataConnection *conn_u8Xchange;
  CDataConnection *conn_u8Ychange;

  CDataConnection conn_STATUS;
  CDataConnection conn_u8OldXchange;
  CDataConnection conn_u8OldYchange;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_USINT &pau8Xchange, const CIEC_USINT &pau8Ychange, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldXchange, CIEC_USINT &pau8OldYchange, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObjIdParent = pau16ObjIdParent;
    var_u8Xchange = pau8Xchange;
    var_u8Ychange = pau8Ychange;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldXchange = var_u8OldXchange;
    pau8OldYchange = var_u8OldYchange;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_USINT &pau8Xchange, const CIEC_USINT &pau8Ychange, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldXchange, CIEC_USINT &pau8OldYchange, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObjIdParent = pau16ObjIdParent;
    var_u8Xchange = pau8Xchange;
    var_u8Ychange = pau8Ychange;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau8OldXchange = var_u8OldXchange;
    pau8OldYchange = var_u8OldYchange;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_USINT &pau8Xchange, const CIEC_USINT &pau8Ychange, CIEC_STRING &paSTATUS, CIEC_USINT &pau8OldXchange, CIEC_USINT &pau8OldYchange, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau16ObjIdParent, pau8Xchange, pau8Ychange, paSTATUS, pau8OldXchange, pau8OldYchange, pas16result);
  }
};


