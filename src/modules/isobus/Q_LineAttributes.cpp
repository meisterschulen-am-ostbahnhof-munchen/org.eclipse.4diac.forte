/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_LineAttributes
 *** Description: Command change line attributes ( Part 6 - F.30 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_LineAttributes.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_LineAttributes_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_LineAttributes, g_nStringIdQ_LineAttributes)

const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8LineColour, g_nStringIdu8LineWidth, g_nStringIdu16LineArt};
const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldLineColour, g_nStringIdu8OldLineWidth, g_nStringIdu16OldLineArt, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_LineAttributes::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_LineAttributes::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_LineAttributes::scmEOWith[] = {0, 4, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_LineAttributes::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_LineAttributes::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_LineAttributes::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_LineAttributes::FORTE_Q_LineAttributes(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldLineColour(var_u8OldLineColour),
    var_conn_u8OldLineWidth(var_u8OldLineWidth),
    var_conn_u16OldLineArt(var_u16OldLineArt),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8LineColour(nullptr),
    conn_u8LineWidth(nullptr),
    conn_u16LineArt(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldLineColour(this, 1, &var_conn_u8OldLineColour),
    conn_u8OldLineWidth(this, 2, &var_conn_u8OldLineWidth),
    conn_u16OldLineArt(this, 3, &var_conn_u16OldLineArt),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_LineAttributes::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8LineColour = 0_USINT;
  var_u8LineWidth = 0_USINT;
  var_u16LineArt = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldLineColour = 0_USINT;
  var_u8OldLineWidth = 0_USINT;
  var_u16OldLineArt = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_LineAttributes::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendLineAttributes();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_LineAttributes::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8LineColour, conn_u8LineColour);
      readData(2, var_u8LineWidth, conn_u8LineWidth);
      readData(3, var_u16LineArt, conn_u16LineArt);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_LineAttributes::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(4, var_s16result, conn_s16result);
      writeData(1, var_u8OldLineColour, conn_u8OldLineColour);
      writeData(2, var_u8OldLineWidth, conn_u8OldLineWidth);
      writeData(3, var_u16OldLineArt, conn_u16OldLineArt);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_LineAttributes::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8LineColour;
    case 2: return &var_u8LineWidth;
    case 3: return &var_u16LineArt;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_LineAttributes::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldLineColour;
    case 2: return &var_u8OldLineWidth;
    case 3: return &var_u16OldLineArt;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_LineAttributes::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_LineAttributes::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_LineAttributes::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8LineColour;
    case 2: return &conn_u8LineWidth;
    case 3: return &conn_u16LineArt;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_LineAttributes::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldLineColour;
    case 2: return &conn_u8OldLineWidth;
    case 3: return &conn_u16OldLineArt;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_LineAttributes::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_LineAttributes::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_LineAttributes::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldLineColour = static_cast<CIEC_USINT>(0xFF);
  var_u8OldLineWidth = static_cast<CIEC_USINT>(0xFF);
  var_u16OldLineArt = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_LineAttributes::sendLineAttributes(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldLineColour, var_u8LineColour) //
      || func_NE(var_u8OldLineWidth, var_u8LineWidth) //
        || func_NE(var_u16OldLineArt, var_u16LineArt)) {
        ret = IsoVtcCmd_LineAttributes(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8LineColour), static_cast<iso_u8>(var_u8LineWidth), static_cast<iso_u16>(var_u16LineArt));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldLineColour = var_u8LineColour; //store this value.
            var_u8OldLineWidth = var_u8LineWidth; //store this value.
            var_u16OldLineArt = var_u16LineArt; //store this value
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


