/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChangeObjectLabel
 *** Description: Command Change Object Label( Part 6 - F.50 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_usint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_ChangeObjectLabel final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ChangeObjectLabel)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendChangeObjectLabel(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ChangeObjectLabel(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_UINT var_u16ObIdStringVar;
  CIEC_USINT var_u8FontType;
  CIEC_UINT var_u16ObIdGrafic;

  CIEC_STRING var_STATUS;
  CIEC_UINT var_u16OldObIdStringVar;
  CIEC_USINT var_u8OldFontType;
  CIEC_UINT var_u16OldObIdGrafic;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_UINT var_conn_u16OldObIdStringVar;
  CIEC_USINT var_conn_u8OldFontType;
  CIEC_UINT var_conn_u16OldObIdGrafic;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u16ObIdStringVar;
  CDataConnection *conn_u8FontType;
  CDataConnection *conn_u16ObIdGrafic;

  CDataConnection conn_STATUS;
  CDataConnection conn_u16OldObIdStringVar;
  CDataConnection conn_u8OldFontType;
  CDataConnection conn_u16OldObIdGrafic;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObIdStringVar, const CIEC_USINT &pau8FontType, const CIEC_UINT &pau16ObIdGrafic, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldObIdStringVar, CIEC_USINT &pau8OldFontType, CIEC_UINT &pau16OldObIdGrafic, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObIdStringVar = pau16ObIdStringVar;
    var_u8FontType = pau8FontType;
    var_u16ObIdGrafic = pau16ObIdGrafic;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldObIdStringVar = var_u16OldObIdStringVar;
    pau8OldFontType = var_u8OldFontType;
    pau16OldObIdGrafic = var_u16OldObIdGrafic;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObIdStringVar, const CIEC_USINT &pau8FontType, const CIEC_UINT &pau16ObIdGrafic, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldObIdStringVar, CIEC_USINT &pau8OldFontType, CIEC_UINT &pau16OldObIdGrafic, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObIdStringVar = pau16ObIdStringVar;
    var_u8FontType = pau8FontType;
    var_u16ObIdGrafic = pau16ObIdGrafic;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldObIdStringVar = var_u16OldObIdStringVar;
    pau8OldFontType = var_u8OldFontType;
    pau16OldObIdGrafic = var_u16OldObIdGrafic;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObIdStringVar, const CIEC_USINT &pau8FontType, const CIEC_UINT &pau16ObIdGrafic, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldObIdStringVar, CIEC_USINT &pau8OldFontType, CIEC_UINT &pau16OldObIdGrafic, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau16ObIdStringVar, pau8FontType, pau16ObIdGrafic, paSTATUS, pau16OldObIdStringVar, pau8OldFontType, pau16OldObIdGrafic, pas16result);
  }
};


