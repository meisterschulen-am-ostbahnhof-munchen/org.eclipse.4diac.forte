/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ObjSelectInput
 *** Description: Command Select input object ( Part 6 - F.6 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ObjSelectInput.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ObjSelectInput_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_ObjSelectInput, g_nStringIdQ_ObjSelectInput)

const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8Option};
const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldOption, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ObjSelectInput::scmEIWith[] = {0, scmWithListDelimiter, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ObjSelectInput::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ObjSelectInput::scmEOWith[] = {0, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ObjSelectInput::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ObjSelectInput::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ObjSelectInput::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ObjSelectInput::FORTE_Q_ObjSelectInput(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldOption(var_u8OldOption),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8Option(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldOption(this, 1, &var_conn_u8OldOption),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_ObjSelectInput::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8Option = 0_USINT;
  var_STATUS = ""_STRING;
  var_u8OldOption = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ObjSelectInput::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendObjSelectInput();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ObjSelectInput::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8Option, conn_u8Option);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ObjSelectInput::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(2, var_s16result, conn_s16result);
      writeData(1, var_u8OldOption, conn_u8OldOption);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ObjSelectInput::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8Option;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ObjSelectInput::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldOption;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ObjSelectInput::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ObjSelectInput::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ObjSelectInput::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8Option;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ObjSelectInput::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldOption;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ObjSelectInput::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ObjSelectInput::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ObjSelectInput::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldOption = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ObjSelectInput::sendObjSelectInput(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      ret = IsoVtcCmd_ObjSelectInput(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8Option));
      switch(ret){
        case E_NO_ERR: //- OK
          var_STATUS = scmOK;
          var_u8OldOption = var_u8Option; //store this value.
          break;
        case E_OVERFLOW: //- buffer overflow
          var_STATUS = scmE_OVERFLOW;
          break;
        case E_NOACT: //- Command not possible in current state
          var_STATUS = scmE_NOACT;
          break;
        case E_NO_INSTANCE: //- No VT client available
          var_STATUS = scmE_NO_INSTANCE;
          break;
        default:
          var_STATUS = scmERR; //- Unknown Error
          break;
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


