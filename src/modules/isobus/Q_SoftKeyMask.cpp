/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SoftKeyMask
 *** Description: Command change soft key mask( Part 6 - F.36 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_SoftKeyMask.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_SoftKeyMask_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_SoftKeyMask, g_nStringIdQ_SoftKeyMask)

const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmDataInputNames[] = {g_nStringIdu8MaskType, g_nStringIdu16DataMaskId, g_nStringIdu16SoftKeyMaskId};
const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmDataInputTypeIds[] = {g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldMaskType, g_nStringIdu16OldDataMaskId, g_nStringIdu16OldSoftKeyMaskId, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_SoftKeyMask::scmEIWith[] = {1, 2, 0, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SoftKeyMask::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_SoftKeyMask::scmEOWith[] = {0, 4, 2, 3, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SoftKeyMask::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SoftKeyMask::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_SoftKeyMask::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  3, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_SoftKeyMask::FORTE_Q_SoftKeyMask(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldMaskType(var_u8OldMaskType),
    var_conn_u16OldDataMaskId(var_u16OldDataMaskId),
    var_conn_u16OldSoftKeyMaskId(var_u16OldSoftKeyMaskId),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u8MaskType(nullptr),
    conn_u16DataMaskId(nullptr),
    conn_u16SoftKeyMaskId(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldMaskType(this, 1, &var_conn_u8OldMaskType),
    conn_u16OldDataMaskId(this, 2, &var_conn_u16OldDataMaskId),
    conn_u16OldSoftKeyMaskId(this, 3, &var_conn_u16OldSoftKeyMaskId),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_SoftKeyMask::setInitialValues() {
  var_u8MaskType = 0_USINT;
  var_u16DataMaskId = 0_UINT;
  var_u16SoftKeyMaskId = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldMaskType = 0_USINT;
  var_u16OldDataMaskId = 0_UINT;
  var_u16OldSoftKeyMaskId = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_SoftKeyMask::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendSoftKeyMask();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_SoftKeyMask::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u16DataMaskId, conn_u16DataMaskId);
      readData(2, var_u16SoftKeyMaskId, conn_u16SoftKeyMaskId);
      readData(0, var_u8MaskType, conn_u8MaskType);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_SoftKeyMask::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(4, var_s16result, conn_s16result);
      writeData(2, var_u16OldDataMaskId, conn_u16OldDataMaskId);
      writeData(3, var_u16OldSoftKeyMaskId, conn_u16OldSoftKeyMaskId);
      writeData(1, var_u8OldMaskType, conn_u8OldMaskType);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_SoftKeyMask::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u8MaskType;
    case 1: return &var_u16DataMaskId;
    case 2: return &var_u16SoftKeyMaskId;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SoftKeyMask::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldMaskType;
    case 2: return &var_u16OldDataMaskId;
    case 3: return &var_u16OldSoftKeyMaskId;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SoftKeyMask::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_SoftKeyMask::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_SoftKeyMask::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u8MaskType;
    case 1: return &conn_u16DataMaskId;
    case 2: return &conn_u16SoftKeyMaskId;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_SoftKeyMask::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldMaskType;
    case 2: return &conn_u16OldDataMaskId;
    case 3: return &conn_u16OldSoftKeyMaskId;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_SoftKeyMask::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_SoftKeyMask::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}

void FORTE_Q_SoftKeyMask::iso_init(void) {
  // Set the "old Value" to invalid.
  var_u8OldMaskType = static_cast<CIEC_USINT>(0xFF);
  var_u16OldDataMaskId = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldSoftKeyMaskId = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_SoftKeyMask::sendSoftKeyMask(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      ret = IsoVtcCmd_SoftKeyMask(u8VtcInstance, static_cast<iso_u8>(var_u8MaskType), static_cast<iso_u16>(var_u16DataMaskId), static_cast<iso_u16>(var_u16SoftKeyMaskId));
      switch(ret){
        case E_NO_ERR: //- OK
          var_STATUS = scmOK;
          var_u8OldMaskType = var_u8MaskType; //store this value.
          var_u16OldDataMaskId = var_u16DataMaskId; //store this value.
          var_u16OldSoftKeyMaskId = var_u16SoftKeyMaskId; //store this value.
          break;
        case E_OVERFLOW: //- buffer overflow
          var_STATUS = scmE_OVERFLOW;
          break;
        case E_NOACT: //- Command not possible in current state
          var_STATUS = scmE_NOACT;
          break;
        case E_NO_INSTANCE: //- No VT client available
          var_STATUS = scmE_NO_INSTANCE;
          break;
        default:
          var_STATUS = scmERR; //- Unknown Error
          break;
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}



