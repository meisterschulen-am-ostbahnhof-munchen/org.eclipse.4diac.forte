/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_FillAttributes
 *** Description: Command change fill attributes ( Part 6 - F.32 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_FillAttributes.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_FillAttributes_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_FillAttributes, g_nStringIdQ_FillAttributes)

const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8FillType, g_nStringIdu8FillColour, g_nStringIdu16FillPatternId};
const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldFillType, g_nStringIdu8OldFillColour, g_nStringIdu16OldFillPatternId, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_FillAttributes::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_FillAttributes::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_FillAttributes::scmEOWith[] = {0, 3, 1, 2, 4, scmWithListDelimiter};
const TForteInt16 FORTE_Q_FillAttributes::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_FillAttributes::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_FillAttributes::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_FillAttributes::FORTE_Q_FillAttributes(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldFillType(var_u8OldFillType),
    var_conn_u8OldFillColour(var_u8OldFillColour),
    var_conn_u16OldFillPatternId(var_u16OldFillPatternId),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8FillType(nullptr),
    conn_u8FillColour(nullptr),
    conn_u16FillPatternId(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldFillType(this, 1, &var_conn_u8OldFillType),
    conn_u8OldFillColour(this, 2, &var_conn_u8OldFillColour),
    conn_u16OldFillPatternId(this, 3, &var_conn_u16OldFillPatternId),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_FillAttributes::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8FillType = 0_USINT;
  var_u8FillColour = 0_USINT;
  var_u16FillPatternId = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldFillType = 0_USINT;
  var_u8OldFillColour = 0_USINT;
  var_u16OldFillPatternId = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_FillAttributes::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendFillAttributes();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_FillAttributes::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u8FillType, conn_u8FillType);
      readData(2, var_u8FillColour, conn_u8FillColour);
      readData(3, var_u16FillPatternId, conn_u16FillPatternId);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_FillAttributes::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_u16OldFillPatternId, conn_u16OldFillPatternId);
      writeData(1, var_u8OldFillType, conn_u8OldFillType);
      writeData(2, var_u8OldFillColour, conn_u8OldFillColour);
      writeData(4, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_FillAttributes::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8FillType;
    case 2: return &var_u8FillColour;
    case 3: return &var_u16FillPatternId;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_FillAttributes::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldFillType;
    case 2: return &var_u8OldFillColour;
    case 3: return &var_u16OldFillPatternId;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_FillAttributes::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_FillAttributes::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_FillAttributes::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8FillType;
    case 2: return &conn_u8FillColour;
    case 3: return &conn_u16FillPatternId;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_FillAttributes::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldFillType;
    case 2: return &conn_u8OldFillColour;
    case 3: return &conn_u16OldFillPatternId;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_FillAttributes::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_FillAttributes::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_FillAttributes::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u8OldFillType = static_cast<CIEC_USINT>(0xFF);
  var_u8OldFillColour = static_cast<CIEC_USINT>(0xFF);
  var_u16OldFillPatternId = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_FillAttributes::sendFillAttributes(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u8OldFillType, var_u8FillType) //
      || func_NE(var_u8OldFillColour, var_u8FillColour) //
        || func_NE(var_u16OldFillPatternId, var_u16FillPatternId)) {
        ret = IsoVtcCmd_FillAttributes(u8VtcInstance, u16ObjId, static_cast<iso_u8>(var_u8FillType), static_cast<iso_u8>(var_u8FillColour),
          static_cast<iso_u16>(var_u16FillPatternId));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u8OldFillType = var_u8FillType; //store this value.
            var_u8OldFillColour = var_u8FillColour; //store this value.
            var_u16OldFillPatternId = var_u16FillPatternId; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}



