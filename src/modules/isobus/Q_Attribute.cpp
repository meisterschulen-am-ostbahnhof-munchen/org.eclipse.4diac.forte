/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_Attribute
 *** Description: Command change attribute( Part 6 - F.38 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_Attribute.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_Attribute_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_Attribute, g_nStringIdQ_Attribute)

const CStringDictionary::TStringId FORTE_Q_Attribute::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu8IdAttribute, g_nStringIdu32ValueAttribute};
const CStringDictionary::TStringId FORTE_Q_Attribute::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdUDINT};
const CStringDictionary::TStringId FORTE_Q_Attribute::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu32OldValueAttribute, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_Attribute::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUDINT, g_nStringIdINT};
const TDataIOID FORTE_Q_Attribute::scmEIWith[] = {0, 1, scmWithListDelimiter, 2, scmWithListDelimiter};
const TForteInt16 FORTE_Q_Attribute::scmEIWithIndexes[] = {0, 3};
const CStringDictionary::TStringId FORTE_Q_Attribute::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_Attribute::scmEOWith[] = {0, 1, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_Attribute::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_Attribute::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_Attribute::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  3, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_Attribute::FORTE_Q_Attribute(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u32OldValueAttribute(var_u32OldValueAttribute),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u8IdAttribute(nullptr),
    conn_u32ValueAttribute(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u32OldValueAttribute(this, 1, &var_conn_u32OldValueAttribute),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_Attribute::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u8IdAttribute = 0_USINT;
  var_u32ValueAttribute = 0_UDINT;
  var_STATUS = ""_STRING;
  var_u32OldValueAttribute = 0_UDINT;
  var_s16result = 0_INT;
}

void FORTE_Q_Attribute::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendAttribute();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_Attribute::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      readData(1, var_u8IdAttribute, conn_u8IdAttribute);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(2, var_u32ValueAttribute, conn_u32ValueAttribute);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_Attribute::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_u32OldValueAttribute, conn_u32OldValueAttribute);
      writeData(2, var_s16result, conn_s16result);
      writeData(1, var_u32OldValueAttribute, conn_u32OldValueAttribute);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_Attribute::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u8IdAttribute;
    case 2: return &var_u32ValueAttribute;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_Attribute::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u32OldValueAttribute;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_Attribute::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_Attribute::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_Attribute::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u8IdAttribute;
    case 2: return &conn_u32ValueAttribute;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_Attribute::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u32OldValueAttribute;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_Attribute::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_Attribute::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_Attribute::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  u8IdAttribute = static_cast<iso_u8>(var_u8IdAttribute);
  // Set the "old Value" to invalid.
  var_u32OldValueAttribute = static_cast<CIEC_UDINT>(0xFFFFFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_Attribute::sendAttribute(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u32OldValueAttribute, var_u32ValueAttribute)) {
        ret = IsoVtcCmd_Attribute(u8VtcInstance, u16ObjId, u8IdAttribute, static_cast<iso_u32>(var_u32ValueAttribute));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u32OldValueAttribute = var_u32ValueAttribute; //store this Value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

