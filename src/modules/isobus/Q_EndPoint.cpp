/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_EndPoint
 *** Description: Command change end point ( Part 6 - F.26 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_EndPoint.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_EndPoint_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_EndPoint, g_nStringIdQ_EndPoint)

const CStringDictionary::TStringId FORTE_Q_EndPoint::scmDataInputNames[] = {g_nStringIdu16ObjId, g_nStringIdu16Width, g_nStringIdu16Height, g_nStringIdu8LineDirection};
const CStringDictionary::TStringId FORTE_Q_EndPoint::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_EndPoint::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu16OldWidth, g_nStringIdu16OldHeight, g_nStringIdu8OldLineDirection, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_EndPoint::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_EndPoint::scmEIWith[] = {0, scmWithListDelimiter, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_EndPoint::scmEIWithIndexes[] = {0, 2};
const CStringDictionary::TStringId FORTE_Q_EndPoint::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_EndPoint::scmEOWith[] = {0, 3, 1, 2, 4, scmWithListDelimiter};
const TForteInt16 FORTE_Q_EndPoint::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_EndPoint::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_EndPoint::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  5, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_EndPoint::FORTE_Q_EndPoint(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u16OldWidth(var_u16OldWidth),
    var_conn_u16OldHeight(var_u16OldHeight),
    var_conn_u8OldLineDirection(var_u8OldLineDirection),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16ObjId(nullptr),
    conn_u16Width(nullptr),
    conn_u16Height(nullptr),
    conn_u8LineDirection(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u16OldWidth(this, 1, &var_conn_u16OldWidth),
    conn_u16OldHeight(this, 2, &var_conn_u16OldHeight),
    conn_u8OldLineDirection(this, 3, &var_conn_u8OldLineDirection),
    conn_s16result(this, 4, &var_conn_s16result) {
};

void FORTE_Q_EndPoint::setInitialValues() {
  var_u16ObjId = 0_UINT;
  var_u16Width = 0_UINT;
  var_u16Height = 0_UINT;
  var_u8LineDirection = 0_USINT;
  var_STATUS = ""_STRING;
  var_u16OldWidth = 0_UINT;
  var_u16OldHeight = 0_UINT;
  var_u8OldLineDirection = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_EndPoint::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendEndPoint();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_EndPoint::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventINITID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u16ObjId, conn_u16ObjId);
      break;
    }
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u16Width, conn_u16Width);
      readData(2, var_u16Height, conn_u16Height);
      readData(3, var_u8LineDirection, conn_u8LineDirection);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_EndPoint::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_u8OldLineDirection, conn_u8OldLineDirection);
      writeData(1, var_u16OldWidth, conn_u16OldWidth);
      writeData(2, var_u16OldHeight, conn_u16OldHeight);
      writeData(4, var_s16result, conn_s16result);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_EndPoint::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16ObjId;
    case 1: return &var_u16Width;
    case 2: return &var_u16Height;
    case 3: return &var_u8LineDirection;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_EndPoint::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u16OldWidth;
    case 2: return &var_u16OldHeight;
    case 3: return &var_u8OldLineDirection;
    case 4: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_EndPoint::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_EndPoint::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_EndPoint::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16ObjId;
    case 1: return &conn_u16Width;
    case 2: return &conn_u16Height;
    case 3: return &conn_u8LineDirection;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_EndPoint::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u16OldWidth;
    case 2: return &conn_u16OldHeight;
    case 3: return &conn_u8OldLineDirection;
    case 4: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_EndPoint::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_EndPoint::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}



void FORTE_Q_EndPoint::iso_init(void) {
  // Store the ObjectID, because we want 1 ObjID per Instance
  u16ObjId = static_cast<iso_u16>(var_u16ObjId);
  // Set the "old Value" to invalid.
  var_u16OldWidth = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldWidth = static_cast<CIEC_UINT>(0xFFFF);
  var_u8OldLineDirection = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_EndPoint::sendEndPoint(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u16OldWidth, var_u16Width) //
      || func_NE(var_u16OldWidth, var_u16Width) //
        || func_NE(var_u16OldHeight, var_u16Height) //
        || func_NE(var_u8OldLineDirection, var_u8LineDirection)) {
        ret = IsoVtcCmd_EndPoint(u8VtcInstance, u16ObjId, static_cast<iso_u16>(var_u16Width), static_cast<iso_u16>(var_u16Height),
          static_cast<iso_u8>(var_u8LineDirection));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u16OldWidth = var_u16Width; //store this value.
            var_u16OldWidth = var_u16Width; //store this value.
            var_u16OldHeight = var_u16Height; //store this value.
            var_u8OldLineDirection = var_u8LineDirection; //store this value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}



