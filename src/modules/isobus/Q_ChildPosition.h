/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChildPosition
 *** Description: Command change child position ( Part 6 - F.16 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_int.h"
#include "forte_string.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"

#include "IsoDef.h"

class FORTE_Q_ChildPosition final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ChildPosition)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendChildPosition(void);

  iso_u16 u16ObjId;
  iso_u16 u16ObjIdParent;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ChildPosition(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_UINT var_u16ObjIdParent;
  CIEC_INT var_s16Xposition;
  CIEC_INT var_s16Yposition;

  CIEC_STRING var_STATUS;
  CIEC_INT var_s16OldXposition;
  CIEC_INT var_s16OldYposition;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_INT var_conn_s16OldXposition;
  CIEC_INT var_conn_s16OldYposition;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u16ObjIdParent;
  CDataConnection *conn_s16Xposition;
  CDataConnection *conn_s16Yposition;

  CDataConnection conn_STATUS;
  CDataConnection conn_s16OldXposition;
  CDataConnection conn_s16OldYposition;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_INT &pas16Xposition, const CIEC_INT &pas16Yposition, CIEC_STRING &paSTATUS, CIEC_INT &pas16OldXposition, CIEC_INT &pas16OldYposition, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObjIdParent = pau16ObjIdParent;
    var_s16Xposition = pas16Xposition;
    var_s16Yposition = pas16Yposition;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pas16OldXposition = var_s16OldXposition;
    pas16OldYposition = var_s16OldYposition;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_INT &pas16Xposition, const CIEC_INT &pas16Yposition, CIEC_STRING &paSTATUS, CIEC_INT &pas16OldXposition, CIEC_INT &pas16OldYposition, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16ObjIdParent = pau16ObjIdParent;
    var_s16Xposition = pas16Xposition;
    var_s16Yposition = pas16Yposition;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pas16OldXposition = var_s16OldXposition;
    pas16OldYposition = var_s16OldYposition;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16ObjIdParent, const CIEC_INT &pas16Xposition, const CIEC_INT &pas16Yposition, CIEC_STRING &paSTATUS, CIEC_INT &pas16OldXposition, CIEC_INT &pas16OldYposition, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau16ObjIdParent, pas16Xposition, pas16Yposition, paSTATUS, pas16OldXposition, pas16OldYposition, pas16result);
  }
};


