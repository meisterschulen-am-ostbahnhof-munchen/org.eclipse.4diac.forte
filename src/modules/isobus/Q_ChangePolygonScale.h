/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ChangePolygonScale
 *** Description: Command Change Polygon Scale( Part 6 - F.54 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#pragma once

#include "funcbloc.h"
#include "CisobusFunctionBlock.h"
#include "forte_uint.h"
#include "forte_string.h"
#include "forte_int.h"
#include "iec61131_functions.h"
#include "forte_array_common.h"
#include "forte_array.h"
#include "forte_array_fixed.h"
#include "forte_array_variable.h"


#include "IsoDef.h"

class FORTE_Q_ChangePolygonScale final : public CisobusFunctionBlock {
  DECLARE_FIRMWARE_FB(FORTE_Q_ChangePolygonScale)

private:
  static const CStringDictionary::TStringId scmDataInputNames[];
  static const CStringDictionary::TStringId scmDataInputTypeIds[];
  static const CStringDictionary::TStringId scmDataOutputNames[];
  static const CStringDictionary::TStringId scmDataOutputTypeIds[];
  static const TEventID scmEventINITID = 0;
  static const TEventID scmEventREQID = 1;
  static const TDataIOID scmEIWith[];
  static const TForteInt16 scmEIWithIndexes[];
  static const CStringDictionary::TStringId scmEventInputNames[];
  static const TEventID scmEventINITOID = 0;
  static const TEventID scmEventCNFID = 1;
  static const TDataIOID scmEOWith[];
  static const TForteInt16 scmEOWithIndexes[];
  static const CStringDictionary::TStringId scmEventOutputNames[];

  static const SFBInterfaceSpec scmFBInterfaceSpec;

  void executeEvent(TEventID paEIID, CEventChainExecutionThread *const paECET) override;
  void iso_init(void);
  void sendChangePolygonScale(void);

  iso_u16 u16ObjId;
  
  void readInputData(TEventID paEIID) override;
  void writeOutputData(TEventID paEIID) override;
  void setInitialValues() override;

public:
  FORTE_Q_ChangePolygonScale(CStringDictionary::TStringId paInstanceNameId, CResource *paSrcRes);

  CIEC_UINT var_u16ObjId;
  CIEC_UINT var_u16NewWidth;
  CIEC_UINT var_u16NewHeight;

  CIEC_STRING var_STATUS;
  CIEC_UINT var_u16OldWidth;
  CIEC_UINT var_u16OldHeight;
  CIEC_INT var_s16result;

  CIEC_STRING var_conn_STATUS;
  CIEC_UINT var_conn_u16OldWidth;
  CIEC_UINT var_conn_u16OldHeight;
  CIEC_INT var_conn_s16result;

  CEventConnection conn_INITO;
  CEventConnection conn_CNF;

  CDataConnection *conn_u16ObjId;
  CDataConnection *conn_u16NewWidth;
  CDataConnection *conn_u16NewHeight;

  CDataConnection conn_STATUS;
  CDataConnection conn_u16OldWidth;
  CDataConnection conn_u16OldHeight;
  CDataConnection conn_s16result;

  CIEC_ANY *getDI(size_t) override;
  CIEC_ANY *getDO(size_t) override;
  CIEC_ANY *getDIO(size_t) override;
  CEventConnection *getEOConUnchecked(TPortId) override;
  CDataConnection **getDIConUnchecked(TPortId) override;
  CDataConnection *getDOConUnchecked(TPortId) override;
  CInOutDataConnection **getDIOInConUnchecked(TPortId) override;
  CInOutDataConnection *getDIOOutConUnchecked(TPortId) override;

  void evt_INIT(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16NewWidth, const CIEC_UINT &pau16NewHeight, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldWidth, CIEC_UINT &pau16OldHeight, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16NewWidth = pau16NewWidth;
    var_u16NewHeight = pau16NewHeight;
    receiveInputEvent(scmEventINITID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldWidth = var_u16OldWidth;
    pau16OldHeight = var_u16OldHeight;
    pas16result = var_s16result;
  }

  void evt_REQ(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16NewWidth, const CIEC_UINT &pau16NewHeight, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldWidth, CIEC_UINT &pau16OldHeight, CIEC_INT &pas16result) {
    var_u16ObjId = pau16ObjId;
    var_u16NewWidth = pau16NewWidth;
    var_u16NewHeight = pau16NewHeight;
    receiveInputEvent(scmEventREQID, nullptr);
    paSTATUS = var_STATUS;
    pau16OldWidth = var_u16OldWidth;
    pau16OldHeight = var_u16OldHeight;
    pas16result = var_s16result;
  }

  void operator()(const CIEC_UINT &pau16ObjId, const CIEC_UINT &pau16NewWidth, const CIEC_UINT &pau16NewHeight, CIEC_STRING &paSTATUS, CIEC_UINT &pau16OldWidth, CIEC_UINT &pau16OldHeight, CIEC_INT &pas16result) {
    evt_INIT(pau16ObjId, pau16NewWidth, pau16NewHeight, paSTATUS, pau16OldWidth, pau16OldHeight, pas16result);
  }
};


