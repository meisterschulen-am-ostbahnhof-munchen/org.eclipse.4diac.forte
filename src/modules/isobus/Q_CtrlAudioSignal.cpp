/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_CtrlAudioSignal
 *** Description: Command Control audio device ( Part 6 - F.10 ).
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_CtrlAudioSignal.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_CtrlAudioSignal_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX

DEFINE_FIRMWARE_FB(FORTE_Q_CtrlAudioSignal, g_nStringIdQ_CtrlAudioSignal)

const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmDataInputNames[] = {g_nStringIdu8NumOfRepit, g_nStringIdu16Frequency, g_nStringIdu16OnTimeMs, g_nStringIdu16OffTimeMs};
const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmDataInputTypeIds[] = {g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldNumOfRepit, g_nStringIdu16OldFrequency, g_nStringIdu16OldOnTimeMs, g_nStringIdu16OldOffTimeMs, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_CtrlAudioSignal::scmEIWith[] = {0, 1, 2, 3, scmWithListDelimiter};
const TForteInt16 FORTE_Q_CtrlAudioSignal::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_CtrlAudioSignal::scmEOWith[] = {0, 3, 1, 2, 5, 4, scmWithListDelimiter};
const TForteInt16 FORTE_Q_CtrlAudioSignal::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_CtrlAudioSignal::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_CtrlAudioSignal::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  4, scmDataInputNames, scmDataInputTypeIds,
  6, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_CtrlAudioSignal::FORTE_Q_CtrlAudioSignal(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
    CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldNumOfRepit(var_u8OldNumOfRepit),
    var_conn_u16OldFrequency(var_u16OldFrequency),
    var_conn_u16OldOnTimeMs(var_u16OldOnTimeMs),
    var_conn_u16OldOffTimeMs(var_u16OldOffTimeMs),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u8NumOfRepit(nullptr),
    conn_u16Frequency(nullptr),
    conn_u16OnTimeMs(nullptr),
    conn_u16OffTimeMs(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldNumOfRepit(this, 1, &var_conn_u8OldNumOfRepit),
    conn_u16OldFrequency(this, 2, &var_conn_u16OldFrequency),
    conn_u16OldOnTimeMs(this, 3, &var_conn_u16OldOnTimeMs),
    conn_u16OldOffTimeMs(this, 4, &var_conn_u16OldOffTimeMs),
    conn_s16result(this, 5, &var_conn_s16result) {
};

void FORTE_Q_CtrlAudioSignal::setInitialValues() {
  var_u8NumOfRepit = 0_USINT;
  var_u16Frequency = 0_UINT;
  var_u16OnTimeMs = 0_UINT;
  var_u16OffTimeMs = 0_UINT;
  var_STATUS = ""_STRING;
  var_u8OldNumOfRepit = 0_USINT;
  var_u16OldFrequency = 0_UINT;
  var_u16OldOnTimeMs = 0_UINT;
  var_u16OldOffTimeMs = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_CtrlAudioSignal::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendCtrlAudioSignal();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_CtrlAudioSignal::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u8NumOfRepit, conn_u8NumOfRepit);
      readData(1, var_u16Frequency, conn_u16Frequency);
      readData(2, var_u16OnTimeMs, conn_u16OnTimeMs);
      readData(3, var_u16OffTimeMs, conn_u16OffTimeMs);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_CtrlAudioSignal::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(3, var_u16OldOnTimeMs, conn_u16OldOnTimeMs);
      writeData(1, var_u8OldNumOfRepit, conn_u8OldNumOfRepit);
      writeData(2, var_u16OldFrequency, conn_u16OldFrequency);
      writeData(5, var_s16result, conn_s16result);
      writeData(4, var_u16OldOffTimeMs, conn_u16OldOffTimeMs);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_CtrlAudioSignal::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u8NumOfRepit;
    case 1: return &var_u16Frequency;
    case 2: return &var_u16OnTimeMs;
    case 3: return &var_u16OffTimeMs;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_CtrlAudioSignal::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldNumOfRepit;
    case 2: return &var_u16OldFrequency;
    case 3: return &var_u16OldOnTimeMs;
    case 4: return &var_u16OldOffTimeMs;
    case 5: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_CtrlAudioSignal::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_CtrlAudioSignal::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_CtrlAudioSignal::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u8NumOfRepit;
    case 1: return &conn_u16Frequency;
    case 2: return &conn_u16OnTimeMs;
    case 3: return &conn_u16OffTimeMs;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_CtrlAudioSignal::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldNumOfRepit;
    case 2: return &conn_u16OldFrequency;
    case 3: return &conn_u16OldOnTimeMs;
    case 4: return &conn_u16OldOffTimeMs;
    case 5: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_CtrlAudioSignal::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_CtrlAudioSignal::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_CtrlAudioSignal::iso_init(void) {
  // Set the "old Value" to invalid.
  var_u8OldNumOfRepit = static_cast<CIEC_USINT>(0xFF);
  var_u16OldFrequency = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldOnTimeMs = static_cast<CIEC_UINT>(0xFFFF);
  var_u16OldOffTimeMs = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_CtrlAudioSignal::sendCtrlAudioSignal(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      ret = IsoVtcCmd_CtrlAudioSignal(u8VtcInstance, static_cast<iso_u8>(var_u8NumOfRepit), static_cast<iso_u16>(var_u16Frequency), static_cast<iso_u16>(var_u16OnTimeMs), static_cast<iso_u16>(var_u16OffTimeMs));
      switch(ret){
        case E_NO_ERR: //- OK
          var_STATUS = scmOK;
          var_u8OldNumOfRepit = var_u8NumOfRepit; //store this value.
          var_u16OldFrequency = var_u16Frequency; //store this value.
          var_u16OldOnTimeMs = var_u16OnTimeMs; //store this value.
          var_u16OldOffTimeMs = var_u16OffTimeMs; //store this value.
          break;
        case E_OVERFLOW: //- buffer overflow
          var_STATUS = scmE_OVERFLOW;
          break;
        case E_NOACT: //- Command not possible in current state
          var_STATUS = scmE_NOACT;
          break;
        case E_NO_INSTANCE: //- No VT client available
          var_STATUS = scmE_NO_INSTANCE;
          break;
        default:
          var_STATUS = scmERR; //- Unknown Error
          break;

      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}


