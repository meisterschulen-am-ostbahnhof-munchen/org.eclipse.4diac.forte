/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_SetAudioVolume
 *** Description: Command SetAudioVolume.
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_SetAudioVolume.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_SetAudioVolume_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"

#include "AppGetCfHandleMaster.h"

DEFINE_FIRMWARE_FB(FORTE_Q_SetAudioVolume, g_nStringIdQ_SetAudioVolume)

const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmDataInputNames[] = {g_nStringIdu8Volume};
const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmDataInputTypeIds[] = {g_nStringIdUSINT};
const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu8OldVolume, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUSINT, g_nStringIdINT};
const TDataIOID FORTE_Q_SetAudioVolume::scmEIWith[] = {0, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SetAudioVolume::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_SetAudioVolume::scmEOWith[] = {0, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_SetAudioVolume::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_SetAudioVolume::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_SetAudioVolume::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  1, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_SetAudioVolume::FORTE_Q_SetAudioVolume(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u8OldVolume(var_u8OldVolume),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u8Volume(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u8OldVolume(this, 1, &var_conn_u8OldVolume),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_SetAudioVolume::setInitialValues() {
  var_u8Volume = 0_USINT;
  var_STATUS = ""_STRING;
  var_u8OldVolume = 0_USINT;
  var_s16result = 0_INT;
}

void FORTE_Q_SetAudioVolume::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendSetAudioVolume();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_SetAudioVolume::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(0, var_u8Volume, conn_u8Volume);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_SetAudioVolume::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(2, var_s16result, conn_s16result);
      writeData(1, var_u8OldVolume, conn_u8OldVolume);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_SetAudioVolume::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u8Volume;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SetAudioVolume::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u8OldVolume;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_SetAudioVolume::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_SetAudioVolume::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_SetAudioVolume::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u8Volume;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_SetAudioVolume::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u8OldVolume;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_SetAudioVolume::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_SetAudioVolume::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_SetAudioVolume::iso_init(void) {
  // Set the "old Value" to invalid.
  var_u8OldVolume = static_cast<CIEC_USINT>(0xFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_SetAudioVolume::sendSetAudioVolume(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      ret = IsoVtcCmd_SetAudioVolume(u8VtcInstance, static_cast<iso_u8>(var_u8Volume));
      switch(ret){
        case E_NO_ERR: //- OK
          var_STATUS = scmOK;
          var_u8OldVolume = var_u8Volume; //store this value.
          break;
        case E_OVERFLOW: //- buffer overflow
          var_STATUS = scmE_OVERFLOW;
          break;
        case E_NOACT: //- Command not possible in current state
          var_STATUS = scmE_NOACT;
          break;
        case E_NO_INSTANCE: //- No VT client available
          var_STATUS = scmE_NO_INSTANCE;
          break;
        default:
          var_STATUS = scmERR; //- Unknown Error
          break;
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

