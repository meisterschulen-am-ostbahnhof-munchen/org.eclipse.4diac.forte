/*************************************************************************
 *** FORTE Library Element
 ***
 *** This file was generated using the 4DIAC FORTE Export Filter V1.0.x NG!
 ***
 *** Name: Q_ActiveMask
 *** Description: Command change active mask( Part 6 - F.34 )
 *** Version:
 ***     1.0: 2022-07-08/franz -  -
 *************************************************************************/

#include "Q_ActiveMask.h"
#ifdef FORTE_ENABLE_GENERATED_SOURCE_CPP
#include "Q_ActiveMask_gen.cpp"
#endif

#include "criticalregion.h"
#include "resource.h"


#include "AppGetCfHandleMaster.h"

//TODO make a check if every ObjID is only registered once like in IX


DEFINE_FIRMWARE_FB(FORTE_Q_ActiveMask, g_nStringIdQ_ActiveMask)

const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmDataInputNames[] = {g_nStringIdu16WorkSetId, g_nStringIdu16NewMaskId};
const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmDataInputTypeIds[] = {g_nStringIdUINT, g_nStringIdUINT};
const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmDataOutputNames[] = {g_nStringIdSTATUS, g_nStringIdu16OldMaskId, g_nStringIds16result};
const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmDataOutputTypeIds[] = {g_nStringIdSTRING, g_nStringIdUINT, g_nStringIdINT};
const TDataIOID FORTE_Q_ActiveMask::scmEIWith[] = {1, 0, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ActiveMask::scmEIWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmEventInputNames[] = {g_nStringIdINIT, g_nStringIdREQ};
const TDataIOID FORTE_Q_ActiveMask::scmEOWith[] = {0, 1, 2, 1, scmWithListDelimiter};
const TForteInt16 FORTE_Q_ActiveMask::scmEOWithIndexes[] = {-1, 0};
const CStringDictionary::TStringId FORTE_Q_ActiveMask::scmEventOutputNames[] = {g_nStringIdINITO, g_nStringIdCNF};
const SFBInterfaceSpec FORTE_Q_ActiveMask::scmFBInterfaceSpec = {
  2, scmEventInputNames, scmEIWith, scmEIWithIndexes,
  2, scmEventOutputNames, scmEOWith, scmEOWithIndexes,
  2, scmDataInputNames, scmDataInputTypeIds,
  3, scmDataOutputNames, scmDataOutputTypeIds,
  0, nullptr,
  0, nullptr
};

FORTE_Q_ActiveMask::FORTE_Q_ActiveMask(const CStringDictionary::TStringId paInstanceNameId, CResource *const paSrcRes) :
	CisobusFunctionBlock(paSrcRes, &scmFBInterfaceSpec, paInstanceNameId),
    var_conn_STATUS(var_STATUS),
    var_conn_u16OldMaskId(var_u16OldMaskId),
    var_conn_s16result(var_s16result),
    conn_INITO(this, 0),
    conn_CNF(this, 1),
    conn_u16WorkSetId(nullptr),
    conn_u16NewMaskId(nullptr),
    conn_STATUS(this, 0, &var_conn_STATUS),
    conn_u16OldMaskId(this, 1, &var_conn_u16OldMaskId),
    conn_s16result(this, 2, &var_conn_s16result) {
};

void FORTE_Q_ActiveMask::setInitialValues() {
  var_u16WorkSetId = 0_UINT;
  var_u16NewMaskId = 0_UINT;
  var_STATUS = ""_STRING;
  var_u16OldMaskId = 0_UINT;
  var_s16result = 0_INT;
}

void FORTE_Q_ActiveMask::executeEvent(const TEventID paEIID, CEventChainExecutionThread *const paECET) {
  switch(paEIID) {
    case scmEventINITID:
      iso_init();
      sendOutputEvent(scmEventINITOID);
      break;
    case scmEventREQID:
      sendActiveMask();
      sendOutputEvent(scmEventCNFID);
      break;
  }
}

void FORTE_Q_ActiveMask::readInputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventREQID: {
      RES_DATA_CON_CRITICAL_REGION();
      readData(1, var_u16NewMaskId, conn_u16NewMaskId);
      readData(0, var_u16WorkSetId, conn_u16WorkSetId);
      break;
    }
    default:
      break;
  }
}

void FORTE_Q_ActiveMask::writeOutputData(const TEventID paEIID) {
  switch(paEIID) {
    case scmEventCNFID: {
      RES_DATA_CON_CRITICAL_REGION();
      writeData(0, var_STATUS, conn_STATUS);
      writeData(1, var_u16OldMaskId, conn_u16OldMaskId);
      writeData(2, var_s16result, conn_s16result);
      writeData(1, var_u16OldMaskId, conn_u16OldMaskId);
      break;
    }
    default:
      break;
  }
}

CIEC_ANY *FORTE_Q_ActiveMask::getDI(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_u16WorkSetId;
    case 1: return &var_u16NewMaskId;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ActiveMask::getDO(const size_t paIndex) {
  switch(paIndex) {
    case 0: return &var_STATUS;
    case 1: return &var_u16OldMaskId;
    case 2: return &var_s16result;
  }
  return nullptr;
}

CIEC_ANY *FORTE_Q_ActiveMask::getDIO(size_t) {
  return nullptr;
}

CEventConnection *FORTE_Q_ActiveMask::getEOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_INITO;
    case 1: return &conn_CNF;
  }
  return nullptr;
}

CDataConnection **FORTE_Q_ActiveMask::getDIConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_u16WorkSetId;
    case 1: return &conn_u16NewMaskId;
  }
  return nullptr;
}

CDataConnection *FORTE_Q_ActiveMask::getDOConUnchecked(const TPortId paIndex) {
  switch(paIndex) {
    case 0: return &conn_STATUS;
    case 1: return &conn_u16OldMaskId;
    case 2: return &conn_s16result;
  }
  return nullptr;
}

CInOutDataConnection **FORTE_Q_ActiveMask::getDIOInConUnchecked(TPortId) {
  return nullptr;
}

CInOutDataConnection *FORTE_Q_ActiveMask::getDIOOutConUnchecked(TPortId) {
  return nullptr;
}


void FORTE_Q_ActiveMask::iso_init(void) {
  // Set the "old Value" to invalid.
  var_u16OldMaskId = static_cast<CIEC_UINT>(0xFFFF);
  var_STATUS = scmInitialised;
}

//********************************************************************************************

void FORTE_Q_ActiveMask::sendActiveMask(void) {
  iso_s16 ret = E_UNKNOWN_ERR;
  iso_s16 s16CfHandleMaster = HANDLE_UNVALID; // Stored CF handle of VT client
  s16CfHandleMaster = AppGetCfHandleMaster();
  if(s16CfHandleMaster != HANDLE_UNVALID) {
    iso_u8 u8VtcInstance = ISO_INSTANCE_INVALID;
    u8VtcInstance = IsoVtcGetInstanceID(s16CfHandleMaster, VTC_MAIN_INSTANCE);
    if(u8VtcInstance != ISO_INSTANCE_INVALID) {
      if(func_NE(var_u16OldMaskId, var_u16NewMaskId)) {
        ret = IsoVtcCmd_ActiveMask(u8VtcInstance, static_cast<iso_u16>(var_u16WorkSetId), static_cast<iso_u16>(var_u16NewMaskId));
        switch(ret){
          case E_NO_ERR: //- OK
            var_STATUS = scmOK;
            var_u16OldMaskId = var_u16NewMaskId; //store this Value.
            break;
          case E_OVERFLOW: //- buffer overflow
            var_STATUS = scmE_OVERFLOW;
            break;
          case E_NOACT: //- Command not possible in current state
            var_STATUS = scmE_NOACT;
            break;
          case E_NO_INSTANCE: //- No VT client available
            var_STATUS = scmE_NO_INSTANCE;
            break;
          default:
            var_STATUS = scmERR; //- Unknown Error
            break;
        }
      } else {
        var_STATUS = scmOK_oldIsNew; // this Number was not sent, because it was already set.
      }
    } else {
      var_STATUS = VtcInstanceunvalid;
    }
  } else {
    var_STATUS = CfHandleMasterunvalid;
  }
  var_s16result = static_cast<CIEC_INT>(ret);
}

