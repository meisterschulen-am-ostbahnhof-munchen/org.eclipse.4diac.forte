#!/bin/bash
echo "----------------------------------------------------------------------------"
echo " Automatically set up development environment for POSIX-platform"
echo "----------------------------------------------------------------------------"
echo ""
echo " Includes 64bit-datatypes, float-datatypes, Ethernet-Interface,"
echo " ASN1-encoding, ..."
echo ""
echo " To include tests set directories for boost-test-framework and "
echo " set FORTE_TESTS-option to 'ON'"
echo ""
echo "----------------------------------------------------------------------------"

export forte_bin_dir="bin/esp32c3"

#set to boost-include directory
export forte_boost_test_inc_dirs=""
#set to boost-library directory
export forte_boost_test_lib_dirs=""

if [ ! -d "$forte_bin_dir" ]; then
  mkdir -p "$forte_bin_dir"
fi

if [ -d "$forte_bin_dir" ]; then
  
  echo "For building forte go to $forte_bin_dir and execute \"make\""
  echo "forte can be found at ${forte_bin_dir}/src"
  echo "forte_tests can be found at ${forte_bin_dir}/tests"
  
  
  cd "./$forte_bin_dir"
  
  cmake -G "Unix Makefiles" \
    -DCMAKE_TOOLCHAIN_FILE="${IDF_PATH}/tools/cmake/toolchain-esp32c3.cmake" \
    -DCMAKE_C_COMPILER="riscv32-esp-elf-gcc" \
    -DCMAKE_CXX_COMPILER="riscv32-esp-elf-g++" \
    -DFORTE_ARCHITECTURE=FreeRTOSLwIP \
    -DFORTE_FreeRTOSLwIP_INCLUDES="\
${IDF_PATH}/components/freertos/esp_additions/include;\
${IDF_PATH}/components/freertos/esp_additions/include/freertos;\
${IDF_PATH}/components/freertos/FreeRTOS-Kernel/portable/riscv/include;\
${IDF_PATH}/components/freertos/FreeRTOS-Kernel/include;\
${IDF_PATH}/components/freertos/FreeRTOS-Kernel/include/freertos;\
${IDF_PATH}/components/esp_common/include;\
${IDF_PATH}/components/freertos/port/riscv/include;\
${IDF_PATH}/components/riscv/include;\
${IDF_PATH}/components/lwip/lwip/src;\
${IDF_PATH}/components/riscv;\
${IDF_PATH}/components/riscv/include;\
${IDF_PATH}/components/esp_rom/include;\
${IDF_PATH}/components/esp_ringbuf/include;\
${IDF_PATH}/components/esp_rom/include/linux;\
${IDF_PATH}/components/esp_hw_support/include;\
${IDF_PATH}/components/hal/include;\
${IDF_PATH}/components/lwip/lwip/src/include;\
${IDF_PATH}/components/soc/esp32c3/include;\
${IDF_PATH}/components/lwip/include;\
${IDF_PATH}/components/lwip/port/esp32/include;\
${IDF_PATH}/components/hal/esp32c3/include;\
${IDF_PATH}/components/soc/include;\
${IDF_PATH}/components/esp_system/include;\
${IDF_PATH}/components/esp_timer/include;\
${IDF_PATH}/components/newlib/platform_include;\
${IDF_PATH}/components/heap/include;\
${IDF_PATH}/components/freertos/include;\
${IDF_PATH}/components/freertos/include/esp_additions;\
${IDF_PATH}/components/driver/include;\
${IDF_PATH}/components/log/include;\
${IDF_PATH}/components/nvs_flash/include;\
${IDF_PATH}/components/spi_flash/include;\
${IDF_PATH}/components/esp_partition/include;\
${IDF_PATH}/components/esp_adc/include;\
../../Application/components/led_strip/include;\
../../Application/components/led_strip/interface;\
../../Application/components/button/include"\
    -DCMAKE_C_FLAGS="-ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_C_FLAGS_DEBUG="-g -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_C_FLAGS_MINSIZEREL="-Os -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_C_FLAGS_RELEASE="-O3 -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_C_FLAGS_RELWITHDEBINFO="-O2 -g -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_CXX_FLAGS="-ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-Os -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_CXX_FLAGS_RELEASE="-O3 -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-O2 -g -DNDEBUG -ffunction-sections -fdata-sections -fno-threadsafe-statics -fno-rtti -fno-exceptions -DconfigMINIMAL_STACK_SIZE_FORTE=15000 -DCONFIG_IDF_CMAKE=1 -DCONFIG_IDF_TARGET_ESP32C3=1 -DCONFIG_FREERTOS_UNICORE=1 -DCONFIG_FREERTOS_HZ=1000 -DCONFIG_FREERTOS_MAX_TASK_NAME_LEN=16 -DCONFIG_LWIP_TCP_OVERSIZE_MSS=1" \
    -DFORTE_BUILD_EXECUTABLE=OFF \
    -DFORTE_BUILD_STATIC_LIBRARY=ON \
    -DFORTE_IO=ON \
    -DFORTE_IO_COMMON_IO=ON \
    -DFORTE_MODULE_ESP32=ON \
    -DFORTE_MODULE_esp-addons=ON \
    -DFORTE_MODULE_HUTSCHIENENMOPED=ON \
    -DFORTE_MODULE_ESP32_DIGITAL_IN=ON \
    -DFORTE_MODULE_ESP32_DIGITAL_OUT=ON \
    -DCMAKE_BUILD_TYPE=DEBUG \
    -DFORTE_LOGLEVEL=LOGINFO \
    -DFORTE_SUPPORT_ARRAYS=ON \
    -DFORTE_COM_ETH=ON \
    -DFORTE_COM_FBDK=ON \
    -DFORTE_COM_LOCAL=ON \
    -DFORTE_SUPPORT_BOOT_FILE=ON \
    -DFORTE_BootfileLocation="/data/test_FORTE_PC.fboot" \
    -DFORTE_TESTS=OFF \
    -DFORTE_TESTS_INC_DIRS=${forte_boost_test_inc_dirs} \
    -DFORTE_TESTS_LINK_DIRS=${forte_boost_test_inc_dirs} \
    -DFORTE_MODULE_IEC61131=ON \
    -DFORTE_MODULE_UTILS_SIMPLE=ON \
    -DFORTE_MODULE_UTILS=OFF \
    -DFORTE_USE_LUATYPES=Lua \
    -DLUA_INCLUDE_DIR="../../Application/components/lua/src" \
    ../../
else
  echo "unable to create ${forte_bin_dir}"
  exit 1
fi
